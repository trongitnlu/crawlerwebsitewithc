﻿using CrawlerWeb.model;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;



namespace CrawlerWeb
{
    public partial class Form1 : Form
    {

        public Form1()
        {
            InitializeComponent();
            Activate();
            //WindowState = FormWindowState.Maximized;
            setupListView();

        }

        private List<string> listHeaderSpa = new List<string>() { "STT","ID", "Vị trí", "Link", "Danh mục" };
        private List<string> listHeaderItem = new List<string>() { "STT", "Tên công ty", "Tên sản phẩm", "Link hình ảnh" };
        private List<string> listHeader = new List<string>() { "STT","ID", "Vị trí", "Link" };
        private ICollection<dynamic> listCategory;
        private ICollection<dynamic> list;

        private List<dynamic> listCategoryAdd = new List<dynamic>();
        private List<SearchTypes> listCategorySearch = new List<SearchTypes>();

        private dynamic[] arrayCategory = {"Cần lấy danh mục"};


        private void setupListView()
        {
           
            listHeader.ForEach(name => listView1.Columns.Add(name));
            ResizeListViewColumns(listView1);

        }
        private void ResizeListViewColumns(ListView lv)
        {
            foreach (ColumnHeader column in lv.Columns)
            {
                column.Width = -2;
            }
        }

        private void btnCrawler_Click(object sender, EventArgs e)
        {
            if(list !=null && list.Count > 0)
            {
                string website = comboBox1.Text;
                switch (website)
                {
                    case Constant.VIET_NAME_WORK: crawlerVietNamWork(); break;
                    case Constant.TIM_VIET_NHANH: crawlerTimVietNhanh(); break;
                    case Constant.CAREER_LINK: crawlerCareerLink(); break;
                    case Constant.IT_VIEC: crawlerITViec(); break;
                    case Constant.MY_WORK: crawlerMyWork(); break;
                    case Constant.DIA_CHI_LAM_DEP: crawlerDiaChiLamDep(); break;
                    case Constant.TRANG_VANG_VIET_NAM: crawlerTrangVangVietNam(); break;
                    case Constant.CAREER_BUILDER: crawlerCareerBuilder(); break;
                }
                
            }
            else
            {
                MessageBox.Show("Không có data", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
           
        }

        private void crawlerCareerBuilder()
        {
            string address = getAddressSaveJobsDetail();
            System.Diagnostics.Debug.WriteLine(address);
            if (!address.Equals(""))
            {
                var crawler = new CrawlerCareerBuilder();
                if (radioButtonExcel.Checked)
                    crawler.getJobDetail(list, checkBoxAutoSaveJobs.Checked, address);
                else if (radioButtonXML.Checked)
                    crawler.getJobDetailWithXML(list, checkBoxAutoSaveJobs.Checked, address);
            }
            else
            if (!checkBoxAutoSaveJobs.Checked)
            {
                var crawler = new CrawlerCareerBuilder();
                if (radioButtonExcel.Checked)
                    crawler.getJobDetail(list, checkBoxAutoSaveJobs.Checked, address);
                else if (radioButtonXML.Checked)
                    crawler.getJobDetailWithXML(list, checkBoxAutoSaveJobs.Checked, address);
            }
        }

        private void crawlerTrangVangVietNam()
        {
            string address = getAddressSaveJobsDetail();
            if (!address.Equals(""))
            {
                var crawler = new CrawlerTrangVangVietNam();
                if(radioButtonExcel.Checked)
                    crawler.getJobDetail(list, checkBoxAutoSaveJobs.Checked, address);
                else if(radioButtonXML.Checked)
                    crawler.getJobDetailWithXML(list, checkBoxAutoSaveJobs.Checked, address);
            }
            else
              if (!checkBoxAutoSaveJobs.Checked)
            {
                var crawler = new CrawlerTrangVangVietNam();
                if (radioButtonExcel.Checked)
                    crawler.getJobDetail(list, checkBoxAutoSaveJobs.Checked, address);
                else if (radioButtonXML.Checked)
                    crawler.getJobDetailWithXML(list, checkBoxAutoSaveJobs.Checked, address);
            }


        }

        private void crawlerDiaChiLamDep()
        {
            string address = getAddressSaveJobsDetail();
            System.Diagnostics.Debug.WriteLine(address);
            if (!address.Equals(""))
            {
                var crawler = new CrawlerDiaChiLamDep();
                if (radioButtonExcel.Checked)
                    crawler.getJobDetail(list, checkBoxAutoSaveJobs.Checked, address);
                else if (radioButtonXML.Checked)
                    crawler.getJobDetailWithXML(list, checkBoxAutoSaveJobs.Checked, address);
            }
            else
              if (!checkBoxAutoSaveJobs.Checked)
            {
                var crawler = new CrawlerDiaChiLamDep();
                if (radioButtonExcel.Checked)
                    crawler.getJobDetail(list, checkBoxAutoSaveJobs.Checked, address);
                else if (radioButtonXML.Checked)
                    crawler.getJobDetailWithXML(list, checkBoxAutoSaveJobs.Checked, address);
            }
        }

        private void crawlerMyWork()
        {
            string address = getAddressSaveJobsDetail();
            System.Diagnostics.Debug.WriteLine(address);
            if (!address.Equals(""))
            {
                var crawler = new CrawlerMyWork();
                if (radioButtonExcel.Checked)
                    crawler.getJobDetail(list, checkBoxAutoSaveJobs.Checked, address);
                else if (radioButtonXML.Checked)
                    crawler.getJobDetailWithXML(list, checkBoxAutoSaveJobs.Checked, address);
            }
            else
              if (!checkBoxAutoSaveJobs.Checked)
            {
                var crawler = new CrawlerMyWork();
                if (radioButtonExcel.Checked)
                    crawler.getJobDetail(list, checkBoxAutoSaveJobs.Checked, address);
                else if (radioButtonXML.Checked)
                    crawler.getJobDetailWithXML(list, checkBoxAutoSaveJobs.Checked, address);
            }
        }

        private void crawlerITViec()
        {
            string address = getAddressSaveJobsDetail();
            System.Diagnostics.Debug.WriteLine(address);
            if (!address.Equals(""))
            {
                var crawler = new CrawlerITViec();
                if (radioButtonExcel.Checked)
                    crawler.getJobDetail(list, checkBoxAutoSaveJobs.Checked, address);
                else if (radioButtonXML.Checked)
                    crawler.getJobDetailWithXML(list, checkBoxAutoSaveJobs.Checked, address);
            }
            else
              if (!checkBoxAutoSaveJobs.Checked)
            {
                var crawler = new CrawlerITViec();
                if (radioButtonExcel.Checked)
                    crawler.getJobDetail(list, checkBoxAutoSaveJobs.Checked, address);
                else if (radioButtonXML.Checked)
                    crawler.getJobDetailWithXML(list, checkBoxAutoSaveJobs.Checked, address);
            }
        }

        private void crawlerCareerLink()
        {
            string address = getAddressSaveJobsDetail();
            System.Diagnostics.Debug.WriteLine(address);
            if (!address.Equals(""))
            {
                var crawler = new CrawlerCareerLink();
                if (radioButtonExcel.Checked)
                    crawler.getJobDetail(list, checkBoxAutoSaveJobs.Checked, address);
                else if (radioButtonXML.Checked)
                    crawler.getJobDetailWithXML(list, checkBoxAutoSaveJobs.Checked, address);
            }
            else
            if (!checkBoxAutoSaveJobs.Checked)
            {
                var crawler = new CrawlerCareerLink();
                if (radioButtonExcel.Checked)
                    crawler.getJobDetail(list, checkBoxAutoSaveJobs.Checked, address);
                else if (radioButtonXML.Checked)
                    crawler.getJobDetailWithXML(list, checkBoxAutoSaveJobs.Checked, address);
            }
        }

        private void crawlerVietNamWork()
        {
            string address = getAddressSaveJobsDetail();
            System.Diagnostics.Debug.WriteLine(address);
            if (!address.Equals(""))
            {
                var crawler = new CrawlerVietnamWork();
                if (radioButtonExcel.Checked)
                    crawler.getJobDetail(list, checkBoxAutoSaveJobs.Checked, address);
                else if (radioButtonXML.Checked)
                    crawler.getJobDetailWithXML(list, checkBoxAutoSaveJobs.Checked, address);
            }
            else
            if (!checkBoxAutoSaveJobs.Checked)
            {
                var crawler = new CrawlerVietnamWork();
                if (radioButtonExcel.Checked)
                    crawler.getJobDetail(list, checkBoxAutoSaveJobs.Checked, address);
                else if (radioButtonXML.Checked)
                    crawler.getJobDetailWithXML(list, checkBoxAutoSaveJobs.Checked, address);
            }
           
        }

        private void crawlerTimVietNhanh()
        {
            string address = getAddressSaveJobsDetail();
            System.Diagnostics.Debug.WriteLine(address);
            if (!address.Equals(""))
            {
                var crawler = new CrawlerTimViecNhanh();
                if (radioButtonExcel.Checked)
                    crawler.getJobDetail(list, checkBoxAutoSaveJobs.Checked, address);
                else if (radioButtonXML.Checked)
                    crawler.getJobDetailWithXML(list, checkBoxAutoSaveJobs.Checked, address);
            }
            else
              if (!checkBoxAutoSaveJobs.Checked)
            {
                var crawler = new CrawlerTimViecNhanh();
                if (radioButtonExcel.Checked)
                    crawler.getJobDetail(list, checkBoxAutoSaveJobs.Checked, address);
                else if (radioButtonXML.Checked)
                    crawler.getJobDetailWithXML(list, checkBoxAutoSaveJobs.Checked, address);
            }

        }


        private void btnTest_Click(object sender, EventArgs e)
        {

            string text = comboBox1.Text;
            string address = comboBoxAddress.Text;
            string salary = comboBoxSalary.Text;
            if(address.Equals("") || salary.Equals(""))
            {
                MessageBox.Show("Lỗi!", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                switch (text)
                {
                    case Constant.VIET_NAME_WORK: { getLinkVietNamWork(); break; }
                    case Constant.TIM_VIET_NHANH: { getLinkTimVietNhanh(); break; }
                    case Constant.CAREER_LINK: { getLinkCareerLink(); break; }
                    case Constant.IT_VIEC: { getLinkItViec(); break; }
                    case Constant.MY_WORK: { getLinkMyWork(); break; }
                    case Constant.DIA_CHI_LAM_DEP: { getLinkDiaChiLamDepByCategory(address); break; }
                    case Constant.TRANG_VANG_VIET_NAM: { getLinkTrangVangVietNam(address); break; }
                    case Constant.CAREER_BUILDER: { getLinkCareerBuilder(); break; }
                }
            }

        }

        private void getLinkCareerBuilder()
        {
            var crawler = new CrawlerCareerBuilder();
            crawler.numberPages = int.Parse(textBoxPages.Text);
            string address = comboBoxAddress.Text;
            string salary = comboBoxSalary.Text;

            var searchType = new SearchTypes()
            {
                address = address,
                salary = salary
            };

            list = crawler.getLinkByCustom(searchType);
            loadListDataIntoListView(list);
            saveAutoLinks(list);
        }

        private void getLinkTrangVangVietNam(string address)
        {
            var crawler = new CrawlerTrangVangVietNam();
            crawler.numberPages = int.Parse(textBoxPages.Text);

            foreach (var e in listCategory)
            {
                if (e.title.Equals(address))
                {
                    list = crawler.getLinkByCustom(e.href);
                    break;
                }
            }

            loadListDataIntoListView(list);
            saveAutoLinks(list);
            if (list.Count > 0)
            {
                btnGetUrlImage.Visible = true;
            }
            else
            {
                btnGetUrlImage.Visible = false;
            }
        }

        private void getLinkDiaChiLamDepByCategory(string address)
        {
            var crawler = new CrawlerDiaChiLamDep();
            crawler.numberPages = int.Parse(textBoxPages.Text);

            foreach(Category e in listCategory)
            {
                if (e.category.Equals(address))
                {
                     list = crawler.getLinkByCustom(e.linkSpas);
                    break;
                }
            }


           
            loadListDataIntoListView(list);
            saveAutoLinks(list);
        }

        private void getLinkCategoryDiaChiLamDep()
        {
            var crawler = new CrawlerDiaChiLamDep();
            crawler.numberPages = int.Parse(textBoxPages.Text);

            listCategory = crawler.getCategory();
            arrayCategory = listCategory.Select(x => x.category ).ToArray();
            updateComboBoxAddress(arrayCategory);
            if (arrayCategory.Length > 0)
            {
                btnCategory.Enabled = false;
            }
        }

        private void getLinkMyWork()
        {
            var crawler = new CrawlerMyWork();
            crawler.numberPages = int.Parse(textBoxPages.Text);
            string address = comboBoxAddress.Text;
            string salary = comboBoxSalary.Text;

            var searchType = new SearchTypes()
            {
                address = address,
                salary = salary
            };

            list = crawler.getLinkByCustom(searchType);
            loadListDataIntoListView(list);
            saveAutoLinks(list);
        }

        private void getLinkItViec()
        {
            var crawler = new CrawlerITViec();
            crawler.numberPages = int.Parse(textBoxPages.Text);
            string address = comboBoxAddress.Text;
            string salary = comboBoxSalary.Text;

            var searchType = new SearchTypes()
            {
                address = address,
                salary = salary
            };

            list = crawler.getLinkByCustom(searchType);
            loadListDataIntoListView(list);
            saveAutoLinks(list);
        }

        private void getLinkCareerLink()
        {
            var crawler = new CrawlerCareerLink();
            crawler.numberPages = int.Parse(textBoxPages.Text);
            string address = comboBoxAddress.Text;
            string salary = comboBoxSalary.Text;

            var searchType =new SearchTypes(){
                address = address,
                salary = salary
            };

            list = crawler.getLinkByCustom(searchType);
            loadListDataIntoListView(list);
            saveAutoLinks(list);
        }

        private void getLinkTimVietNhanh()
        {
            var crawler = new CrawlerTimViecNhanh();
            crawler.numberPages = int.Parse(textBoxPages.Text);
            string address = comboBoxAddress.Text;
            string salary = comboBoxSalary.Text;

            list = crawler.getLinkByCustom(address, salary);

            loadListDataIntoListView(list);
            saveAutoLinks(list);
        }

        private void getLinkVietNamWork()
        {
            var crawler = new CrawlerVietnamWork();
            crawler.numberPages = int.Parse(textBoxPages.Text);
            string address = comboBoxAddress.Text;
            string salary = comboBoxSalary.Text;

            list = crawler.getLinkByCustom(address, salary);

            loadListDataIntoListView(list);

            saveAutoLinks(list);

        }

        private void saveAutoLinks(ICollection<dynamic> list)
        {
            if(list.Count>0)
            if (checkBoxAutoSaveLinks.Checked)
            {
                    if (Directory.Exists(textBoxAddressLinkJobs.Text))
                    {
                        if (radioButtonExcel.Checked)
                        {
                            string time = DateTime.Now.ToString("dd-MM-yyyy hh:mm:ss", CultureInfo.InvariantCulture);
                            string fileName = comboBox1.Text + "_" + comboBoxAddress.Text + "_" + comboBoxSalary.Text + "_" + time;
                            fileName = fileName.Replace(' ', '_').Replace(':', '-');
                            string des = textBoxAddressLinkJobs.Text + "\\Link_" + fileName + ".xlsx";
                            System.Diagnostics.Debug.WriteLine(des);
                            ExcelAction excel = new ExcelAction();
                            excel.save(des, list);
                        }
                        else if (radioButtonXML.Checked)
                        {
                            string time = DateTime.Now.ToString("dd-MM-yyyy hh:mm:ss", CultureInfo.InvariantCulture);
                            string fileName = comboBox1.Text + "_" + comboBoxAddress.Text + "_" + comboBoxSalary.Text + "_" + time;
                            fileName = fileName.Replace(' ', '_').Replace(':', '-');
                            string des = textBoxAddressLinkJobs.Text + "\\Link_" + fileName + ".xml";
                            System.Diagnostics.Debug.WriteLine(des);
                            Util.saveXML(list, des);
                        }
                    }
                    else
                    {
                        MessageBox.Show("Đường dẫn thư mục lưu Links sai!", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
            }
        }

        private void saveAutoLinksFilter(ICollection<dynamic> list)
        {
            if (list.Count > 0)
                if (checkBoxAutoSaveLinks.Checked)
                {
                    if (Directory.Exists(textBoxAddressLinkJobs.Text))
                    {
                            string time = DateTime.Now.ToString("dd-MM-yyyy hh:mm:ss", CultureInfo.InvariantCulture);
                            string fileName = comboBox1.Text + "_TuNgay_" +  textBoxDate.Text + "_" + time;
                            fileName = fileName.Replace(' ', '_').Replace(':', '-').Replace('/','-');
                            string des = textBoxAddressLinkJobs.Text + "\\Link_" + fileName + ".xlsx";
                        if (radioButtonExcel.Checked)
                        {
                            System.Diagnostics.Debug.WriteLine(des);
                            ExcelAction excel = new ExcelAction();
                            excel.save(des, list);
                        }
                        else if (radioButtonXML.Checked)
                        {
                            System.Diagnostics.Debug.WriteLine(des);
                            Util.saveXML(list, des);
                        }
                    }
                    else
                    {
                        MessageBox.Show("Đường dẫn thư mục lưu Links sai!", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
        }
        private string getAddressSaveJobsDetail()
        {
            if (checkBoxAutoSaveJobs.Checked)
            {
                if (Directory.Exists(textBoxAddressJobsDetail.Text))
                {
                    string time = DateTime.Now.ToString("dd-MM-yyyy hh:mm:ss", CultureInfo.InvariantCulture);
                    string fileName = comboBox1.Text + "_" + comboBoxAddress.Text + "_" + comboBoxSalary.Text + "_" + time;
                    fileName = fileName.Replace(' ', '_').Replace(':', '-');
                    string des = textBoxAddressJobsDetail.Text + "\\Details_" + fileName + ".xlsx";
                    System.Diagnostics.Debug.WriteLine(des);
                    return des;
                }
                else
                {
                    MessageBox.Show("Đường dẫn thư mục lưu Links sai!", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return "";
                }
            }
            else
            {
                return "";
            }
                
         }

        private string getAddressSaveJobsDetail(string firstName)
        {
            if (checkBoxAutoSaveJobs.Checked)
            {
                if (Directory.Exists(textBoxAddressJobsDetail.Text))
                {
                    string time = DateTime.Now.ToString("dd-MM-yyyy hh:mm:ss", CultureInfo.InvariantCulture);
                    string fileName = comboBox1.Text + "_" + comboBoxAddress.Text + "_" + comboBoxSalary.Text + "_" + time;
                    fileName = fileName.Replace(' ', '_').Replace(':', '-');
                    string des = textBoxAddressJobsDetail.Text + "\\" + firstName + "_" + fileName + ".xlsx";
                    System.Diagnostics.Debug.WriteLine(des);
                    return des;
                }
                else
                {
                    MessageBox.Show("Đường dẫn thư mục lưu Links sai!", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return "";
                }
            }
            else
            {
                return "";
            }

        }

        private void addHeaderListView()
        {
            

            listHeader.ForEach(name => listView1.Columns.Add(name));
            ResizeListViewColumns(listView1);
        }

        private void addHeaderListView(List<string> listHeader)
        {
            listHeader.ForEach(name => listView1.Columns.Add(name));
            ResizeListViewColumns(listView1);
        }

        private void loadListDataIntoListView(ICollection<dynamic> list)
        {
            try
            {
                listView1.Clear();

                addHeaderListView();

                int i = 1;
                foreach (Job e in list)
                {
                    ListViewItem listViewItem = new ListViewItem();
                    listViewItem.Text = i++.ToString();
                    listViewItem.SubItems.Add(e.id.ToString());
                    listViewItem.SubItems.Add(e.title);
                    listViewItem.SubItems.Add(e.href);
                    listView1.Items.Add(listViewItem);
                }
                if (list.Count > 0)
                {
                    btnGetUrlImage.Visible = true;
                }
                else
                {
                    btnGetUrlImage.Visible = false;
                }
            }
            catch(Exception e)
            {
                Util.showMessageError("Có gì đó sai sai!");
                Console.WriteLine(e);
            }
            
        }

        private void loadListDataLinkListView(ICollection<dynamic> list)
        {
            try
            {
                listView1.Clear();

                addHeaderListView(listHeaderSpa);

                int i = 1;
                foreach (LinkSpa e in list)
                {
                    ListViewItem listViewItem = new ListViewItem();
                    listViewItem.Text = i++.ToString();
                    listViewItem.SubItems.Add(e.title);
                    listViewItem.SubItems.Add(e.href);
                    listViewItem.SubItems.Add(e.category);
                    listView1.Items.Add(listViewItem);
                }
                if (list.Count > 0)
                {
                    btnGetUrlImage.Visible = true;
                }
                else
                {
                    btnGetUrlImage.Visible = false;
                }
            }
            catch(Exception e)
            {
                MessageBox.Show("Có gì đó sai sai!", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if(list != null && list.Count > 0)
            {
                if (radioButtonExcel.Checked)
                {
                    ExcelAction excel = new ExcelAction();
                    excel.saveExcel(list);
                }else if (radioButtonXML.Checked)
                {
                    SaveFileDialog saveFileDialog = new SaveFileDialog();
                    saveFileDialog.Filter = "Các tệp xml|*.xml|Tất cả các tệp|*.*";
                    saveFileDialog.ShowDialog();
                    if (!saveFileDialog.FileName.Equals(""))
                    {
                        Util.saveXML(list, saveFileDialog.FileName);
                    }
                    else
                    {
                        MessageBox.Show("Đường dẫn không chính xác", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                
            }
            else
            {
                MessageBox.Show("Không có dữ liệu để lưu!", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            
        }

        private void btnLoadExcel_Click(object sender, EventArgs e)
        {
            OpenFileDialog fileDialog = new OpenFileDialog();
            fileDialog.Filter = "Excel files (*.xlsx)|*.xlsx|XML files (*.xml)|*.xml|All files (*.*)|*.*";
            fileDialog.ShowDialog();
            string fileName = fileDialog.FileName;

            if (fileName != "" && File.Exists(fileName))
            {
                string fileExt = Path.GetExtension(fileName);
                if (fileExt == ".xml")
                {
                    list = Util.OpenXML(fileName);
                    loadListDataIntoListView(list);
                }else if(fileExt == ".xlsx")
                {
                    ExcelAction excel = new ExcelAction();
                    var listExcel = excel.ActionOpenExcel(fileName);
                    if (listExcel.Count > 0)
                    {
                        list = listExcel;
                        loadListDataIntoListView(list);
                    }
                }

            }
            
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            string text = comboBox1.Text;
            panelDate.Visible = false;

            if (list == null || list.Count <= 0)
            {
                btnCategory.Visible = false;
                btnGetUrlImage.Visible = false;
            }
            
            switch (text)
            {
                case Constant.VIET_NAME_WORK: { updateComboBoxAddress(Constant.addressVietNamWork); updateComboBoxSalary(Constant.salaryOfferVietNamWork); break; }
                case Constant.TIM_VIET_NHANH: { updateComboBoxAddress(Constant.addressTimVietNhanh); updateComboBoxSalary(Constant.salaryOfferTimViecNhanh); break; }
                case Constant.CAREER_LINK: { updateComboBoxAddress(Constant.addressCareerLink); updateComboBoxSalary(Constant.salaryOfferCareerLink);  break;  }
                case Constant.IT_VIEC: { updateComboBoxAddress(Constant.addressITViec); updateComboBoxSalary(Constant.salaryOfferITViec); break; }
                case Constant.MY_WORK: { updateComboBoxAddress(Constant.addressMyWork); updateComboBoxSalary(Constant.salaryOfferMyWork); break; }
                case Constant.DIA_CHI_LAM_DEP: { btnCategory.Visible = true; updateComboBoxAddress(arrayCategory); updateComboBoxSalary(new string[] {"No" }); break; }
                case Constant.TRANG_VANG_VIET_NAM:
                    { visiblePanelDate(); btnCategory.Visible = true; updateComboBoxAddress(arrayCategory); updateComboBoxSalary(new string[] { "No" }); break; }
                case Constant.CAREER_BUILDER: { updateComboBoxAddress(Constant.addressCareerBuilder); updateComboBoxSalary(Constant.salaryOfferCareerBuilder); break; }

            }
        }

        private void visiblePanelDate()
        {
            panelDate.Visible = true;
            textBoxDate.Text = DateTime.Today.ToString("dd/MM/yyyy");
        }

        private void updateComboBoxAddress(object[] objects)
        {
            if(objects.Length > 0)
            comboBoxAddress.Text = objects[0].ToString();
            comboBoxAddress.Items.Clear();
            comboBoxAddress.Items.AddRange(objects);
        }

        private void updateComboBoxSalary(object[] objects)
        {
            if (objects.Length > 0)
            comboBoxSalary.Text = objects[0].ToString();
            comboBoxSalary.Items.Clear();
            comboBoxSalary.Items.AddRange(objects);
        }

        private void btnChooseAddressSaveLinkJob_Click(object sender, EventArgs e)
        {
            textBoxAddressLinkJobs.Text = GetSelectFolder();

        }
        private string GetSelectFolder()
        {
            FolderBrowserDialog openFileDialog = new FolderBrowserDialog();
            DialogResult result = openFileDialog.ShowDialog();

            if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(openFileDialog.SelectedPath))
            {
               return openFileDialog.SelectedPath;
            }
            else
            {
                MessageBox.Show("File không tồn tại!", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return "";
            }
        }

        private void btnChooseAddressSaveJobDetail_Click(object sender, EventArgs e)
        {
            textBoxAddressJobsDetail.Text = GetSelectFolder();
        }

        private void btnCategory_Click(object sender, EventArgs e)
        {
            string text = comboBox1.Text;
            string address = comboBoxAddress.Text;
                switch (text)
                {
                    case Constant.DIA_CHI_LAM_DEP: { getLinkCategoryDiaChiLamDep(); break; }
                    case Constant.TRANG_VANG_VIET_NAM: { getLinkCategoryTrangVangVietNam(); break; }
            }
        }

        private void getLinkCategoryTrangVangVietNam()
        {
            var crawler = new CrawlerTrangVangVietNam();
            crawler.numberPages = int.Parse(textBoxPages.Text);

            listCategory = crawler.getCategory();
            arrayCategory = listCategory.Select(x => x.title).ToArray();
            updateComboBoxAddress(arrayCategory);
            if (arrayCategory.Length > 0)
            {
                btnCategory.Enabled = false;
            }
        }

        private void btnGetUrlImage_Click(object sender, EventArgs e)
        {
            if (list != null && list.Count > 0)
            {
                string website = comboBox1.Text;
                switch (website)
                {
                    case Constant.TRANG_VANG_VIET_NAM: crawlerAllUrlImage(); break;
                }

            }
            else
            {
                MessageBox.Show("Không có data", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void crawlerAllUrlImage()
        {
            string address = getAddressSaveJobsDetail("Link_URL");
            System.Diagnostics.Debug.WriteLine(address);
            if (!address.Equals(""))
            {
                var crawler = new CrawlerTrangVangVietNam();
                if (radioButtonExcel.Checked)
                    list = crawler.getAllLinkImgDetail(list, checkBoxAutoSaveJobs.Checked, address);
                else if (radioButtonXML.Checked)
                    list =crawler.getAllLinkImgDetailWithXML(list, checkBoxAutoSaveJobs.Checked, address);
            }
            else
              if (!checkBoxAutoSaveJobs.Checked)
            {
                var crawler = new CrawlerTrangVangVietNam();
                if (radioButtonExcel.Checked)
                    list = crawler.getAllLinkImgDetail(list, checkBoxAutoSaveJobs.Checked, address);
                else if (radioButtonXML.Checked)
                    list = crawler.getAllLinkImgDetailWithXML(list, checkBoxAutoSaveJobs.Checked, address);
            }

            loadListDataLinkImgToListView(list);
        }

        private void btnChooseAddressSaveImg_Click(object sender, EventArgs e)
        {
            textBoxAddressSaveImg.Text = GetSelectFolder();
        }

        private void btnOpenExcel_Click(object sender, EventArgs e)
        {
            try
            {
                if (radioButtonExcel.Checked)
                {
                    ExcelAction excel = new ExcelAction();
                    var listExcel = excel.ActionOpenExcelImg();
                    if (listExcel.Count > 0)
                    {
                        list = listExcel;
                    }
                }
                else if (radioButtonXML.Checked)
                {
                    OpenFileDialog dialog = Util.showOpenFileDialogXML();
                    if (!dialog.FileName.Equals(""))
                    {
                        Util.OpenXMLImg(dialog.FileName);
                    }
                }
                loadListDataLinkImgToListView(list);
            }
            catch(Exception ev)
            {
                Console.WriteLine(ev);
                Util.showMessageError("Có lỗi xảy ra, vui lòng chọn đúng định dạng!");
            }
            
            
        }

        private void loadListDataLinkImgToListView(ICollection<dynamic> list)
        {
            try
            {
                listView1.Clear();

                addHeaderListView(listHeaderSpa);

                int i = 1;
                foreach (Item e in list)
                {
                    ListViewItem listViewItem = new ListViewItem();
                    listViewItem.Text = i++.ToString();
                    listViewItem.SubItems.Add(e.id);
                    listViewItem.SubItems.Add(e.title);
                    listViewItem.SubItems.Add(e.nameSP);
                    listViewItem.SubItems.Add(e.href);
                    listView1.Items.Add(listViewItem);
                }
                if (list.Count > 0 && comboBox1.Text.Equals(Constant.TRANG_VANG_VIET_NAM))
                {
                    btnGetUrlImage.Visible = true;
                }
                else
                {
                    btnGetUrlImage.Visible = false;
                }
            }
            catch(Exception e)
            {
                MessageBox.Show("Không đúng định dạng!", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            
        }


       

        private void button3_Click(object sender, EventArgs e)
        {
            System.Net.ServicePointManager.DefaultConnectionLimit = 1000;
            btnChooseAddressSaveImg.Enabled = false;
            if (list == null || list.Count <= 0)
                MessageBox.Show("Không có data!", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            else
            {
                Util.downloadImageAsync(list, textBoxAddressSaveImg.Text, progress =>
                {
                    labelProgress.Text = progress;
                });
            }
            //DownloadMultipleFilesAsync(list as ICollection<Item>);
        }
        private async Task DownloadFileAsync(Item e)
        {
            try
            {
                using (WebClient webClient = new WebClient())
                {
                    string downloadToDirectory = textBoxAddressSaveImg.Text + "\\" + getFileName(e.href);
                    webClient.Credentials = System.Net.CredentialCache.DefaultNetworkCredentials;
                    await webClient.DownloadFileTaskAsync(new Uri(e.href), @downloadToDirectory);

                    //Add them to the local
                }
            }
            catch (Exception)
            {
            }
        }

        private string getFileName(string urlImage)
        {
            Uri uri = new Uri(urlImage);
            string filename = Path.GetFileName(uri.LocalPath);
            return filename;
        }

        private async Task DownloadMultipleFilesAsync(ICollection<Item> doclist)
        {
            await Task.WhenAll(doclist.Select(doc => DownloadFileAsync(doc)));
        }

        private void btnAddCategory_Click(object sender, EventArgs e)
        {
            
            switch (comboBox1.Text)
            {
                case Constant.TRANG_VANG_VIET_NAM: addLinkTrangVangVietNam(); break;
                case Constant.DIA_CHI_LAM_DEP: break;
                default: addListCategoryJob(); break;
            }
            
        }

        private void addListCategoryJob()
        {
            dataGridView1.ColumnCount = 3;
            dataGridView1.Columns[0].Name = "STT";
            dataGridView1.Columns[1].Name = "Tỉnh";
            dataGridView1.Columns[2].Name = "Lương";
            SearchTypes types = new SearchTypes()
            {
                address = comboBoxAddress.Text,
                salary = comboBoxSalary.Text,
            };
            listCategorySearch.Add(types);
            dataGridView1.Rows.Add(listCategorySearch.Count, types.address, types.salary);
        }

        private void addLinkTrangVangVietNam()
        {
            dataGridView1.ColumnCount = 2;
            dataGridView1.Columns[0].Name = "STT";
            dataGridView1.Columns[1].Name = "Danh mục";
            var item = listCategory.Where(r => r.title.Equals(comboBoxAddress.Text)).First();
            if (!listCategoryAdd.Contains(item))
            {
                listCategoryAdd.Add(item);
                dataGridView1.Rows.Add(listCategoryAdd.Count, item.title);
            }
        }

        private void btnDeleteCategory_Click(object sender, EventArgs e)
        {

            switch (comboBox1.Text)
            {
                case Constant.TRANG_VANG_VIET_NAM: deleteCategoryTrangVangVietNam(); break;
                case Constant.DIA_CHI_LAM_DEP: break;
                default: deleteCategoryJob(); break;
            }
                
        }
        private void deleteCategoryJob()
        {
            if (listCategorySearch.Count > 0)
                listCategorySearch.RemoveAt(dataGridView1.SelectedCells[0].RowIndex);
            dataGridView1.Rows.RemoveAt(dataGridView1.SelectedCells[0].RowIndex);
            updateGridViewJob();
        }

        private void deleteCategoryTrangVangVietNam()
        {
            if (listCategoryAdd.Count > 0)
                listCategoryAdd.RemoveAt(dataGridView1.SelectedCells[0].RowIndex);
            dataGridView1.Rows.RemoveAt(dataGridView1.SelectedCells[0].RowIndex);
            updateGridView();
        }

        private void updateGridViewJob()
        {
            int i = 1;

            dataGridView1.Rows.Clear();

            System.Diagnostics.Debug.WriteLine(listCategorySearch.Count);
            foreach (var c in listCategorySearch)
            {
                dataGridView1.Rows.Add(i++, c.address, c.salary);
            }
        }

        private void updateGridView()
        {
            int i = 1;

            dataGridView1.Rows.Clear();

            System.Diagnostics.Debug.WriteLine(listCategoryAdd.Count);
            foreach (var c in listCategoryAdd)
            {
                dataGridView1.Rows.Add(i++, c.title);
            }
        }

        private void btnGetLinkByListCategory_Click(object sender, EventArgs e1)
        {
            switch (comboBox1.Text)
            {
                case Constant.TRANG_VANG_VIET_NAM:
                    {
                        var crawler = new CrawlerTrangVangVietNam();
                        crawler.numberPages = int.Parse(textBoxPages.Text);

                        list = crawler.getLinkByCustom(listCategoryAdd);

                        if (list.Count > 0)
                        {
                            btnGetUrlImage.Visible = true;
                        }
                        else
                        {
                            btnGetUrlImage.Visible = false;
                        }
                    }; break;
                case Constant.DIA_CHI_LAM_DEP: break;
                case Constant.VIET_NAME_WORK:
                    {
                        var crawler = new CrawlerVietnamWork();
                        crawler.numberPages = int.Parse(textBoxPages.Text);
                        list = crawler.getLinkByCustomListCategory(listCategorySearch);

                    }; break;

                case Constant.TIM_VIET_NHANH:
                    {
                        var crawler = new CrawlerTimViecNhanh();
                        crawler.numberPages = int.Parse(textBoxPages.Text);
                        list = crawler.getLinkByCustomListCategory(listCategorySearch);
                        
                    }; break;
                case Constant.MY_WORK:
                    {
                        var crawler = new CrawlerMyWork();
                        crawler.numberPages = int.Parse(textBoxPages.Text);
                        list = crawler.getLinkByCustomListCategory(listCategorySearch);

                    }; break;

                case Constant.CAREER_LINK:
                    {
                        var crawler = new CrawlerCareerLink();
                        crawler.numberPages = int.Parse(textBoxPages.Text);
                        list = crawler.getLinkByCustomListCategory(listCategorySearch);

                    }; break;
                case Constant.IT_VIEC:
                    {
                        var crawler = new CrawlerITViec();
                        crawler.numberPages = int.Parse(textBoxPages.Text);
                        list = crawler.getLinkByCustomListCategory(listCategorySearch);

                    }; break;

                case Constant.CAREER_BUILDER:
                    {
                        var crawler = new CrawlerCareerBuilder();
                        crawler.numberPages = int.Parse(textBoxPages.Text);
                        list = crawler.getLinkByCustomListCategory(listCategorySearch);

                    }; break;
            }
            loadListDataIntoListView(list);
            saveAutoLinks(list);

        }

        private void btnDeleteListLinkItem_Click(object sender, EventArgs ev)
        {
            try
            {
                int from = int.Parse(numericUpDownFrom.Text);
                int to = int.Parse(numericUpDownTo.Text);


                List<dynamic> jobs = list.ToList();
                for(var i=0; i<to; i++)
                {
                    jobs.RemoveAt(from-1);
                }
                list = jobs;
                loadListDataIntoListView(list);

            }
            catch(Exception e)
            {
                Console.WriteLine(e);
            }
        }

        string ExtractString(string s)
        {
            // You should check for errors in real-world code, omitted for brevity
            var startTag = "(";
            int startIndex = s.IndexOf(startTag) + startTag.Length;
            int endIndex = s.IndexOf(")", startIndex);
            return s.Substring(startIndex, endIndex - startIndex).Trim();
        }

        private void btnFilter_Click(object sender, EventArgs e)
        {
            //Regex regex = new Regex("*(*)*");
            // var v = regex.Match("Cập nhật ngày: (26/12/2016 )");
            // string s = v.Groups[1].ToString();
            //Console.WriteLine(ExtractString("Cập nhật ngày: (26/12/2018 )"));
            //var a = ExtractString("Cập nhật ngày: (26/12/2018 )").Split('/');
            //var b = textBoxDate.Text.Split('/');

            //var date1 = new DateTime(int.Parse(a[2]), int.Parse(a[1]), int.Parse(a[0]));
            //var date2 = new DateTime(int.Parse(b[2]), int.Parse(b[1]), int.Parse(b[0]));
            //int result = DateTime.Compare(date1, date2);
            //Console.WriteLine(result);
            if (list != null && list.Count > 0)
            {
                string website = comboBox1.Text;
                switch (website)
                {
                    case Constant.TRANG_VANG_VIET_NAM: filterDateTrangVangVietNam(); break;
                }

            }
            else
            {
                MessageBox.Show("Không có data", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void filterDateTrangVangVietNam()
        {
            var crawler = new CrawlerTrangVangVietNam();
            crawler.strDate = textBoxDate.Text;

            list = crawler.filterLinkByDate(list);

            loadListDataIntoListView(list);
            saveAutoLinksFilter(list);
            if (list.Count > 0)
            {
                btnGetUrlImage.Visible = true;
            }
            else
            {
                btnGetUrlImage.Visible = false;
            }
        }
    }
}