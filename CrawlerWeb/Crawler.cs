﻿using CrawlerWeb.model;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CrawlerWeb
{
    abstract class Crawler
    {
        public int numberPages { get; set; } = 0;
        public string strDate { get; set; } = "";
        protected IWebDriver chromeDriver;

        public Crawler()
        {
            var options = new ChromeOptions();
            options.AddArgument("--window-size=1024,768");
            chromeDriver = new ChromeDriver(ChromeDriverService.CreateDefaultService(), options, TimeSpan.FromMinutes(5));
        }

        protected IWebElement getDataWebEmlement(By by)
        {
            try
            {
                return chromeDriver.FindElement(by);

            }
            catch (NoSuchElementException e)
            {
                return new EmptyElement();
            }
        }

        protected ReadOnlyCollection<IWebElement> getDataWebEmlements(By by)
        {
            try
            {
                return chromeDriver.FindElements(by);

            }
            catch (NoSuchElementException e)
            {
                return new ReadOnlyCollection<IWebElement>(new List<IWebElement>());
            }
        }
        protected string sliptCharacter(string text, char pattern)
        {
            try
            {
                string result = text.Split(pattern)[1].Trim();
                return result;
            }
            catch
            {
                return text;
            }
        }
        protected string splitByString(string data, string pattern, int index)
        {

            string result = "";
            try
            {
                result = data.Split(new string[] { pattern }, StringSplitOptions.None)[index];

            }
            catch (Exception e)
            {
                result = data;
            }
            return result;
        }

        protected string getListDataText(By by)
        {
            var list = getDataWebEmlements(by);
            string result = "";
            foreach (var e in list)
            {
                result += e.Text.Trim() + "\n";
            }
            return result;
        }

        public void getJobDetailWithXML(ICollection<dynamic> list, bool autoSave, string fileDes)
        {
            LinkedList<JobDetail> listDetail = new LinkedList<JobDetail>();
            try
            {
                foreach (Job job in list)
                {
                    crawler(job, jobDetail =>
                    {
                        listDetail.AddLast(jobDetail);
                    });

                };

                chromeDriver.Quit();
                if (autoSave)
                {

                    saveXML(listDetail, fileDes);
                }
                else
                {
                    SaveFileDialog dialog = Util.chooseAddressSaveXML();
                    if (!dialog.FileName.Equals(""))
                        saveXML(listDetail, dialog.FileName);

                }
            }
            catch (Exception e)
            {
                chromeDriver.Quit();
                if (autoSave)
                {

                    saveXML(listDetail, fileDes);
                }
                else
                {
                    SaveFileDialog dialog = Util.chooseAddressSaveXML();
                    if (!dialog.FileName.Equals(""))
                        saveXML(listDetail, dialog.FileName);

                }
                System.Diagnostics.Debug.WriteLine(e);
                return;
            }
        }

        //Những link được add vào gridview
        public LinkedList<dynamic> getLinkByCustomListCategory(List<SearchTypes> list)
        {
            LinkedList<dynamic> listLink = new LinkedList<dynamic>();
            try
            {

                foreach (SearchTypes e in list)
                {

                    foreach (var r in getLinkByCustomListCategory(e))
                    {
                        listLink.AddLast(r);
                    }
                };

                chromeDriver.Quit();
            }
            catch (Exception e)
            {
                chromeDriver.Quit();
                System.Diagnostics.Debug.WriteLine(e);
                return listLink;
            }

            return listLink;
        }
        public abstract LinkedList<dynamic> getLinkByCustomListCategory(SearchTypes types);
        public abstract void crawler(Job job, Action<JobDetail> callback);
        public abstract void saveXML(ICollection<JobDetail> list, string desc);
    }
}
