﻿using CrawlerWeb.model;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Linq;
using Excel = Microsoft.Office.Interop.Excel;

namespace CrawlerWeb
{
    class CrawlerITViec : Crawler
    {
        private SearchTypes searchTypes = null;
        private string[] arrayHeader = {"ID", "Vị trí", "Tên công ty", "Ngày cập nhật", "Địa chỉ",
                                                    "Quy mô công ty",
                        //"Người liên hệ",
                                                    "Lương", "Phúc lợi",
                                                   //"Ngành nghề", "Cấp bậc", "Kỹ Năng", "Ngôn ngữ",
                                                   //"Hạn nộp CV",
                        "Mô tả",
                                                   "Yêu cầu công việc", "Thông tin khác", "Loại hình công ty",

                        "Quốc gia", "Ngày làm việc", "Overtime",
        };

        public void getJobDetailWithXML(ICollection<dynamic> list, bool autoSave, string fileDes)
        {
            LinkedList<JobDetail> listDetail = new LinkedList<JobDetail>();

            var isLogin = login(chromeDriver);

            if (isLogin)
            {
                try
                {
                    foreach (Job job in list)
                    {
                        crawler(job, jobDetail =>
                        {
                            listDetail.AddLast(jobDetail);
                        });

                    };

                    chromeDriver.Quit();
                    if (autoSave)
                    {

                        saveXML(listDetail, fileDes);
                    }
                    else
                    {
                        SaveFileDialog dialog = Util.chooseAddressSaveXML();
                        if (!dialog.FileName.Equals(""))
                            saveXML(listDetail, dialog.FileName);

                    }
                }
                catch (Exception e)
                {
                    chromeDriver.Quit();
                    if (autoSave)
                    {

                        saveXML(listDetail, fileDes);
                    }
                    else
                    {
                        SaveFileDialog dialog = Util.chooseAddressSaveXML();
                        if (!dialog.FileName.Equals(""))
                            saveXML(listDetail, dialog.FileName);

                    }
                    System.Diagnostics.Debug.WriteLine(e);
                    return;
                }
            }
            else
            {
                Util.showMessageError("Đăng nhập thất bại!");
            }
        }

            public void getJobDetail(ICollection<dynamic> list, bool autoSave, string fileDes)
        {
            var isLogin = login(chromeDriver);

            if (isLogin)
            {
                ExcelAction excel = new ExcelAction();
                LinkedList<JobDetail> listDetail = new LinkedList<JobDetail>();
                    foreach(Job job in list)
                    {
                        crawler(job, jobDetail =>
                        {
                            listDetail.AddLast(jobDetail);

                        });

                    };

                chromeDriver.Quit();
                if (autoSave)
                {
                    excel.saveJobDetailCustom(fileDes, sheet => {
                        insertData(sheet, listDetail);
                    });
                }
                else
                {
                    excel.saveExcelJobDetail(sheet =>
                    {
                        insertData(sheet, listDetail);
                    });
                }
            }
            else
            {
                MessageBox.Show("Đăng nhập thất bại!", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private string creatLinkByAddress(string address)
        {
            string s = "";
            //City
            if (address.Equals(Constant.addressITViec[0]))
            {
                s = "it-jobs?page=";
            }
            //Ho chi Minh
            if (address.Equals(Constant.addressITViec[1]))
            {
                s = "ho-chi-minh-hcm?page=";
            }

            //Ha Noi
            if (address.Equals(Constant.addressITViec[2]))
            {
                s = "ha-noi?page=";
            }

            //Da Nang
            if (address.Equals(Constant.addressITViec[3]))
            {
                s = "da-nang?page=";
            }

            //Ohthers
            if (address.Equals(Constant.addressITViec[4]))
            {
                s = "others?page=";
            }
            return "https://itviec.com/viec-lam-it/" + s;

        }

        override
        public LinkedList<dynamic> getLinkByCustomListCategory(SearchTypes searchTypes)
        {
            LinkedList<dynamic> list = new LinkedList<dynamic>();
            this.searchTypes = searchTypes;
            string url = creatLinkByAddress(searchTypes.address) + 0;

            try
            {
                chromeDriver.Navigate().GoToUrl(url);

                getAllLinkJob(e =>
                {
                    list = e;
                });
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e);
                return list;
            }

            return list;


        }

        public LinkedList<dynamic> getLinkByCustom(SearchTypes searchTypes)
        {
            LinkedList<dynamic> list = new LinkedList<dynamic>();
            this.searchTypes = searchTypes;
            string url = creatLinkByAddress(searchTypes.address)+0;

            try
            {
                chromeDriver.Navigate().GoToUrl(url);

                getAllLinkJob(e =>
                {
                    list = e;
                });
                chromeDriver.Quit();
            }
            catch(Exception e)
            {
                chromeDriver.Quit();
                System.Diagnostics.Debug.WriteLine(e);
                return list;
            }
            
            return list;


        }
        int id = 1;

        public void getAllLinkJob(Action<LinkedList<dynamic>> callback)
        {
                LinkedList<dynamic> listAddress = new LinkedList<dynamic>();
            try
            {
                IWebElement aNext1 = null;
                string url = creatLinkByAddress(searchTypes.address);
                int i = 2;
                int page = 0;
                while (true)
                {
                    Thread.Sleep(1000);
                    var jobItems = getDataWebEmlement(By.ClassName("first-group")).FindElements(By.ClassName("job"));
                    foreach (var job in jobItems)
                    {
                        string title = job.FindElement(By.TagName("h2")).Text;
                        string href = job.FindElement(By.TagName("h2")).FindElement(By.TagName("a")).GetAttribute("href");
                        listAddress.AddLast(new Job(id++.ToString(), title, href));
                    }
                    try
                    {
                        aNext1 = getDataWebEmlement(By.Id("show_more"));
                        chromeDriver.Navigate().GoToUrl(url + i++);
                        page++;
                    }
                    catch (NoSuchElementException e)
                    {
                        aNext1 = new EmptyElement();
                    }
                    //Cài đặt bao nhiêu trang là thoát
                    if (page == numberPages && numberPages > 0)
                    {
                        callback(listAddress);
                        return;
                    }

                    //end
                    if (aNext1 is EmptyElement)
                    {
                        callback(listAddress);
                        return;
                    }
                    else
                    {
                        continue;
                    }

                }
            }
            catch(Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e);
                callback(listAddress);
            }

        }

        public bool login(IWebDriver chromeDriver)
        {
            chromeDriver.Navigate().GoToUrl("https://itviec.com/");
            Thread.Sleep(1000);
            var btnLogin = getDataWebEmlement(By.XPath("//*[@id='pageMenuToggle']/ul/li[5]/a"));
            if(btnLogin is EmptyElement)
            {
                return false;
            }
            btnLogin.Click();
            Thread.Sleep(1000);
            var inputEmail = getDataWebEmlement(By.XPath("//*[@id=\"signin-tab\"]/form/div[2]/input"));
            inputEmail.SendKeys("taikhoantest3210@gmail.com");
            var inputPasswork = getDataWebEmlement(By.XPath("//*[@id='signin-tab']/form/div[3]/input"));
            inputPasswork.SendKeys("trong2010" + OpenQA.Selenium.Keys.Enter);


            Thread.Sleep(1000);
            var userId = getDataWebEmlement(By.XPath("//*[@id=\"user-dropdown\"]/a/span[1]")).Text;

            return userId.Equals("nguyenwipwa");
        }

        private string getDateByTime(string strTime)
        {
            int ngay  = strTime.Contains("ngày trước") ? int.Parse(strTime.Split(' ')[0].Trim()) : 0;
            DateTime now = DateTime.Today.AddDays(-ngay);
            return now.ToString("dd/M/yyyy", CultureInfo.InvariantCulture);
        }

        private void insertData(Excel._Worksheet sheet, LinkedList<JobDetail> listDetail)
        {
            
            for (int h = 1; h <= arrayHeader.Length; h++)
            {
                sheet.Cells[1, h] = arrayHeader[h - 1];
            }

            int i = 0;
            foreach (var b in listDetail)
            {
                string[] a = {b.id, b.title, b.nameCty,b.ngayCapNhat,
                    b.addressCty,
                    b.quyMoCty,
                    //b.nguoiLienHe,
                    b.luong,
                    b.phucLoi,
                    //b.nganhNghe,
                    //b.capBac,
                    //b.kyNang,
                    //b.ngonNgu,
                    //b.hanNopCV,
                    b.motaCV,
                    b.yeucauCV,
                        b.thongTinKhac,
                b.loaiHinhCty,
                b.quocGia,
                b.ngayLamViec,
                b.overtime,
                };
                for (int j = 1; j <= a.Length; j++)
                {
                    sheet.Cells[i + 2, j].NumberFormat = "@";
                    sheet.Cells[i + 2, j].HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;
                    sheet.Cells[i + 2, j] = a[j - 1];
                }
                i++;
            }
        }

        override
        public void crawler(Job job, Action<JobDetail> callback)
        {
            try
            {
                chromeDriver.Navigate().GoToUrl(job.href);
                try
                {
                    string title = getDataWebEmlement(By.ClassName("job_title")).Text;
                    string nameCty = getDataWebEmlement(By.ClassName("name")).Text;
                    string ngayCapNhat = getDataWebEmlement(By.ClassName("distance-time-job-posted")).Text;


                    string luong = getDataWebEmlement(By.ClassName("salary")).Text;
                    string thongTinKhac = getDataWebEmlement(By.ClassName("top-3-reasons")).Text;
                    string loaiHinh = getDataWebEmlement(By.ClassName("gear-icon")).Text;
                    string quocGia = getDataWebEmlement(By.ClassName("country-name")).Text;
                    string ngayLamViec = getDataWebEmlement(By.ClassName("working-date")).Text;
                    string overtime = getDataWebEmlement(By.ClassName("overtime")).Text;

                    //string nganhNghe = getDataWebEmlement(By.XPath("//*[@id=\"job-info\"]/div/div[1]/div/div[1]/div[3]/div[2]/span[2]")).Text;
                    //string capBac = getDataWebEmlement(By.XPath("//*[@id=\"job-info\"]/div/div[1]/div/div[1]/div[2]/div[2]/span[2]")).Text;
                    //string kyNang = getDataWebEmlement(By.XPath("//*[@id=\"job-info\"]/div/div[1]/div/div[1]/div[4]/div[2]/span[2]")).Text;
                    //string ngonNgu = getDataWebEmlement(By.XPath("//*[@id=\"job-info\"]/div/div[1]/div/div[1]/div[5]/div[2]/span[2]")).Text;



                    // string hanNopCV = getDataWebEmlement(By.XPath("//*[@id=\"wrapper\"]/div[3]/div[5]/section[1]/div/div[2]/div[2]/div/div[1]/div/div[2]/div/span[4]")).Text;
                    string motaCV = getDataWebEmlement(By.ClassName("description")).Text;
                    string yeucauCV = getDataWebEmlement(By.ClassName("experience")).Text;

                    var a = getDataWebEmlement(By.XPath("//*[@id=\"wrapper\"]/div[3]/div[5]/section[2]/ul/li[2]/a"));
                    if (!(a is EmptyElement))
                    {
                        a.Click();
                        Thread.Sleep(1000);
                    }

                    string addressCty = getDataWebEmlement(By.ClassName("address__full-address")).Text;
                    string quyMoCty = getDataWebEmlement(By.ClassName("group-icon")).Text;
                    string phucLoi = getDataWebEmlement(By.ClassName("culture_description")).Text;

                    //string nguoiLienHe = getDataWebEmlement(By.XPath("//*[@id=\"company-info\"]/div/div[2]/div/div/div[3]/div[2]/span[2]")).Text;


                    System.Diagnostics.Debug.WriteLine(title + "\t" + ngayCapNhat + "\t" + nameCty + "\t" + addressCty + "\t" + thongTinKhac + "\t");
                    //System.Diagnostics.Debug.WriteLine(nganhNghe + "\t" + capBac + "\t" + luong);
                    //System.Diagnostics.Debug.WriteLine(hanNopCV + "\t" + motaCV + "\t" + yeucauCV + "\t");

                    JobDetail jobDetail = new JobDetail()
                    {
                        id = job.id,
                        title = title,
                        nameCty = nameCty,
                        ngayCapNhat = getDateByTime(ngayCapNhat),
                        addressCty = addressCty,
                        quyMoCty =  quyMoCty,
                        //nguoiLienHe = nguoiLienHe,
                        luong = luong,
                        phucLoi = phucLoi,
                        //nganhNghe = nganhNghe,
                        //capBac = capBac,
                        //kyNang = kyNang,
                        //ngonNgu = ngonNgu,
                        //hanNopCV = hanNopCV,
                        motaCV = motaCV,
                        yeucauCV = yeucauCV,
                        thongTinKhac = thongTinKhac,
                        loaiHinhCty = loaiHinh,
                        quocGia = quocGia,
                        ngayLamViec = ngayLamViec,
                        overtime = overtime,

                    };
                    callback(jobDetail);
                }
                catch (Exception e)
                {

                    System.Diagnostics.Debug.WriteLine(e.Message);
                    crawler(job, callback);
                }
            }
            catch(Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e);
                return;
            }
                   
        }

        //XML
        override
        public void saveXML(ICollection<JobDetail> list, string desc)
        {
            try
            {
                System.Diagnostics.Debug.WriteLine("Bat dau luu! XML ");
                XElement xmlElements = new XElement("data", list.Select(i => new XElement("Item", nodes(i))));

                xmlElements.Save(desc + ".xml");

                System.Diagnostics.Debug.WriteLine("Xong!");
                Util.showMessageInformation("Lưu thành công: " + list.Count + " Item.");
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e);
                MessageBox.Show("Lưu thất bại!", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private object[] nodes(JobDetail b)
        {
            int i = 0;
            string[] a = {b.id, b.title, b.nameCty,b.ngayCapNhat,
                    b.addressCty,
                    b.quyMoCty,
                    //b.nguoiLienHe,
                    b.luong,
                    b.phucLoi,
                    //b.nganhNghe,
                    //b.capBac,
                    //b.kyNang,
                    //b.ngonNgu,
                    //b.hanNopCV,
                    b.motaCV,
                    b.yeucauCV,
                        b.thongTinKhac,
            b.loaiHinhCty,
            b.quocGia,
            b.ngayLamViec,
            b.overtime,
            };
            object[] nodes = arrayHeader.Select(e => new XElement(e.Replace(' ', '-'), a[i++])).ToArray<object>();

            return nodes;
        }

    }
}
