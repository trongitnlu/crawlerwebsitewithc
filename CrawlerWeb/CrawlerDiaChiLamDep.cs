﻿using CrawlerWeb.model;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Linq;
using Excel = Microsoft.Office.Interop.Excel;

namespace CrawlerWeb
{
    class CrawlerDiaChiLamDep : Crawler
    {
        private string[] arrayHeader = {"ID", "Tên danh mục", "Liên hệ", "Điện thoại", "Địa chỉ", "Email", "Khu vực", "Loại hình" };
        int i = 1;

        public void getAllLinkItem(LinkedList<dynamic> list,Action<LinkedList<dynamic>> callback)
        {
        
            LinkedList<dynamic> listAddress = new LinkedList<dynamic>();
            IWebElement aNext1 = null;
            int page = 0;

            foreach (LinkSpa e in list)
            {
           
                chromeDriver.Navigate().GoToUrl(e.href);
                var linkItems = getDataWebEmlement(By.ClassName("Ajax")).FindElements(By.ClassName("SellingTitle"));
                System.Diagnostics.Debug.WriteLine(linkItems.Count);
                foreach (var location in linkItems)
                {
                    string title = location.FindElement(By.TagName("a")).GetAttribute("title");
                    string href = location.FindElement(By.TagName("a")).GetAttribute("href");
                    listAddress.AddLast(new Job(i++.ToString(), title, href));
                }

            }

            try
            {
                aNext1 = getDataWebEmlement(By.XPath("//a[contains(text(),'>>')]"));

            }
            catch (NoSuchElementException e)
            {
                aNext1 = new EmptyElement();
            }

            if (aNext1.GetAttribute("href") == null || aNext1.GetAttribute("href").Equals("") || aNext1 is EmptyElement)
            {
                callback(listAddress);
                chromeDriver.Quit();
                return;
            }
            else
            {
                System.Diagnostics.Debug.WriteLine("click: " + page);
                aNext1.Click();
                page++;
            }

            //Cài đặt bao nhiêu trang là thoát
            if (page == numberPages && numberPages > 0)
            {
                callback(listAddress);
                return;
            }

        }

        public LinkedList<dynamic> getLinkByCustom(LinkedList<dynamic> list)
        {
            LinkedList<dynamic> listLink = new LinkedList<dynamic>();
            try
            {
                string url = "http://diachilamdep.net/tin-dang/";

                chromeDriver.Navigate().GoToUrl(url);

                getAllLinkItem(list ,e =>
                {
                    listLink = e;
                });
                chromeDriver.Quit();
            }
            catch (Exception e)
            {
                chromeDriver.Quit();
                System.Diagnostics.Debug.WriteLine(e);
                return listLink;
            }

            return listLink;
        }

        public LinkedList<dynamic> getCategory()
        {
            LinkedList<dynamic> list = new LinkedList<dynamic>();
            try
            {
                string url = "http://diachilamdep.net/tin-dang/";

                chromeDriver.Navigate().GoToUrl(url);
                
                getAllLinkCategory(e =>
                {
                    list = e;
                });
                chromeDriver.Quit();
            }
            catch(Exception e)
            {
                chromeDriver.Quit();
                System.Diagnostics.Debug.WriteLine(e);
                return list;
            }

            return list;
        }

        private bool checkReject(string category, string [] a) 
        {
            bool isTrue = true;
            foreach (var e in a)
            {
                if (category.Equals(e))
                {
                    isTrue = false;
                }
            }
            return isTrue;
        }

        public void getAllLinkCategory(Action<LinkedList<dynamic>> callback)
        {
            string[] a = {"TUYỂN DỤNG", "Xăm Nghệ Thuật", "Dịch Vụ Y Tế", " Studio - Trang Điểm" };
            LinkedList<dynamic> categorys = new LinkedList<dynamic>();

                var linkItems = getDataWebEmlement(By.Id("tree")).FindElements(By.TagName("li"));
                foreach (var link in linkItems)
                {
                    LinkedList<dynamic> listAddress = new LinkedList<dynamic>();
                    var linkLocations = link.FindElements(By.TagName("li"));
                    string category = link.Text.Trim();
                    if(checkReject(category, a) && !category.Equals(""))
                    {
                        foreach (var location in linkLocations)
                        {
                            string locationStr = location.FindElement(By.TagName("a")).GetAttribute("title");
                            string href = location.FindElement(By.TagName("a")).GetAttribute("href");
                            listAddress.AddLast(new LinkSpa(category, locationStr, href));
                        }
                        var cate = new Category(category, listAddress);
                        System.Diagnostics.Debug.WriteLine(cate);
                        categorys.AddLast(cate);
                    }
                    
                }
                callback(categorys);
        }

        public void getJobDetail(ICollection<dynamic> list, bool autoSave, string fileDes)
        {
                ExcelAction excel = new ExcelAction();
                LinkedList<JobDetail> listDetail = new LinkedList<JobDetail>();
            try
            {
                foreach (Job job in list)
                {
                    crawler(job, jobDetail =>
                    {
                        listDetail.AddLast(jobDetail);
                    });

                };

                chromeDriver.Quit();
                if (autoSave)
                {
                    excel.saveJobDetailCustom(fileDes, sheet => {
                        insertData(sheet, listDetail);
                    });
                }
                else
                {
                    excel.saveExcelJobDetail(sheet =>
                    {
                        insertData(sheet, listDetail);
                    });
                }
            }
            catch (Exception e)
            {
                chromeDriver.Quit();
                if (autoSave)
                {
                    excel.saveJobDetailCustom(fileDes, sheet => {
                        insertData(sheet, listDetail);
                    });
                }
                else
                {
                    excel.saveExcelJobDetail(sheet =>
                    {
                        insertData(sheet, listDetail);
                    });
                }
                System.Diagnostics.Debug.WriteLine(e);
                return;
            }
        }

        private void insertData(Excel._Worksheet sheet, LinkedList<JobDetail> listDetail)
        {
            for (int h = 1; h <= arrayHeader.Length; h++)
            {
                sheet.Cells[1, h] = arrayHeader[h - 1];
            }

            int i = 0;
            System.Diagnostics.Debug.WriteLine(listDetail.Count);
            foreach (var b in listDetail)
            {
                string[] a = {
                    b.id,
                    b.title,
                    b.nguoiLienHe,
                    b.sdt,
                    b.diachiLienHe,
                    b.email,
                    b.addressCty,
                    b.nganhNghe,
                };

                for (int j = 1; j <= a.Length; j++)
                {
                    sheet.Cells[i + 2, j].NumberFormat = "@";
                    sheet.Cells[i + 2, j].HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;
                    sheet.Cells[i + 2, j] = a[j - 1];
                }
                i++;
            }
        }

        override
        public void crawler(Job job, Action<JobDetail> callback)
        {
            chromeDriver.Navigate().GoToUrl(job.href);
            try
            {
                    string title = getDataWebEmlement(By.XPath("/html/body/div[2]/div[2]/div[1]/div[1]/div[3]/div[1]/div[2]/h1")).Text;
                    string category = getDataWebEmlement(By.XPath("/html/body/div[2]/div[1]/div[2]/div/a[2]/span")).Text;
                    string location = getDataWebEmlement(By.XPath("/html/body/div[2]/div[1]/div[2]/div/a[3]/span")).Text;
                    string nguoiLienHe = getDataWebEmlement(By.XPath("/html/body/div[2]/div[2]/div[1]/div[1]/div[3]/div[1]/div[2]/div[2]/div/div[2]/p[1]/span[2]/strong")).Text;
                    string diaChiLienHe = getDataWebEmlement(By.XPath("/html/body/div[2]/div[2]/div[1]/div[1]/div[3]/div[1]/div[2]/div[2]/div/div[2]/p[3]/span[2]")).Text;
                    string sdt = getDataWebEmlement(By.XPath("/html/body/div[2]/div[2]/div[1]/div[1]/div[3]/div[1]/div[2]/div[2]/div/div[2]/p[2]/span[2]")).Text;
                    string email = getDataWebEmlement(By.XPath("/html/body/div[2]/div[2]/div[1]/div[1]/div[3]/div[1]/div[2]/div[2]/div/div[2]/p[4]/span[2]/a")).Text;

                    System.Diagnostics.Debug.WriteLine(title + "\t" + nguoiLienHe + "\t" + diaChiLienHe+ "\t" + sdt + "\t" + email);

                    JobDetail jobDetail = new JobDetail()
                    {
                        id = job.id,
                        title = title,
                        nguoiLienHe = nguoiLienHe,
                        diachiLienHe = diaChiLienHe,                 
                        sdt = sdt,
                        email = email,
                        addressCty = location,
                        nganhNghe = category,

                    };

                    callback(jobDetail);
                
            }
            catch (WebDriverException e)
            {
                System.Diagnostics.Debug.WriteLine("Link Die: " + job.href);
                return;
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("Lỗi nặng quá: " + job.href);
                System.Diagnostics.Debug.WriteLine(e);
                return;
            }

        }

        override
        public void saveXML(ICollection<JobDetail> list, string desc)
        {
            try
            {
                System.Diagnostics.Debug.WriteLine("Bat dau luu! XML ");
                XElement xmlElements = new XElement("data", list.Select(i => new XElement("Item", nodes(i))));

                xmlElements.Save(desc + ".xml");

                System.Diagnostics.Debug.WriteLine("Xong!");
                Util.showMessageInformation("Lưu thành công: " + list.Count + " Item.");
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e);
                MessageBox.Show("Lưu thất bại!", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private object[] nodes(dynamic b)
        {
            int i = 0;
            string[] a = {
                    b.id,
                    b.title,
                    b.nguoiLienHe,
                    b.sdt,
                    b.diachiLienHe,
                    b.email,
                    b.addressCty,
                    b.nganhNghe,
                };
            object[] nodes = arrayHeader.Select(e => new XElement(e.Replace(' ', '-'), a[i++])).ToArray<object>();

            return nodes;
        }

        public override LinkedList<dynamic> getLinkByCustomListCategory(SearchTypes types)
        {
            return null;
        }
    }
}
