﻿using CrawlerWeb.model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Excel = Microsoft.Office.Interop.Excel;

namespace CrawlerWeb
{

    class ExcelAction
    {
        public void saveExcel(ICollection<dynamic> jobs)
        {

            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "Các tệp excel|*.xlsx|Tất cả các tệp|*.*";
            saveFileDialog.ShowDialog();

            if (saveFileDialog.FileName != "")
            {
                save(saveFileDialog.FileName, jobs);
            }
        }

        public void saveExcelJobDetail(Action<Excel._Worksheet> callback)
        {

            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "Các tệp excel|*.xlsx|Tất cả các tệp|*.*";
            saveFileDialog.ShowDialog();

            if (saveFileDialog.FileName != "")
            {
                saveJobDetailCustom(saveFileDialog.FileName, callback);
            }
        }

        public void save(string fileDes, ICollection<dynamic> jobs)
        {
            Excel.Application app = new Excel.Application();
            Excel.Workbook wb = app.Workbooks.Add(Type.Missing);
            //Tạo sheet
            Excel._Worksheet sheet = null;
            try
            {
                sheet = wb.ActiveSheet;
                sheet.Name = "Dữ liệu xuất ra";

                int i = 0; ;
                foreach(var e in jobs)
                {
                    if(e is LinkSpa)
                    {
                        sheet.Cells[i + 2, 1] = e.title;
                        sheet.Cells[i + 2, 2] = e.href;
                        sheet.Cells[i + 2, 3] = e.category;
                    }
                    else
                    {
                        sheet.Cells[i + 2, 1] = "'"+ e.id;
                        sheet.Cells[i + 2, 2] = e.title;
                        sheet.Cells[i + 2, 3] = e.href;
                    }
                    i++;
                }

                wb.SaveAs(fileDes);
                MessageBox.Show("Lưu thành công!", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);

                wb.Close();
                Marshal.ReleaseComObject(wb);
                //quit and release
                app.Quit();
                Marshal.ReleaseComObject(app);
            }
            catch (Exception e)
            {
                MessageBox.Show("Lưu thất bại!", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                wb.Close();
                Marshal.ReleaseComObject(wb);
                //quit and release
                app.Quit();
                Marshal.ReleaseComObject(app);
                System.Diagnostics.Debug.WriteLine(e.Message);
            }
        }

        public void saveJobDetailCustom(string fileDes, Action<Excel._Worksheet> sheetCallback)
        {
            Excel.Application app = new Excel.Application();
            Excel.Workbook wb = app.Workbooks.Add(Type.Missing);
            //Tạo sheet
            Excel._Worksheet sheet = null;
            try
            {
                sheet = wb.ActiveSheet;
                sheet.Name = "DATA";
                sheet.Cells.NumberFormat = "@";

                //Tùy chỉnh thêm data
                sheetCallback(sheet);
               
                wb.SaveAs(fileDes);
                MessageBox.Show("Lưu thành công!", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);

                wb.Close();
                Marshal.ReleaseComObject(wb);
                //quit and release
                app.Quit();
                Marshal.ReleaseComObject(app);
            }
            catch (Exception e)
            {
                MessageBox.Show("Lưu thất bại!", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                wb.Close();
                Marshal.ReleaseComObject(wb);
                //quit and release
                app.Quit();
                Marshal.ReleaseComObject(app);
                System.Diagnostics.Debug.WriteLine(e);
            }
        }


        //Open link excel
        public ICollection<dynamic> ActionOpenExcel(string fileName)
        {
            ICollection<dynamic> jobs = new List<dynamic>();

            if (fileName != "")
            {
                Excel.Application app = new Excel.Application();
                Excel.Workbook wb = app.Workbooks.Open(fileName);

                try
                {
                    Excel._Worksheet sheet = wb.Sheets[1];
                    Excel.Range range = sheet.UsedRange;
                    //đọc dữ liệu
                    int rows = range.Rows.Count;
                    int cols = range.Columns.Count;

                    //Đọc các dòng
                    for (int i = 1; i <= rows; i++)
                    {
                        string id = range.Cells[i, 1].Value;
                        string title = range.Cells[i, 2].Value;
                        string href = range.Cells[i, 3].Value;

                        Job job = new Job(id, title, href);
                        jobs.Add(job);
                    }
                    wb.Close();
                    Marshal.ReleaseComObject(wb);
                    //quit and release
                    app.Quit();
                    Marshal.ReleaseComObject(app);
                    return jobs;
                }
                catch (Exception e)
                {
                    wb.Close();
                    Marshal.ReleaseComObject(wb);
                    //quit and release
                    app.Quit();
                    Marshal.ReleaseComObject(app);
                    MessageBox.Show("Có lỗi xảy ra! \n" + e.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show("File không tồn tại!", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return jobs;
        }

        //Open link excel
        public ICollection<dynamic> ActionOpenExcelImg()
        {
            ICollection<dynamic> list = new List<dynamic>();

            OpenFileDialog fileDialog = new OpenFileDialog();
            fileDialog.Filter = "Excel files (*.xlsx)|*.xlsx|All files (*.*)|*.*";
            fileDialog.ShowDialog();
            string fileName = fileDialog.FileName;

            if (fileName != "")
            {
                Excel.Application app = new Excel.Application();
                Excel.Workbook wb = app.Workbooks.Open(fileName);

                try
                {
                    Excel._Worksheet sheet = wb.Sheets[1];
                    Excel.Range range = sheet.UsedRange;
                    //đọc dữ liệu
                    int rows = range.Rows.Count;
                    int cols = range.Columns.Count;

                    //Đọc các dòng
                    for (int i = 2; i <= rows; i++)
                    {
                        string id = range.Cells[i, 1].Value;
                        string title = range.Cells[i, 2].Value;
                        string tenSanPham = range.Cells[i, 3].Value;
                        string link_img = range.Cells[i, 4].Value;
                        Item job = new Item() {
                            id = id,
                            title = title,
                            nameSP = tenSanPham,
                            href = link_img,
                        };
                        list.Add(job);
                    }
                    wb.Close();
                    Marshal.ReleaseComObject(wb);
                    //quit and release
                    app.Quit();
                    Marshal.ReleaseComObject(app);
                    return list;
                }
                catch (Exception e)
                {
                    wb.Close();
                    Marshal.ReleaseComObject(wb);
                    //quit and release
                    app.Quit();
                    Marshal.ReleaseComObject(app);
                    MessageBox.Show("Có lỗi xảy ra! \n" + e.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show("File không tồn tại!", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return list;
        }
    }
}
