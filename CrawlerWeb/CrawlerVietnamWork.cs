﻿using CrawlerWeb.model;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Linq;
using Excel = Microsoft.Office.Interop.Excel;

namespace CrawlerWeb
{
    class CrawlerVietnamWork : Crawler
    {
        private string[] arrayHeader = {"ID", "Vị trí", "Tên công ty", "Ngày cập nhật", "Địa chỉ",
                                                    "Quy mô công ty", "Người liên hệ",
                                                    "Lương", "Phúc lợi",
                                                   "Ngành nghề", "Cấp bậc", "Kỹ Năng", "Ngôn ngữ",
                                                   "Hạn nộp CV", "Mô tả",
                                                   "Yêu cầu công việc", "Lượt xem"};
        public void getJobDetailWithXML(ICollection<dynamic> list, bool autoSave, string fileDes)
        {
            LinkedList<JobDetail> listDetail = new LinkedList<JobDetail>();
            var isLogin = loginVietNamWork(chromeDriver);

            if (isLogin)
            {
                try
                {
                    foreach (Job job in list)
                    {
                        crawler(job, jobDetail =>
                        {
                            listDetail.AddLast(jobDetail);
                        });

                    };

                    chromeDriver.Quit();
                    if (autoSave)
                    {

                        saveXML(listDetail, fileDes);
                    }
                    else
                    {
                        SaveFileDialog dialog = Util.chooseAddressSaveXML();
                        if (!dialog.FileName.Equals(""))
                            saveXML(listDetail, dialog.FileName);

                    }
                }
                catch (Exception e)
                {
                    chromeDriver.Quit();
                    if (autoSave)
                    {

                        saveXML(listDetail, fileDes);
                    }
                    else
                    {
                        SaveFileDialog dialog = Util.chooseAddressSaveXML();
                        if (!dialog.FileName.Equals(""))
                            saveXML(listDetail, dialog.FileName);

                    }
                    System.Diagnostics.Debug.WriteLine(e);
                    return;
                }
            }
            else
            {
                Util.showMessageError("Đăng nhập thất bại!");
            }
        }

        public void getJobDetail(ICollection<dynamic> list, bool autoSave, string fileDes)
        {
            var isLogin = loginVietNamWork(chromeDriver);
            ExcelAction excel = new ExcelAction();
            LinkedList<JobDetail> listDetail = new LinkedList<JobDetail>();

            if (isLogin)
            {
                    foreach(Job job in list)
                    {
                        crawler(job, jobDetail =>
                        {
                            listDetail.AddLast(jobDetail);

                        });

                    };

                chromeDriver.Quit();
                if (autoSave)
                {
                    excel.saveJobDetailCustom(fileDes, sheet => {
                        insertData(sheet, listDetail);
                    });
                }
                else
                {
                    excel.saveExcelJobDetail(sheet =>
                    {
                        insertData(sheet, listDetail);
                    });
                }
                
            }
            else
            {
                MessageBox.Show("Đăng nhập thất bại!", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            
        }

        private void insertData(Excel._Worksheet sheet, LinkedList<JobDetail> listDetail)
        {
           
            for (int h = 1; h <= arrayHeader.Length; h++)
            {
                sheet.Cells[1, h] = arrayHeader[h - 1];
            }

            int i = 0;
            foreach (var b in listDetail)
            {
                string[] a = {
                    b.id, b.title, b.nameCty,b.ngayCapNhat,
                    b.addressCty,
                    b.quyMoCty,
                    b.nguoiLienHe,
                    b.luong,
                    b.phucLoi,
                    b.nganhNghe,
                    b.capBac,
                    b.kyNang,
                    b.ngonNgu,
                    b.hanNopCV,
                    b.motaCV,
                    b.yeucauCV,
                     b.luotXem};
                for (int j = 1; j <= a.Length; j++)
                {
                    sheet.Cells[i + 2, j].HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;
                    sheet.Cells[i + 2, j] = a[j - 1];
                }
                i++;
            }
        }

        override
        public LinkedList<dynamic> getLinkByCustomListCategory(SearchTypes searchTypes)
        {
            string url = "https://www.vietnamworks.com/tim-viec-lam/tat-ca-viec-lam";

            LinkedList<dynamic> list = new LinkedList<dynamic>();

            try
            {
                chromeDriver.Navigate().GoToUrl(url);

                //Location address
                var btnSelectLocation = chromeDriver.FindElement(By.XPath("//*[@id=\"s2id_autogen20\"]/a"));
                btnSelectLocation.Click();

                var inputAddress = getDataWebEmlement(By.XPath("//*[@id=\"s2id_autogen21_search\"]"));
                inputAddress.SendKeys(searchTypes.address + OpenQA.Selenium.Keys.Enter);
                //end location address

                //Action with salary
                var btnSalary = getDataWebEmlements(By.ClassName("header-numeric-refinement-values"))[0];
                btnSalary.Click();

                Thread.Sleep(200);
                var listSalary = getDataWebEmlements(By.ClassName("radio"));
                foreach (var e in listSalary)
                {
                    if (searchTypes.salary.Equals(e.Text))
                    {
                        e.Click();
                    }
                }
                btnSalary.Click();
                getAllLinkJob(e =>
                {
                    list = e;
                });
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e);
                return list;
            }

            return list;


        }


        public LinkedList<dynamic> getLinkByCustom(string address, string salary)
        {
            string url = "https://www.vietnamworks.com/tim-viec-lam/tat-ca-viec-lam";

            LinkedList<dynamic> list = new LinkedList<dynamic>();

            try
            {
                chromeDriver.Navigate().GoToUrl(url);

                //Location address
                var btnSelectLocation = chromeDriver.FindElement(By.XPath("//*[@id=\"s2id_autogen20\"]/a"));
                btnSelectLocation.Click();

                var inputAddress = getDataWebEmlement(By.XPath("//*[@id=\"s2id_autogen21_search\"]"));
                inputAddress.SendKeys(address + OpenQA.Selenium.Keys.Enter);
                //end location address

                //Action with salary
                var btnSalary = getDataWebEmlements(By.ClassName("header-numeric-refinement-values"))[0];
                btnSalary.Click();

                Thread.Sleep(200);
                var listSalary = getDataWebEmlements(By.ClassName("radio"));
                foreach (var e in listSalary)
                {
                    if (salary.Equals(e.Text))
                    {
                        e.Click();
                    }
                }
                btnSalary.Click();
                getAllLinkJob(e =>
                {
                    list = e;
                });
                chromeDriver.Quit();
            }
            catch(Exception e)
            {
                chromeDriver.Quit();
                System.Diagnostics.Debug.WriteLine(e);
                return list;
            }
            
            return list;


        }

        int i = 1;

        public void getAllLinkJob(Action<LinkedList<dynamic>> callback)
        {
            LinkedList<dynamic> listAddress = new LinkedList<dynamic>();
            IWebElement aNext1 = null;
            int page = 0;
            try
            {
                while (true)
                {
                    try
                    {
                        Thread.Sleep(1000);
                        var jobItems = getDataWebEmlements(By.ClassName("job-item-info"));
                        foreach (var job in jobItems)
                        {
                            string title = job.FindElement(By.TagName("h3")).Text;
                            string href = job.FindElement(By.TagName("a")).GetAttribute("href");
                            listAddress.AddLast(new Job(i++.ToString(), title, href));
                        }
                        aNext1 = getDataWebEmlement(By.ClassName("ais-pagination--item__next")).FindElement(By.TagName("a"));

                    }
                    catch (NoSuchElementException e)
                    {
                        aNext1 = new EmptyElement();
                    }
                    catch (StaleElementReferenceException e)
                    {
                        continue;
                    }
                    if (aNext1 is EmptyElement)
                    {
                        aNext1 = getDataWebEmlement(By.XPath("//*[@id=\"pagination-container\"]/div/ul/li[11]/span"));
                    }
                    if (aNext1 is EmptyElement)
                    {
                        callback(listAddress);
                        return;
                    }
                    else
                    {
                        aNext1.Click();
                        page++;
                    }

                    //Cài đặt bao nhiêu trang là thoát
                    if (page == numberPages && numberPages > 0)
                    {
                        callback(listAddress);
                        return;
                    }
                }
            }
            catch(Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.Message);
                callback(listAddress);
                return;
            }
           

        }

        public bool loginVietNamWork(IWebDriver chromeDriver)
        {
            chromeDriver.Navigate().GoToUrl("https://www.vietnamworks.com/thoat");
            Thread.Sleep(1000);
            var btnLogin = getDataWebEmlement(By.XPath("/html/body/div[2]/header/nav/div/div[2]/ul[2]/li[5]/a"));
            if(btnLogin is EmptyElement)
            {
                return false;
            }
            btnLogin.Click();
            Thread.Sleep(1000);
            var inputEmail = getDataWebEmlement(By.Id("email"));
            inputEmail.SendKeys("nguyenwipwa@gmail.com");
            var inputPasswork = getDataWebEmlement(By.Id("login__password"));
            inputPasswork.SendKeys("trong2010" + OpenQA.Selenium.Keys.Enter);


            WebDriverWait wait = new WebDriverWait(chromeDriver, new TimeSpan(0, 0, 10));
            // var input = wait.Until(dr => dr.FindElement(By.XPath("//*[@id='tsf']/div[2]/div[1]/div[2]/div/div[1]/input")));
            var userId = wait.Until(dr => dr.FindElement(By.ClassName("full-name")));
            Console.WriteLine(userId.Text);
            return userId != null;
        }

        override
        public void crawler(Job job, Action<JobDetail> callback)
        {
            chromeDriver.Navigate().GoToUrl(job.href);
            try
            {
                string title = getDataWebEmlement(By.XPath("//*[@id=\"wrapper\"]/div[3]/div[5]/section[1]/div/div[2]/div[2]/div/div[1]/div/h1")).Text;
                string nameCty = getDataWebEmlement(By.XPath("//*[@id=\"wrapper\"]/div[3]/div[5]/section[1]/div/div[2]/div[2]/div/div[1]/div/div[1]/div/a")).Text;
                string ngayCapNhat = getDataWebEmlement(By.XPath("//*[@id=\"job-info\"]/div/div[1]/div/div[1]/div[1]/div[2]/span[2]")).Text;

               

                string luong = getDataWebEmlement(By.XPath("//*[@id=\"wrapper\"]/div[3]/div[5]/section[1]/div/div[2]/div[2]/div/div[1]/div/div[2]/div/span[1]")).Text;
                string phucLoi = getDataWebEmlement(By.XPath("//*[@id=\"job-info\"]/div/div[2]/div[1]/div")).Text;
                string luotXem = getDataWebEmlement(By.XPath("//*[@id='wrapper']/div[3]/div[5]/section[1]/div/div[2]/div[2]/div/div[1]/div/div[2]/div/span[2]")).Text;


                string nganhNghe = getDataWebEmlement(By.XPath("//*[@id=\"job-info\"]/div/div[1]/div/div[1]/div[3]/div[2]/span[2]")).Text;
                string capBac = getDataWebEmlement(By.XPath("//*[@id=\"job-info\"]/div/div[1]/div/div[1]/div[2]/div[2]/span[2]")).Text;
                string kyNang = getDataWebEmlement(By.XPath("//*[@id=\"job-info\"]/div/div[1]/div/div[1]/div[4]/div[2]/span[2]")).Text;
                string ngonNgu = getDataWebEmlement(By.XPath("//*[@id=\"job-info\"]/div/div[1]/div/div[1]/div[5]/div[2]/span[2]")).Text;



                string hanNopCV = getDataWebEmlement(By.XPath("//*[@id=\"wrapper\"]/div[3]/div[5]/section[1]/div/div[2]/div[2]/div/div[1]/div/div[2]/div/span[4]")).Text;
                string motaCV = getDataWebEmlement(By.XPath("//*[@id=\"job-info\"]/div/div[2]/div[2]/div")).Text;
                string yeucauCV = getDataWebEmlement(By.XPath("//*[@id=\"job-info\"]/div/div[2]/div[3]/div")).Text;

                var a = getDataWebEmlement(By.XPath("//*[@id=\"wrapper\"]/div[3]/div[5]/section[2]/ul/li[2]/a"));
                if(!(a is EmptyElement))
                {
                    a.Click();
                    Thread.Sleep(1000);
                }

                string addressCty = getDataWebEmlement(By.XPath("//*[@id=\"company-info\"]/div/div[2]/div/div/div[1]/div[2]/span[2]")).Text;
                string quyMoCty = getDataWebEmlement(By.XPath("//*[@id=\"company-info\"]/div/div[2]/div/div/div[2]/div[2]/span[2]")).Text;
                string nguoiLienHe = getDataWebEmlement(By.XPath("//*[@id=\"company-info\"]/div/div[2]/div/div/div[3]/div[2]/span[2]")).Text;


                System.Diagnostics.Debug.WriteLine(title + "\t" + ngayCapNhat + "\t" + nameCty + "\t" + addressCty + "\t"+ quyMoCty + "\t");
                //System.Diagnostics.Debug.WriteLine(nganhNghe + "\t" + capBac + "\t" + luong);
                //System.Diagnostics.Debug.WriteLine(hanNopCV + "\t" + motaCV + "\t" + yeucauCV + "\t");

                JobDetail jobDetail = new JobDetail()
                {
                    id = job.id,
                    title = title,
                    nameCty = nameCty,
                    ngayCapNhat = ngayCapNhat,
                    addressCty = addressCty,
                    quyMoCty = quyMoCty,
                    nguoiLienHe = nguoiLienHe,
                    luong = luong,
                    phucLoi = phucLoi,
                    nganhNghe = nganhNghe,
                    capBac = capBac,
                    kyNang = kyNang,
                    ngonNgu = ngonNgu,
                    hanNopCV = hanNopCV,
                    motaCV = motaCV,
                    yeucauCV = yeucauCV,
                    luotXem = luotXem,
                };
                callback(jobDetail);
            }
            catch (Exception e)
            {

                System.Diagnostics.Debug.WriteLine(e.Message);
                crawler(job, callback);
            }            
        }

        //XML

        override
        public void saveXML(ICollection<JobDetail> list, string desc)
        {
            try
            {
                int id = 1;
                System.Diagnostics.Debug.WriteLine("Bat dau luu! XML ");
                XElement xmlElements = new XElement("data", list.Select(i => new XElement("Item", nodes(i, id++))));

                xmlElements.Save(desc + ".xml");

                System.Diagnostics.Debug.WriteLine("Xong!");
                Util.showMessageInformation("Lưu thành công: " + list.Count + " Item.");
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e);
                MessageBox.Show("Lưu thất bại!", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private object[] nodes(dynamic b, int id)
        {
            int i = 0;
            string[] a = {
                    
                    b.id, b.title, b.nameCty,b.ngayCapNhat,
                    b.addressCty,
                    b.quyMoCty,
                    b.nguoiLienHe,
                    b.luong,
                    b.phucLoi,
                    b.nganhNghe,
                    b.capBac,
                    b.kyNang,
                    b.ngonNgu,
                    b.hanNopCV,
                    b.motaCV,
                    b.yeucauCV,
                    b.luotXem};

            object[] nodes = arrayHeader.Select(e => new XElement(e.Replace(' ', '-'), a[i++])).ToArray<object>();

            return nodes;
        }

    }
}
