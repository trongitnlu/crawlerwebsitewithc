﻿using CrawlerWeb.model;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Linq;
using Excel = Microsoft.Office.Interop.Excel;

namespace CrawlerWeb
{
    class CrawlerTrangVangVietNam : Crawler
    {

       private string[] arrayHeader = {"ID", "Tên công ty","Địa chỉ", "Số điện thoại", "Fax", "Thị trường",
                    "Loại hình công ty", "Email", "Website", "Giới thiệu công ty",
                "Ngành nghề kinh doanh", "Sản phẩm dịch vụ", "Năm thành lập",
                "Số lượng nhân viên", "Tỉnh-Thành phố", "Mã số thuế", "Họ tên liên hệ", "Chức vụ","Di động","Email Liên hệ"};
       private string[] arrayHeaderImg = {"ID", "Tên công ty", "Tên sản phẩm", "Url image" };
        public void getAllLinkItem(string link,Action<LinkedList<dynamic>> callback)
        {
        
            LinkedList<dynamic> listAddress = new LinkedList<dynamic>();
            IWebElement aNext1 = null;
            int page = 0;
            int maxPage = 1;
            var arrA = getDataWebEmlement(By.Id("paging")).FindElements(By.TagName("a"));

            maxPage = int.Parse(arrA[arrA.Count - 2].Text);
            System.Diagnostics.Debug.WriteLine("Số trang: " + maxPage);

            while (page < maxPage)
            {

                var linkItems = getDataWebEmlements(By.ClassName("company_name"));
                System.Diagnostics.Debug.WriteLine("Tong so: " + linkItems.Count);
                foreach (var e in linkItems)
                {
                    string title = e.FindElement(By.TagName("a")).Text;
                    string href = e.FindElement(By.TagName("a")).GetAttribute("href");
                    listAddress.AddLast(new Job(id++.ToString(), title, href));
                }
            try
            {
                aNext1 = getDataWebEmlement(By.XPath("//a[contains(text(),'Tiếp')]"));

            }
            catch (NoSuchElementException e)
            {
                aNext1 = new EmptyElement();
            }

            if (aNext1.GetAttribute("href") == null || aNext1.GetAttribute("href").Equals("") || aNext1 is EmptyElement)
            {
                callback(listAddress);
                return;
            }
            else
            {
                System.Diagnostics.Debug.WriteLine("click: " + page);
                page++;
                aNext1.Click();
            }

                //Cài đặt bao nhiêu trang là thoát
                if (page == numberPages && numberPages > 0)
                {
                    callback(listAddress);
                    return;
                }

            }
                callback(listAddress);
                return;
    }

        public LinkedList<dynamic> getLinkByCustom(string href)
        {
            LinkedList<dynamic> listLink = new LinkedList<dynamic>();
            try
            {

                chromeDriver.Navigate().GoToUrl(href);

                getAllLinkItem(href, e =>
                {
                    listLink = e;
                });
                chromeDriver.Quit();
            }
            catch (Exception e)
            {
                chromeDriver.Quit();
                System.Diagnostics.Debug.WriteLine(e);
                return listLink;
            }

            return listLink;
        }

        int id = 1;
        //Những link được add vào gridview
        public LinkedList<dynamic> getLinkByCustom(List<dynamic> list)
        {
            LinkedList<dynamic> listLink = new LinkedList<dynamic>();
            try
            {

                foreach(Job e in list) {
                    chromeDriver.Navigate().GoToUrl(e.href);

                    getAllLinkItem(e.href, l =>
                    {
                        foreach(var r in l)
                        {
                            listLink.AddLast(r);
                        }
                    });
                };
               
                chromeDriver.Quit();
            }
            catch (Exception e)
            {
                chromeDriver.Quit();
                System.Diagnostics.Debug.WriteLine(e);
                return listLink;
            }

            return listLink;
        }

        public LinkedList<dynamic> getCategory()
        {
            LinkedList<dynamic> list = new LinkedList<dynamic>();
            try
            {
                string url = "https://trangvangvietnam.com/findex.asp?page="+ numberPages;

                chromeDriver.Navigate().GoToUrl(url);
                
                getAllLinkCategory(e =>
                {
                    list = e;
                });
                chromeDriver.Quit();
            }
            catch(Exception e)
            {
                chromeDriver.Quit();
                System.Diagnostics.Debug.WriteLine(e);
                return list;
            }

            return list;
        }

        public void getAllLinkCategory(Action<LinkedList<dynamic>> callback)
        {
            LinkedList<dynamic> categorys = new LinkedList<dynamic>();
            var linkItems = getDataWebEmlement(By.Id("khung_subpages")).FindElements(By.CssSelector("a[style='color:#00C']"));
            System.Diagnostics.Debug.WriteLine("Tong " + +linkItems.Count);
            int i = 1;
            foreach (var link in linkItems)
            {
                string title = link.Text.Trim();
                string href = link.GetAttribute("href");
                Job job = new Job(i++.ToString(), title, href);
                categorys.AddLast(job);
            }

            callback(categorys);
            return;

        }

        public void getJobDetail(ICollection<dynamic> list, bool autoSave, string fileDes)
        {
                ExcelAction excel = new ExcelAction();
                LinkedList<JobDetail> listDetail = new LinkedList<JobDetail>();
            try
            {
                foreach (Job job in list)
                {
                    crawler(job, jobDetail =>
                    {
                        listDetail.AddLast(jobDetail);
                    });

                };

                chromeDriver.Quit();
                if (autoSave)
                {
                    excel.saveJobDetailCustom(fileDes, sheet => {
                        insertData(sheet, listDetail);
                    });

                }
                else
                {
                    excel.saveExcelJobDetail(sheet =>
                    {
                        insertData(sheet, listDetail);
                    });

                }
            }
            catch (Exception e)
            {
                chromeDriver.Quit();
                if (autoSave)
                {
                    excel.saveJobDetailCustom(fileDes, sheet => {
                        insertData(sheet, listDetail);
                    });

                }
                else
                {
                    excel.saveExcelJobDetail(sheet =>
                    {
                        insertData(sheet, listDetail);
                    });

                }
                System.Diagnostics.Debug.WriteLine(e);
                return;
            }
        }

        private string ExtractString(string s)
        {
            // You should check for errors in real-world code, omitted for brevity
            var startTag = "(";
            int startIndex = s.IndexOf(startTag) + startTag.Length;
            int endIndex = s.IndexOf(")", startIndex);
            return s.Substring(startIndex, endIndex - startIndex).Trim();
        }

        public ICollection<dynamic> filterLinkByDate(ICollection<dynamic> list)
        {
            LinkedList<dynamic> listLink = new LinkedList<dynamic>();
            try
            {
                foreach (Job job in list)
                {
                    try
                    {
                        chromeDriver.Navigate().GoToUrl(job.href);

                        var strDateWeb = getDataWebEmlement(By.XPath("//div[contains(text(),'Cập nhật ngày:')]")).Text;
                        Console.WriteLine(strDateWeb + " - " + strDate);
                        if(strDate!=null || !strDate.Equals("Null"))
                        {
                            var a = ExtractString(strDateWeb).Split('/');
                            var b = strDate.Split('/');

                            var date1 = new DateTime(int.Parse(a[2]), int.Parse(a[1]), int.Parse(a[0]));
                            var date2 = new DateTime(int.Parse(b[2]), int.Parse(b[1]), int.Parse(b[0]));
                            int result = DateTime.Compare(date1, date2);
                            if (result >= 0)
                            {
                                listLink.AddLast(job);
                            }
                        }
                    }catch(Exception e)
                    {
                        Console.WriteLine(e.Message);
                    }

                };
                chromeDriver.Quit();
                
            }catch(Exception e)
              {
                Console.WriteLine(e.Message);
                chromeDriver.Quit();
              }
            return listLink;
        }

        private void insertData(Excel._Worksheet sheet, LinkedList<JobDetail> listDetail)
        {
            
            for (int h = 1; h <= arrayHeader.Length; h++)
            {
                sheet.Cells[1, h] = arrayHeader[h - 1];
            }

            int i = 0;
            System.Diagnostics.Debug.WriteLine(listDetail.Count);
            foreach (var b in listDetail)
            {
                string[] a = {
                                b.id,
                                b.title        ,
                                b.diachiLienHe ,
                                b.sdt          ,
                                b.fax          ,
                                b.thiTruong    ,
                                b.loaiHinhCty  ,
                                b.email        ,
                                b.website      ,
                                b.gioiThieuCty ,
                                b.nganhNghe    ,
                                b.sanPhamDichVu,
                                b.namThanhLap,
                                b.quyMoCty,
                                b.thanhPho,
                                b.mst,
                                b.hotenLH,
                                b.chucVu,
                                b.didong,
                                b.emailLH,
                               };

                for (int j = 1; j <= a.Length; j++)
                {
                    sheet.Cells[i + 2, j].NumberFormat = "@";
                    sheet.Cells[i + 2, j].HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;
                    sheet.Cells[i + 2, j] = a[j - 1];
                }
                i++;
            }
        }

        override
        public void crawler(Job job, Action<JobDetail> callback)
        {
            chromeDriver.Navigate().GoToUrl(job.href);
            try
            {
                string title = getDataWebEmlement(By.ClassName("tencongty")).Text;
                string diachi = getDataWebEmlement(By.ClassName("diachi_chitiet_li2dc")).Text;
                string thanhPho = getDataWebEmlement(By.ClassName("diachi_chitiet_li2dc")).FindElement(By.TagName("strong")).Text;

                string sdt = sliptCharacter(getDataWebEmlement(By.XPath("//div[contains(text(),'Tel:')]/parent::*")).Text, ':');
                string fax = sliptCharacter(getDataWebEmlement(By.XPath("//div[contains(text(),'Fax:')]/parent::*")).Text, ':');
                string thiTruong = sliptCharacter(getDataWebEmlement(By.XPath("//div[contains(text(),'Thị trường chính:')]/parent::*/parent::*")).Text, ':');
                string loaiHinhCty = sliptCharacter(getDataWebEmlement(By.XPath("//div[contains(text(),'Loại hình công ty:')]/parent::*/parent::*")).Text, ':');
                string namThanhLap = sliptCharacter(getDataWebEmlement(By.XPath("//div[contains(text(),'Năm thành lập:')]/parent::*/parent::*")).Text, ':');
                string quyMoCT = sliptCharacter(getDataWebEmlement(By.XPath("//div[contains(text(),'Số lượng nhân viên:')]/parent::*/parent::*")).Text, ':');

                //Thong tin lien he
                string mst = sliptCharacter(getDataWebEmlement(By.XPath("//div[contains(text(),'Mã số thuế:')]/parent::*/parent::*")).Text, ':');
                string hoten = sliptCharacter(getDataWebEmlement(By.XPath("//p[contains(text(),'Họ tên:')]/parent::*/parent::*")).Text, ':');
                string chucvu = sliptCharacter(getDataWebEmlement(By.XPath("//p[contains(text(),'Chức vụ:')]/parent::*/parent::*")).Text, ':');
                string didong = sliptCharacter(getDataWebEmlement(By.XPath("//p[contains(text(),'Di động:')]/parent::*/parent::*")).Text, ':');
                string emailLH = sliptCharacter(getDataWebEmlement(By.XPath("//p[contains(text(),'Email:')]/parent::*/parent::*")).Text, ':');

                string email = getDataWebEmlement(By.ClassName("text_email")).Text;
                string website = getDataWebEmlement(By.ClassName("text_website")).Text;
                string gioiThieuCongty = getDataWebEmlement(By.ClassName("gioithieucongty")).Text;
                string nganhNgheKinhDoanh = getListDataText(By.ClassName("nganhnghe_chitiet_li"));
                string sanPhamDichVu = getListDataText(By.ClassName("sanphamdichvu_phannhom_box"));
                if (sanPhamDichVu.Equals(""))
                {
                    sanPhamDichVu = getListDataText(By.ClassName("sanphamdichvu_khongphannhom"));
                }

                System.Diagnostics.Debug.WriteLine(title + "\t" + sdt + "\t" + email);

                    JobDetail jobDetail = new JobDetail()
                    {
                        id              = job.id,
                        title           = title,
                        diachiLienHe    = diachi,
                        sdt             = sdt,
                        fax             = fax,
                        thiTruong       = thiTruong,
                        loaiHinhCty     = loaiHinhCty,
                        email           = email,
                        website         = website,
                        gioiThieuCty    = gioiThieuCongty,
                        nganhNghe       = nganhNgheKinhDoanh,
                        sanPhamDichVu   = sanPhamDichVu,
                        namThanhLap     = namThanhLap,
                        quyMoCty       = quyMoCT,
                        thanhPho        = thanhPho,
                        mst             = mst,
                        hotenLH         = hoten,
                        chucVu          = chucvu,
                        didong          = didong,
                        emailLH         = emailLH,


                    };

                    callback(jobDetail);
                
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e);
                return;
            }

        }

        public void crawlerLinkPicture(Job job, Action<dynamic> callback)
        {
            chromeDriver.Navigate().GoToUrl(job.href);
            try
            {
                string title = getDataWebEmlement(By.ClassName("tencongty")).Text;
                var listItems = getDataWebEmlement(By.ClassName("thuvienhinhanh")).FindElements(By.TagName("img"));

                foreach(IWebElement e in listItems)
                {
                    Item jobDetail = new Item()
                    {
                        id = job.id,
                        title = title,
                        nameSP = e.GetAttribute("alt"),
                        href = e.GetAttribute("src"),
                    };
                    callback(jobDetail);
                }

            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e);
                return;
            }

        }

        public ICollection<dynamic> getAllLinkImgDetail(ICollection<dynamic> list, bool autoSave, string fileDes)
        {
            ExcelAction excel = new ExcelAction();
            LinkedList<dynamic> listDetail = new LinkedList<dynamic>();
            try
            {
                foreach (Job job in list)
                {
                    crawlerLinkPicture(job, jobDetail =>
                    {
                        listDetail.AddLast(jobDetail);
                    });

                };
                chromeDriver.Quit();

                if (autoSave)
                {
                    excel.saveJobDetailCustom(fileDes, sheet => {
                        insertData(sheet, listDetail);
                    });
                }
                else
                {
                    excel.saveExcelJobDetail(sheet =>
                    {
                        insertData(sheet, listDetail);
                    });
                }
            }
            catch (Exception e)
            {
                chromeDriver.Quit();

                if (autoSave)
                {
                    excel.saveJobDetailCustom(fileDes, sheet => {
                        insertData(sheet, listDetail);
                    });
                }
                else
                {
                    excel.saveExcelJobDetail(sheet =>
                    {
                        insertData(sheet, listDetail);
                    });
                }
                System.Diagnostics.Debug.WriteLine(e);
            }
            return listDetail;
        }

        public ICollection<dynamic> getAllLinkImgDetailWithXML(ICollection<dynamic> list, bool autoSave, string fileDes)
        {
            ExcelAction excel = new ExcelAction();
            LinkedList<dynamic> listDetail = new LinkedList<dynamic>();
            try
            {
                foreach (Job job in list)
                {
                    crawlerLinkPicture(job, jobDetail =>
                    {
                        listDetail.AddLast(jobDetail);
                    });

                };
                chromeDriver.Quit();

                if (autoSave)
                {
                    saveLinkImgXML(listDetail, fileDes);
                }
                else
                {
                    SaveFileDialog dialog = new SaveFileDialog();
                    dialog.ShowDialog();
                    if (!dialog.FileName.Equals(""))
                    {
                        saveLinkImgXML(listDetail, dialog.FileName);
                    }
                    else
                    {
                        MessageBox.Show("Đường dẫn sai!", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
            catch (Exception e)
            {
                chromeDriver.Quit();
                if (autoSave)
                {
                    excel.saveJobDetailCustom(fileDes, sheet => {
                        insertData(sheet, listDetail);
                    });
                }
                else
                {
                    excel.saveExcelJobDetail(sheet =>
                    {
                        insertData(sheet, listDetail);
                    });
                }
                System.Diagnostics.Debug.WriteLine(e);
            }
            return listDetail;
        }

       
        private void insertData(Excel._Worksheet sheet, LinkedList<dynamic> listDetail)
        {
            
            for (int h = 1; h <= arrayHeaderImg.Length; h++)
            {
                sheet.Cells[1, h] = arrayHeaderImg[h - 1];
            }

            int i = 0;
            System.Diagnostics.Debug.WriteLine(listDetail.Count);
            foreach (Item b in listDetail)
            {
                string[] a = {
                                b.id,
                                b.title        ,
                                b.nameSP ,
                                b.href,
                               };

                for (int j = 1; j <= a.Length; j++)
                {
                    sheet.Cells[i + 2, j].NumberFormat = "@";
                    sheet.Cells[i + 2, j].HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;
                    sheet.Cells[i + 2, j] = a[j - 1];
                }
                i++;
            }
        }

        override
        public void saveXML(ICollection<JobDetail> list, string desc)
        {
            try
            {
                System.Diagnostics.Debug.WriteLine("Bat dau luu! XML ");
                XElement xmlElements = new XElement("data", list.Select(i => new XElement("Item", nodes(i))));

                xmlElements.Save(desc + ".xml");

                System.Diagnostics.Debug.WriteLine("Xong!");
                Util.showMessageInformation("Lưu thành công: " + list.Count + " Item.");
            }
            catch(Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e);
                MessageBox.Show("Lưu thất bại!", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
           
        }

        private object[] nodes(dynamic b)
        {
            int i = 0;
            string[] a = {
                                b.id,
                                b.title        ,
                                b.diachiLienHe ,
                                b.sdt          ,
                                b.fax          ,
                                b.thiTruong    ,
                                b.loaiHinhCty  ,
                                b.email        ,
                                b.website      ,
                                b.gioiThieuCty ,
                                b.nganhNghe    ,
                                b.sanPhamDichVu,
                                b.namThanhLap,
                                b.quyMoCty,
                                b.thanhPho,
                                b.mst,
                                b.hotenLH,
                                b.chucVu,
                                b.didong,
                                b.emailLH,
                               };
            object[] nodes = arrayHeader.Select(e => new XElement(e.Replace(' ','-'), a[i++])).ToArray<object>();

            return nodes;
        }

        public void saveLinkImgXML(ICollection<dynamic> list, string desc)
        {
            try
            {
                System.Diagnostics.Debug.WriteLine("Bat dau luu! XML ");
                XElement xmlElements = new XElement("data", list.Select(i => new XElement("Item", nodesImg(i))));

                xmlElements.Save(desc + ".xml");

                System.Diagnostics.Debug.WriteLine("Xong!");
                Util.showMessageInformation("Lưu thành công: " + list.Count + " Item.");
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e);
                MessageBox.Show("Lưu thất bại!", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private object[] nodesImg(Item b)
        {
            int i = 0;
            string[] arrayHeader = {"ID", "title", "nameSP", "href" };
            string[] a = {      b.id,
                                b.title        ,
                                b.nameSP ,
                                b.href,
                               };
            object[] nodes = arrayHeader.Select(e => new XElement(e.Replace(' ', '-'), a[i++])).ToArray<object>();

            return nodes;
        }

        public override LinkedList<dynamic> getLinkByCustomListCategory(SearchTypes types)
        {
            return null;
        }
    }
}
