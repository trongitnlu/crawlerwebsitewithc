﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrawlerWeb.model
{
    class Item
    {
        public string title { get; internal set; }
        public string nameSP { get; internal set; }
        public string href { get; internal set; }
        public string id { get; internal set; }

        public Item()
        {

        }

        public Item(string id, string title, string nameSP, string href)
        {
            this.title = title;
            this.id = id;
            this.nameSP = nameSP;
            this.href = href;
        }
    }
}
