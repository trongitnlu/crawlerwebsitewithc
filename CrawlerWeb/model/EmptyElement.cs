﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Drawing;

namespace CrawlerWeb.model
{
    class EmptyElement : IWebElement
    {
        public string TagName => throw new NotImplementedException();

        public string Text {
            get
            {
                return "Null";
            }
        }

        public bool Enabled { get{ return false;} }

        public bool Selected { get { return false; } }

        public Point Location { get { return new Point(); } }

        public Size Size { get { return new Size(); } }

        public bool Displayed { get { return false; } }
        public void Clear() { }

        public void Click() { }

        public IWebElement FindElement(By by)
        {
            return new EmptyElement();
        }

        public ReadOnlyCollection<IWebElement> FindElements(By by)
        {
            List<IWebElement> list = new List<IWebElement>();
            list.Add(new EmptyElement());
            return new ReadOnlyCollection<IWebElement>(list);
        }

        public string GetAttribute(string attributeName)
        {
            return "Null";
        }

        public string GetCssValue(string propertyName)
        {
            return "Null";
        }

        public string GetProperty(string propertyName)
        {
            return "Null";
        }

        public void SendKeys(string text){ }

        public void Submit() { }
    }
}
