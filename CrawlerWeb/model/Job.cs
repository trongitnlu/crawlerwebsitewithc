﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrawlerWeb.model
{
    class Job
    {
        public string id { get; set; }
        public string title { get; set; }
        public string href { get; set; }

        public Job(string title, string href)
        {
            this.title = title;
            this.href = href;
        }
        public Job(string id, string title, string href)
        {
            this.id = id;
            this.title = title;
            this.href = href;
        }
    }
}
