﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrawlerWeb.model
{
    class Category
    {
        public string category { get; set; }
        public LinkedList<dynamic> linkSpas { get; set; }

        public Category(string category, LinkedList<dynamic> linkSpas)
        {
            this.linkSpas = linkSpas;
            this.category = category;
        }

        public override string ToString()
        {
            return category + " : " + linkSpas.Count;
        }
    }
    class LinkSpa
    {
        public string category { get; set; }
        public string title { get; set; }
        public string href { get; set; }

        public LinkSpa(string category, string title, string href)
        {
            this.category = category;
            this.title = title;
            this.href = href;
        }

    }
}
