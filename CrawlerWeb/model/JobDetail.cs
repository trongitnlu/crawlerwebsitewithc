﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrawlerWeb.model
{
    class JobDetail
    {
        public string title { get; set; }
        public string nameCty { get; set; }
        public string ngayCapNhat { get; set; }
        public string addressCty { get; set; }
        public string quyMoCty { get; set; }
        public string nguoiLienHe { get; set; }
        public string luong { get; set; }
        public string phucLoi { get; set; }
        public string nganhNghe { get; set; }
        public string capBac { get; set; }
        public string kyNang { get; set; }
        public string ngonNgu { get; set; }
        public string hanNopCV { get; set; }
        public string motaCV { get; set; }
        public string yeucauCV { get; set; }
        public string diachiLienHe { get; set; }
        public string kinhNghiem { get; set; }
        public string hinhThucLV { get; set; }
        public string gioiTinh { get; set; }
        public string soluongCT { get; set; }
        public string website { get; set; }
        public string tuoi { get; set; }
        public string sdt { get; set; }
        public string thongTinKhac { get; set; }
        public string chucVu { get; set; }
        public string yeuCauHS { get; set; }
        public string email { get; set; }
        public string fax { get; set; }
        public string thiTruong { get; set; }
        public string loaiHinhCty { get; set; }
        public string gioiThieuCty { get; set; }
        public string sanPhamDichVu { get; set; }
        public string namThanhLap { get; set; }
        public string thanhPho { get; set; }
        public string mst { get; internal set; }
        public string hotenLH { get; internal set; }
        public string didong { get; internal set; }
        public string emailLH { get; internal set; }
        public string id { get; internal set; }
        public string luotXem { get; internal set; }
        public string quocGia { get; internal set; }
        public string ngayLamViec { get; internal set; }
        public string overtime { get; internal set; }
        public string noiLamViec { get; internal set; }
    }
}
