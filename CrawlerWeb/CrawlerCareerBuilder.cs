﻿using CrawlerWeb.model;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Linq;
using Excel = Microsoft.Office.Interop.Excel;

namespace CrawlerWeb
{
    class CrawlerCareerBuilder : Crawler
    {

        private string[] arrayHeader = {
            "ID",
            "Vị trí",
            "Tên công ty",
            "Ngày cập nhật",
            "Hạn nộp",
            "Nơi làm việc",
            "Người liên hệ",
            "Website",
            "Địa chỉ công ty",
            "Lương",
            "Kinh nghiệm",
            "Hình thức làm việc",
            "Giơi tính",
            "Tuổi",
            "Bằng cấp",
            "Chức vụ",
            "Ngành nghề",
            "Mô tả công việc",
            "Yêu cầu công việc",
            "Phúc lợi",
            "Giới thiệu công ty",
            "Quy mô công ty",

        };

        override
        public LinkedList<dynamic> getLinkByCustomListCategory(SearchTypes searchTypes)
        {
            LinkedList<dynamic> list = new LinkedList<dynamic>();

            try
            {

                string url = "https://careerbuilder.vn/vi";

                chromeDriver.Navigate().GoToUrl(url);

                var a = getDataWebEmlement(By.XPath("//*[@id='expand_searchjob2']"));
                a.Click();

                Thread.Sleep(500);

                var inputAddress = getDataWebEmlement(By.XPath("//*[@id='locationJA_chosen']/ul/li/input"));
                inputAddress.Click();
                inputAddress.SendKeys(searchTypes.address + OpenQA.Selenium.Keys.Enter);

                var inputSalary = getDataWebEmlement(By.XPath("//*[@id='salaryJA']"));
                inputSalary.Click();

                for (int i = 0; i < Constant.salaryOfferCareerBuilder.Length; i++)
                {
                    if (searchTypes.salary.Equals(Constant.salaryOfferCareerBuilder[i]))
                    {
                        for (int j = 0; j < i; j++)
                        {
                            inputSalary.SendKeys(OpenQA.Selenium.Keys.ArrowDown);
                        }
                        inputSalary.SendKeys(OpenQA.Selenium.Keys.Enter);
                        break;
                    }
                }

                var btnSearch = getDataWebEmlement(By.XPath("//*[@id='frm_search4']/div[6]/button"));
                btnSearch.Click();
                getAllLinkJob(e =>
                {
                    list = e;
                });
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e);
                return list;
            }

            return list;
        }

        public LinkedList<dynamic> getLinkByCustom(SearchTypes searchTypes)
        {
            LinkedList<dynamic> list = new LinkedList<dynamic>();

            try
            {

                string url = "https://careerbuilder.vn/vi";

                chromeDriver.Navigate().GoToUrl(url);

                var a = getDataWebEmlement(By.XPath("//*[@id='expand_searchjob2']"));
                a.Click();

                Thread.Sleep(500);

                var inputAddress = getDataWebEmlement(By.XPath("//*[@id='locationJA_chosen']/ul/li/input"));
                inputAddress.Click();
                inputAddress.SendKeys(searchTypes.address + OpenQA.Selenium.Keys.Enter);

                var inputSalary = getDataWebEmlement(By.XPath("//*[@id='salaryJA']"));
                inputSalary.Click();

                for(int i=0; i< Constant.salaryOfferCareerBuilder.Length; i++)
                {
                    if (searchTypes.salary.Equals(Constant.salaryOfferCareerBuilder[i]))
                    {
                        for(int j =0; j<i; j++)
                        {
                            inputSalary.SendKeys(OpenQA.Selenium.Keys.ArrowDown);
                        }
                        inputSalary.SendKeys(OpenQA.Selenium.Keys.Enter);
                        break;
                    }
                }

                var btnSearch = getDataWebEmlement(By.XPath("//*[@id='frm_search4']/div[6]/button"));
                btnSearch.Click();
                getAllLinkJob(e =>
                {
                    list = e;
                });
                chromeDriver.Quit();
            }
            catch (Exception e)
            {
                chromeDriver.Quit();
                System.Diagnostics.Debug.WriteLine(e);
                return list;
            }

            return list;
        }
        int id = 1;

        public void getAllLinkJob(Action<LinkedList<dynamic>> callback)
        {
            LinkedList<dynamic> listAddress = new LinkedList<dynamic>();
            IWebElement aNext1 = null;
            int page = 0;
            while (true)
            {
                Thread.Sleep(1000);
                var jobItems = getDataWebEmlements(By.ClassName("job"));
                foreach (var job in jobItems)
                {
                    string title = job.FindElement(By.TagName("a")).Text;
                    string href = job.FindElement(By.TagName("a")).GetAttribute("href");
                    listAddress.AddLast(new Job(id++.ToString(), title, href));
                }
                try
                {
                    aNext1 = getDataWebEmlement(By.XPath("//a[contains(text(),'Trang kế tiếp')]"));

                }
                catch (NoSuchElementException e)
                {
                    aNext1 = new EmptyElement();
                }

                if (aNext1.GetAttribute("href")==null || aNext1.GetAttribute("href").Equals("") ||  aNext1 is EmptyElement)
                {
                    callback(listAddress);
                    chromeDriver.Quit();
                    return;
                }
                else
                {
                    System.Diagnostics.Debug.WriteLine("click: " + page);

                    chromeDriver.Navigate().GoToUrl(aNext1.GetAttribute("href"));
                    //aNext1.Click();
                    page++;
                }

                //Cài đặt bao nhiêu trang là thoát
                if (page == numberPages && numberPages > 0)
                {
                    callback(listAddress);
                    return;
                }

            }

        }

        public void getJobDetail(ICollection<dynamic> list, bool autoSave, string fileDes)
        {
                ExcelAction excel = new ExcelAction();
                LinkedList<JobDetail> listDetail = new LinkedList<JobDetail>();
            try
            {
                foreach (Job job in list)
                {
                    crawler(job, jobDetail =>
                    {
                        listDetail.AddLast(jobDetail);

                    });

                };

                chromeDriver.Quit();
                if (autoSave)
                {
                    excel.saveJobDetailCustom(fileDes, sheet => {
                        insertData(sheet, listDetail);
                    });
                }
                else
                {
                    excel.saveExcelJobDetail(sheet =>
                    {
                        insertData(sheet, listDetail);
                    });
                }
            }
            catch (Exception e)
            {
                chromeDriver.Quit();
                if (autoSave)
                {
                    excel.saveJobDetailCustom(fileDes, sheet => {
                        insertData(sheet, listDetail);
                    });
                }
                else
                {
                    excel.saveExcelJobDetail(sheet =>
                    {
                        insertData(sheet, listDetail);
                    });
                }
                System.Diagnostics.Debug.WriteLine(e);
                return;
            }
        }

        private void insertData(Excel._Worksheet sheet, LinkedList<JobDetail> listDetail)
        {
            
            for (int h = 1; h <= arrayHeader.Length; h++)
            {
                sheet.Cells[1, h] = arrayHeader[h - 1];
            }

            int i = 0;
            System.Diagnostics.Debug.WriteLine(listDetail.Count);
            foreach (var b in listDetail)
            {
                string[] a = {
                        b.id,
                        b.title ,
                        b.nameCty ,
                        b.ngayCapNhat ,
                        b.hanNopCV,
                        b.noiLamViec,
                        b.nguoiLienHe ,
                        b.website ,
                        b.addressCty,
                        b.luong ,
                        b.kinhNghiem,
                        b.hinhThucLV,
                        b.gioiTinh,
                        b.tuoi,
                        b.capBac,
                        b.chucVu,
                        b.nganhNghe ,
                        b.motaCV,
                        b.yeucauCV,
                        b.phucLoi ,
                        b.gioiThieuCty,
                        b.quyMoCty,
                };

                for (int j = 1; j <= a.Length; j++)
                {
                    sheet.Cells[i + 2, j].NumberFormat = "@";
                    sheet.Cells[i + 2, j].HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;
                    sheet.Cells[i + 2, j] = a[j - 1];
                }
                i++;
            }
        }

        override
        public void crawler(Job job, Action<JobDetail> callback)
        {
            chromeDriver.Navigate().GoToUrl(job.href);
            try
            {
                    Thread.Sleep(200);
                    string title = getDataWebEmlement(By.ClassName("top-job-info")).FindElement(By.TagName("h1")).Text;
                    string ngayCapNhat = sliptCharacter(getDataWebEmlement(By.XPath("//div[contains(text(),'Ngày cập nhật:')]")).Text, ':');
                    string ngayHetHan = sliptCharacter(getDataWebEmlement(By.XPath("//span[contains(text(),'Hết hạn nộp:')]/parent::*")).Text, ':');

                    string noiLamViec = sliptCharacter(getDataWebEmlement(By.XPath("//span[contains(text(),'Nơi làm việc:')]/parent::*")).Text, ':');
                    string nguoiLienHe = sliptCharacter(getDataWebEmlement(By.XPath("//label[contains(text(),'Người liên hệ:')]")).Text, ':');
                    string website = getDataWebEmlement(By.ClassName("MarginRight30")).Text;
                //string diaChiLienHe = getDataWebEmlement(By.XPath("/html/body/div[1]/div[2]/div[2]/div[1]/div/ul[3]/li[4]/span")).Text;
                //string sdt = getDataWebEmlement(By.XPath("/html/body/div[1]/div[2]/div[2]/div[1]/div/ul[3]/li[5]/span")).Text;

                    string nameCty = getDataWebEmlement(By.ClassName("top-job-info")).FindElement(By.ClassName("tit_company")).Text;
                    string addressCty = getDataWebEmlement(By.ClassName("TitleDetailNew")).FindElement(By.TagName("label")).Text;

                    string quyMoCty = splitByString(splitByString(getDataWebEmlement(By.ClassName("LeftJobCB")).Text, "Qui mô công ty:", 1).Trim(), "\n", 0);

                    string luong = sliptCharacter(getDataWebEmlement(By.XPath("//span[contains(text(),'Lương:')]/parent::*")).Text, ':');
                    string kinhNghiem = sliptCharacter(getDataWebEmlement(By.XPath("//span[contains(text(),'Kinh nghiệm:')]/parent::*")).Text, ':');
                    string hinhThucLV = sliptCharacter(getDataWebEmlement(By.XPath("//li[contains(text(),'Hình thức:')]")).Text, ':');
                    string gioiTinh = sliptCharacter(getDataWebEmlement(By.XPath("//li[contains(text(),'Giới tính:')]")).Text, ':');
                    string tuoi = sliptCharacter(getDataWebEmlement(By.XPath("//li[contains(text(),'Độ tuổi:')]")).Text, ':');

                    string chucVu = sliptCharacter(getDataWebEmlement(By.XPath("//span[contains(text(),'Cấp bậc:')]/parent::*")).Text, ':');

                    string yeuCauBC = sliptCharacter(getDataWebEmlement(By.XPath("//li[contains(text(),'Bằng cấp:')]")).Text, ':');
                    string nganhNghe = sliptCharacter(getDataWebEmlement(By.XPath("//span[contains(text(),'Ngành nghề:')]/parent::*")).Text, ':');
                    //string soluongCT = sliptCharacter(getDataWebEmlement(By.XPath("//*[@id=\"appLayout\"]/main/main/section[1]/div[1]/div[1]/div[1]/div[1]/div[12]/div/span")).Text, ':');

                    string motaCV = getDataWebEmlement(By.XPath("//h4[contains(text(),'Mô tả Công việc')]/parent::*/child::div")).Text;
                    string yeucauCV = getDataWebEmlement(By.XPath("//h4[contains(text(),'Yêu Cầu Công Việc')]/parent::*/child::div")).Text;
                    string quyenLoi = getDataWebEmlement(By.ClassName("list-benefits")).Text.Trim();

                    var aChitiet = getDataWebEmlement(By.XPath("//*[@id='emp_collapse']/a"));
                    if (!(aChitiet is EmptyElement))
                    {
                    IJavaScriptExecutor executor = chromeDriver as IJavaScriptExecutor;
                    executor.ExecuteScript("arguments[0].click();", aChitiet);
                        Thread.Sleep(200);
                    }

                     string gioiThieuCT = getDataWebEmlement(By.ClassName("desc_company")).FindElement(By.TagName("p")).Text;

                    System.Diagnostics.Debug.WriteLine(title + "\t" + ngayCapNhat + "\t" + nameCty + "\t" + addressCty + "\t" + nguoiLienHe + "\t" + kinhNghiem+"\t"+ gioiThieuCT);
                    //System.Diagnostics.Debug.WriteLine(kinhNghiem + "\t" + nganhNghe + "\t" + yeuCauBC + "\t" + luong);
                    //System.Diagnostics.Debug.WriteLine(motaCV + "\t" + yeucauCV + "\t" + quyenLoi);

                    JobDetail jobDetail = new JobDetail()
                    {
                        id          = job.id,
                        title       = title,
                        nameCty     = nameCty,
                        ngayCapNhat =  ngayCapNhat,
                        hanNopCV    =  ngayHetHan,
                        noiLamViec  = noiLamViec,
                        nguoiLienHe = nguoiLienHe,
                        website     = website,
                        addressCty  = addressCty,
                        luong       = luong,
                        kinhNghiem   = kinhNghiem,
                        hinhThucLV   = hinhThucLV,
                        gioiTinh     = gioiTinh,
                        tuoi        = tuoi,
                        capBac      = yeuCauBC,
                        chucVu      = chucVu,
                        nganhNghe   = nganhNghe,
                        motaCV      =  motaCV,
                        yeucauCV    =  yeucauCV,
                        phucLoi     = quyenLoi,
                        gioiThieuCty = gioiThieuCT,
                        quyMoCty    = quyMoCty,
                    };

                    callback(jobDetail);
                
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e);
                return;
            }

        }

        //XML
        override
        public void saveXML(ICollection<JobDetail> list, string desc)
        {
            try
            {
                System.Diagnostics.Debug.WriteLine("Bat dau luu! XML ");
                XElement xmlElements = new XElement("data", list.Select(i => new XElement("Item", nodes(i))));

                xmlElements.Save(desc + ".xml");

                System.Diagnostics.Debug.WriteLine("Xong!");
                Util.showMessageInformation("Lưu thành công: " + list.Count + " Item.");
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e);
                MessageBox.Show("Lưu thất bại!", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private object[] nodes(dynamic b)
        {
            int i = 0;
            string[] a = {
                b.id,
                b.title ,
                b.nameCty ,
                b.ngayCapNhat ,
                b.hanNopCV,
                b.noiLamViec,
                b.nguoiLienHe ,
                b.website ,
                b.addressCty,
                b.luong ,
                b.kinhNghiem,
                b.hinhThucLV,
                b.gioiTinh,
                b.tuoi,
                b.capBac,
                b.chucVu,
                b.nganhNghe ,
                b.motaCV,
                b.yeucauCV,
                b.phucLoi ,
                b.gioiThieuCty,
                b.quyMoCty,

            };

            object[] nodes = arrayHeader.Select(e => new XElement(e.Replace(' ', '-'), a[i++])).ToArray<object>();

            return nodes;
        }

    }
}
