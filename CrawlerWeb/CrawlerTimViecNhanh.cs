﻿using CrawlerWeb.model;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Linq;
using Excel = Microsoft.Office.Interop.Excel;

namespace CrawlerWeb
{
    class CrawlerTimViecNhanh : Crawler
    {


        private string[] arrayHeader = {"ID", "Vị trí", "Tên công ty", "Ngày cập nhật", "Địa chỉ",
                                                    "Quy mô công ty", "Người liên hệ", "Địa chỉ liên hệ",
                                                    "Lương", "Kinh nghiệm", "Phúc lợi",
                                                   "Ngành nghề", "Cấp bậc",
                                                   "Hạn nộp CV", "Mô tả",
                                                   "Yêu cầu công việc", "Giới tính", "Số lượng cần tuyển", "Hình thức làm việc","Website"
                                            };
        private string getValueByAddress(string address)
        {
            var a = Constant.addressTimVietNhanh;
            for(int i=0; i < a.Length; i++)
            {
                if (address.Equals(a[i]))
                {
                    return i.ToString();
                }
            }

            return "";
        }

        private string getValueBySalary(string strSalary)
        {
            string[] salaryValue = {"","2","4","5","7","6","10","8","11","12","9" };
            var a = Constant.salaryOfferTimViecNhanh;
            for(int i=0; i< a.Length; i++)
            {
                if (strSalary.Equals(a[i]))
                {
                    return salaryValue[i];
                }
            };
            return "";
        }


        override
        public LinkedList<dynamic> getLinkByCustomListCategory(SearchTypes types)
        {
            LinkedList<dynamic> list = new LinkedList<dynamic>();
            try
            {
                chromeDriver.Navigate().GoToUrl("https://www.timviecnhanh.com/viec-lam/tim-kiem-nang-cao?tu_khoa=&nganh_nghe%5B%5D=&tinh_thanh%5B%5D=");

                if (!types.address.Equals(Constant.addressTimVietNhanh[0]))
                {
                    var a = chromeDriver.FindElement(By.XPath("//*[@id='s2id_khuvuc']/a"));
                    a.Click();

                    var input = getDataWebEmlement(By.XPath("//*[@id='select2-drop']/div/input"));
                    input.SendKeys(types.address + OpenQA.Selenium.Keys.Enter);

                }

                if (!types.salary.Equals(Constant.salaryOfferTimViecNhanh[0]))
                {
                    var a1 = chromeDriver.FindElement(By.XPath("//*[@id='s2id_mucluong']/a"));
                    a1.Click();
                    var input1 = getDataWebEmlement(By.XPath("//*[@id='select2-drop']/div/input"));
                    input1.SendKeys(types.salary + OpenQA.Selenium.Keys.Enter);
                }

                var btnSubmit = getDataWebEmlement(By.XPath("//*[@id='form-job-search-advance']/div/div/div[1]/div[8]/button"));
                btnSubmit.Click();

                getAllLinkJob(e =>
                {
                    list = e;
                });
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e);
                return list;
            }

            return list;
        }

        public LinkedList<dynamic> getLinkByCustom(string address, string salary)
        {
            LinkedList<dynamic> list = new LinkedList<dynamic>();
            try
            {
                chromeDriver.Navigate().GoToUrl("https://www.timviecnhanh.com/viec-lam/tim-kiem-nang-cao?tu_khoa=&nganh_nghe%5B%5D=&tinh_thanh%5B%5D=");

                if (!address.Equals(Constant.addressTimVietNhanh[0]))
                {
                    var a = chromeDriver.FindElement(By.XPath("//*[@id='s2id_khuvuc']/a"));
                    a.Click();

                    var input = getDataWebEmlement(By.XPath("//*[@id='select2-drop']/div/input"));
                    input.SendKeys(address + OpenQA.Selenium.Keys.Enter);

                }

                if (!salary.Equals(Constant.salaryOfferTimViecNhanh[0]))
                {
                    var a1 = chromeDriver.FindElement(By.XPath("//*[@id='s2id_mucluong']/a"));
                    a1.Click();
                    var input1 = getDataWebEmlement(By.XPath("//*[@id='select2-drop']/div/input"));
                    input1.SendKeys(salary + OpenQA.Selenium.Keys.Enter);
                }

                var btnSubmit = getDataWebEmlement(By.XPath("//*[@id='form-job-search-advance']/div/div/div[1]/div[8]/button"));
                btnSubmit.Click();

                getAllLinkJob(e =>
                {
                    list = e;
                });
                chromeDriver.Quit();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e);
                chromeDriver.Quit();
                return list;
            }

            return list;
        }

        int id = 1;

        public void getAllLinkJob(Action<LinkedList<dynamic>> callback)
        {
            LinkedList<dynamic> listAddress = new LinkedList<dynamic>();
            IWebElement aNext1 = null;
            int page = 0;

            while (true)
            {
                Thread.Sleep(1000);
                var jobItems = getDataWebEmlement(By.ClassName("block-content")).FindElements(By.ClassName("block-item"));
                foreach (var job in jobItems)
                {
                    string title = job.FindElement(By.TagName("a")).GetAttribute("title");
                    string href = job.FindElement(By.TagName("a")).GetAttribute("href");
                    listAddress.AddLast(new Job(id++.ToString(), title, href));
                }
                try
                {
                    aNext1 = getDataWebEmlement(By.ClassName("next"));

                }
                catch (NoSuchElementException e)
                {
                    aNext1 = new EmptyElement();
                }

                if (aNext1.GetAttribute("href")==null || aNext1.GetAttribute("href").Equals("") ||  aNext1 is EmptyElement)
                {
                    callback(listAddress);
                    chromeDriver.Quit();
                    return;
                }
                else
                {
                    System.Diagnostics.Debug.WriteLine("click: " + page);
                    aNext1.Click();
                    page++;
                }
                //Cài đặt bao nhiêu trang là thoát
                if (page == numberPages && numberPages > 0)
                {
                    callback(listAddress);
                    return;
                }

            }

        }

        public void getJobDetail(ICollection<dynamic> list, bool autoSave, string fileDes)
        {
                ExcelAction excel = new ExcelAction();
                LinkedList<JobDetail> listDetail = new LinkedList<JobDetail>();
            try
            {
                foreach (Job job in list)
                {
                    crawler(job, jobDetail =>
                    {
                        listDetail.AddLast(jobDetail);

                    });

                };

                chromeDriver.Quit();
                if (autoSave)
                {
                    excel.saveJobDetailCustom(fileDes, sheet => {
                        insertData(sheet, listDetail);
                    });
                }
                else
                {
                    excel.saveExcelJobDetail(sheet =>
                    {
                        insertData(sheet, listDetail);
                    });
                }
            }
            catch (Exception e)
            {
                chromeDriver.Quit();
                if (autoSave)
                {
                    excel.saveJobDetailCustom(fileDes, sheet => {
                        insertData(sheet, listDetail);
                    });
                }
                else
                {
                    excel.saveExcelJobDetail(sheet =>
                    {
                        insertData(sheet, listDetail);
                    });
                }
                System.Diagnostics.Debug.WriteLine(e);
                return;
            }
        }

        private void insertData(Excel._Worksheet sheet, LinkedList<JobDetail> listDetail)
        {
            
            for (int h = 1; h <= arrayHeader.Length; h++)
            {
                sheet.Cells[1, h] = arrayHeader[h - 1];
            }

            int i = 0;
            System.Diagnostics.Debug.WriteLine(listDetail.Count);
            foreach (var b in listDetail)
            {
                string[] a = {b.id, b.title, b.nameCty,b.ngayCapNhat,
                    b.addressCty,
                    b.quyMoCty,
                    b.nguoiLienHe,
                    b.diachiLienHe,
                    b.luong,
                    b.kinhNghiem,
                    b.phucLoi,
                    b.nganhNghe,
                    b.capBac,
                    b.hanNopCV,
                    b.motaCV,
                    b.yeucauCV, b.gioiTinh, b.soluongCT, b.hinhThucLV, b.website };

                for (int j = 1; j <= a.Length; j++)
                {
                    sheet.Cells[i + 2, j].NumberFormat = "@";
                    sheet.Cells[i + 2, j].HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignLeft;
                    sheet.Cells[i + 2, j] = a[j - 1];
                }
                i++;
            }
        }

        override
        public void crawler(Job job, Action<JobDetail> callback)
        {
            chromeDriver.Navigate().GoToUrl(job.href);
            try
            {
                var chuyenTrangTuyenDung = getDataWebEmlement(By.XPath("//*[@id=\"appLayout\"]/nav[2]/div/div[2]/div/h3")).Text;
                if (!chuyenTrangTuyenDung.Equals("Null"))
                {
                    string title = getDataWebEmlement(By.XPath("//*[@id=\"appLayout\"]/main/main/section[1]/div[1]/div[1]/div[1]/div[1]/h1")).Text;
                    string ngayCapNhat = sliptCharacter(getDataWebEmlement(By.XPath("//*[@id=\"appLayout\"]/main/main/section[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div/span[3]")).Text, ':');
                    string ngayHetHan = sliptCharacter(getDataWebEmlement(By.XPath("//*[@id=\"appLayout\"]/main/main/section[1]/div[1]/div[1]/div[1]/div[1]/div[3]/div/span")).Text, ':');

                    string nguoiLienHe = sliptCharacter(getDataWebEmlement(By.XPath("//*[@id=\"appLayout\"]/main/main/section[1]/div[1]/div[1]/div[2]/div/div[7]/div[2]/span")).Text, ':');
                    string diaChiLienHe = sliptCharacter(getDataWebEmlement(By.XPath("//*[@id=\"appLayout\"]/main/main/section[1]/div[1]/div[1]/div[2]/div/div[7]/div[3]/span")).Text, ':');

                    string nameCty = getDataWebEmlement(By.XPath("//*[@id=\"appLayout\"]/main/main/section[1]/div[1]/div[1]/div[2]/div/div[11]/div[2]")).Text;
                    string addressCty = sliptCharacter(getDataWebEmlement(By.XPath("//*[@id=\"appLayout\"]/main/main/section[1]/div[1]/div[1]/div[2]/div/div[11]/div[3]")).Text, ':');
                    string quyMoCty = sliptCharacter(getDataWebEmlement(By.XPath("//*[@id=\"appLayout\"]/main/main/section[1]/div[1]/div[1]/div[2]/div/div[11]/div[5]")).Text,':');
                    string website = sliptCharacter(getDataWebEmlement(By.XPath("//*[@id=\"appLayout\"]/main/main/section[1]/div[1]/div[1]/div[2]/div/div[11]/div[6]/a")).Text, ':');


                    string luong = sliptCharacter(getDataWebEmlement(By.XPath("//*[@id=\"appLayout\"]/main/main/section[1]/div[1]/div[1]/div[1]/div[1]/div[7]/div/span")).Text, ':');
                    string kinhNghiem = sliptCharacter(getDataWebEmlement(By.XPath("//label[contains(text(),'Kinh nghiệm')]/parent::*")).Text, ':');
                    string hinhThucLV = sliptCharacter(getDataWebEmlement(By.XPath("//*[@id=\"appLayout\"]/main/main/section[1]/div[1]/div[1]/div[1]/div[1]/div[10]/div/span")).Text, ':');
                    string gioiTinh = sliptCharacter(getDataWebEmlement(By.XPath("//*[@id=\"appLayout\"]/main/main/section[1]/div[1]/div[1]/div[1]/div[1]/div[13]/div/span")).Text, ':');

                    string yeuCauBC = sliptCharacter(getDataWebEmlement(By.XPath("//*[@id=\"appLayout\"]/main/main/section[1]/div[1]/div[1]/div[1]/div[1]/div[10]/div/span")).Text, ':');
                    string nganhNghe = sliptCharacter(getDataWebEmlement(By.XPath("//*[@id=\"appLayout\"]/main/main/section[1]/div[1]/div[1]/div[1]/div[1]/div[13]/div/span")).Text, ':');
                    string soluongCT = sliptCharacter(getDataWebEmlement(By.XPath("//*[@id=\"appLayout\"]/main/main/section[1]/div[1]/div[1]/div[1]/div[1]/div[12]/div/span")).Text, ':');

                    //string hanNopCV = getDataWebEmlement(By.XPath("//*[@id=\"left-content\"]/article/table/tbody/tr[4]/td[2]/b")).Text;
                    string motaCV = getDataWebEmlement(By.XPath("//*[@id=\"appLayout\"]/main/main/section[1]/div[1]/div[1]/div[1]/div[1]/div[17]/p[1]")).Text;
                    string yeucauCV = getDataWebEmlement(By.XPath("//*[@id=\"appLayout\"]/main/main/section[1]/div[1]/div[1]/div[1]/div[1]/div[17]/p[2]")).Text;
                    string quyenLoi = getDataWebEmlement(By.ClassName("des-info-job")).FindElements(By.TagName("p"))[2].Text.Trim();

                    System.Diagnostics.Debug.WriteLine(title + "\t" + ngayCapNhat + "\t" + nameCty + "\t" + addressCty + "\t" + nguoiLienHe + "\t" + diaChiLienHe + "\t" + kinhNghiem);
                    //System.Diagnostics.Debug.WriteLine(kinhNghiem + "\t" + nganhNghe + "\t" + yeuCauBC + "\t" + luong);
                    System.Diagnostics.Debug.WriteLine(motaCV + "\t" + yeucauCV + "\t" + quyenLoi);

                    JobDetail jobDetail = new JobDetail()
                    {
                        id    = job.id,
                        title = title,
                        nameCty = nameCty,
                        ngayCapNhat =  ngayCapNhat,
                        hanNopCV = ngayHetHan,
                        nguoiLienHe = nguoiLienHe,
                        diachiLienHe = diaChiLienHe,
                        addressCty = addressCty,
                        quyMoCty = quyMoCty,
                        luong = luong,
                        kinhNghiem = kinhNghiem,
                        hinhThucLV = hinhThucLV,
                        gioiTinh = gioiTinh,
                        capBac = yeuCauBC,
                        nganhNghe = nganhNghe,
                        soluongCT = soluongCT,
                        motaCV =  motaCV,
                        yeucauCV =  yeucauCV,
                        phucLoi = quyenLoi,
                        website = website

                    };

                    callback(jobDetail);
                }
                else
                {
                    string title = getDataWebEmlement(By.XPath("//*[@id=\"left-content\"]/header/h1/span")).Text;
                    string ngayCapNhat = getDataWebEmlement(By.XPath("//*[@id=\"left-content\"]/article/div[1]/div[1]/time")).Text;
                    string ngayHetHan = getDataWebEmlement(By.XPath("//*[text()='Hạn nộp']/parent::*/parent::*")).FindElements(By.TagName("td"))[1].Text;


                    string nguoiLienHe = getDataWebEmlement(By.XPath("//*[@id=\"left-content\"]/article/div[7]/div/table/tbody/tr[1]/td[2]")).Text;
                    string diaChiLienHe = getDataWebEmlement(By.XPath("//*[@id=\"left-content\"]/article/div[7]/div/table/tbody/tr[2]/td[2]")).Text;

                    string nameCty = getDataWebEmlement(By.XPath("/html/body/section/div[1]/div/div[2]/div[1]/div[1]/div[1]/div[2]")).Text;
                    string addressCty = sliptCharacter(getDataWebEmlement(By.XPath("/html/body/section/div[1]/div/div[2]/div[1]/div[1]/p[1]")).Text, ':');
                    string quyMoCty = "Null";
                    string website = sliptCharacter(getDataWebEmlement(By.XPath("/html/body/section/div[1]/div/div[2]/div[1]/div[1]/p[2]")).Text, ':');


                    string luong = sliptCharacter(getDataWebEmlement(By.XPath("//*[@id=\"left-content\"]/article/div[5]/div[1]/ul/li[1]")).Text, ':');
                    string kinhNghiem = sliptCharacter(getDataWebEmlement(By.XPath("//*[@id=\"left-content\"]/article/div[5]/div[1]/ul/li[2]")).Text, ':');
                    string hinhThucLV = sliptCharacter(getDataWebEmlement(By.XPath("//*[@id=\"left-content\"]/article/div[5]/div[2]/ul/li[4]")).Text, ':');
                    string gioiTinh = sliptCharacter(getDataWebEmlement(By.XPath("//*[@id=\"left-content\"]/article/div[5]/div[2]/ul/li[2]")).Text,':');

                    string yeuCauBC = sliptCharacter(getDataWebEmlement(By.XPath("//*[@id=\"left-content\"]/article/div[5]/div[1]/ul/li[3]")).Text,':');
                    string nganhNghe = sliptCharacter(getDataWebEmlement(By.XPath("//*[@id=\"left-content\"]/article/div[5]/div[1]/ul/li[5]")).Text, ':');
                    string soluongCT = sliptCharacter(getDataWebEmlement(By.XPath("//*[@id=\"left-content\"]/article/div[5]/div[2]/ul/li[1]")).Text, ':');

                    //string hanNopCV = getDataWebEmlement(By.XPath("//*[@id=\"left-content\"]/article/table/tbody/tr[4]/td[2]/b")).Text;
                    string motaCV = getDataWebEmlement(By.XPath("//*[@id=\"left-content\"]/article/table/tbody/tr[1]/td[2]/p")).Text;
                    string yeucauCV = getDataWebEmlement(By.XPath("//*[@id=\"left-content\"]/article/table/tbody/tr[2]/td[2]/p")).Text;
                    string quyenLoi = getDataWebEmlement(By.XPath("//*[@id=\"left-content\"]/article/table/tbody/tr[3]/td[2]/p")).Text;

                    System.Diagnostics.Debug.WriteLine(title + "\t" + ngayCapNhat + "\t" + nameCty + "\t" + addressCty + "\t" + nguoiLienHe + "\t" + diaChiLienHe);
                    //System.Diagnostics.Debug.WriteLine(kinhNghiem + "\t" + nganhNghe + "\t" + yeuCauBC + "\t" + luong);
                    //System.Diagnostics.Debug.WriteLine(motaCV + "\t" + yeucauCV + "\t" + quyenLoi);

                    JobDetail jobDetail = new JobDetail()
                    {
                        id    = job.id,
                        title = title,
                        nameCty = nameCty,
                        ngayCapNhat = "'" + ngayCapNhat,
                        hanNopCV = ngayHetHan,
                        nguoiLienHe = nguoiLienHe,
                        diachiLienHe = diaChiLienHe,
                        addressCty = addressCty,
                        quyMoCty = quyMoCty,
                        luong = luong,
                        kinhNghiem = kinhNghiem,
                        hinhThucLV = hinhThucLV,
                        gioiTinh = gioiTinh,
                        capBac = yeuCauBC,
                        nganhNghe = nganhNghe,
                        soluongCT = soluongCT,
                        motaCV = motaCV,
                        yeucauCV = yeucauCV,
                        phucLoi = quyenLoi,
                        website = website
                    };

                    callback(jobDetail);

                }

                
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e);
                return;
            }

        }


        //XML
        override
        public void saveXML(ICollection<JobDetail> list, string desc)
        {
            try
            {
                System.Diagnostics.Debug.WriteLine("Bat dau luu! XML ");
                XElement xmlElements = new XElement("data", list.Select(i => new XElement("Item", nodes(i))));

                xmlElements.Save(desc + ".xml");

                System.Diagnostics.Debug.WriteLine("Xong!");
                Util.showMessageInformation("Lưu thành công: " + list.Count + " Item.");
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e);
                MessageBox.Show("Lưu thất bại!", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private object[] nodes(dynamic b)
        {
            int i = 0;
            string[] a = {b.id, b.title, b.nameCty,b.ngayCapNhat,
                    b.addressCty,
                    b.quyMoCty,
                    b.nguoiLienHe,
                    b.diachiLienHe,
                    b.luong,
                    b.kinhNghiem,
                    b.phucLoi,
                    b.nganhNghe,
                    b.capBac,
                    b.hanNopCV,
                    b.motaCV,
                    b.yeucauCV, b.gioiTinh, b.soluongCT, b.hinhThucLV, b.website };

            object[] nodes = arrayHeader.Select(e => new XElement(e.Replace(' ', '-'), a[i++])).ToArray<object>();

            return nodes;
        }


    }
}
