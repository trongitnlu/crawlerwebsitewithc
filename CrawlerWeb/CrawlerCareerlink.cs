﻿using CrawlerWeb.model;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Linq;
using Excel = Microsoft.Office.Interop.Excel;

namespace CrawlerWeb
{
    class CrawlerCareerLink : Crawler
    {

        private string[] arrayHeader = {"ID", "Vị trí", "Tên công ty", "Ngày cập nhật", "Địa chỉ",
                                                    "Quy mô công ty", "Người liên hệ", "Số điện thoại", "Địa chỉ liên hệ",
                                                    "Lương", "Kinh nghiệm", "Tuổi",
                                                   "Ngành nghề", "Cấp bậc",
                                                   "Hạn nộp CV", "Mô tả",
                                                   "Yêu cầu công việc", "Giới tính", "Số lượng cần tuyển", "Hình thức làm việc","Website"
                                            };

        override
        public LinkedList<dynamic> getLinkByCustomListCategory(SearchTypes searchTypes)
        {
            LinkedList<dynamic> list = new LinkedList<dynamic>();

            try
            {

                string url = "https://www.careerlink.vn/vieclam/list?keyword_use=A";

                chromeDriver.Navigate().GoToUrl(url);

                var inputAddress = getDataWebEmlement(By.XPath("//*[@id=\"searchFormRightCol\"]/div[4]/span/span[1]/span/ul/li/input"));
                inputAddress.SendKeys(searchTypes.address + OpenQA.Selenium.Keys.Enter);

                var inputSalary = getDataWebEmlement(By.Id("advancedJobSearch_salaryType"));
                if (searchTypes.salary.Equals(Constant.salaryOfferCareerLink[1]))
                {
                    inputSalary.SendKeys(OpenQA.Selenium.Keys.ArrowDown + OpenQA.Selenium.Keys.ArrowDown + OpenQA.Selenium.Keys.ArrowDown + OpenQA.Selenium.Keys.Enter + OpenQA.Selenium.Keys.Enter);
                }
                if (searchTypes.salary.Equals(Constant.salaryOfferCareerLink[2]))
                {
                    inputSalary.SendKeys(OpenQA.Selenium.Keys.ArrowDown + OpenQA.Selenium.Keys.ArrowDown + OpenQA.Selenium.Keys.ArrowDown + OpenQA.Selenium.Keys.ArrowDown + OpenQA.Selenium.Keys.Enter + OpenQA.Selenium.Keys.Enter);
                }

                var btnSearch = getDataWebEmlement(By.XPath("//*[@id=\"searchFormRightCol\"]/div[11]/input"));
                btnSearch.Click();
                getAllLinkJob(e =>
                {
                    list = e;
                });
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e);
                return list;
            }

            return list;
        }

        public LinkedList<dynamic> getLinkByCustom(SearchTypes searchTypes)
        {
            LinkedList<dynamic> list = new LinkedList<dynamic>();

            try
            {

                string url = "https://www.careerlink.vn/vieclam/list?keyword_use=A";

                chromeDriver.Navigate().GoToUrl(url);

                var inputAddress = getDataWebEmlement(By.XPath("//*[@id=\"searchFormRightCol\"]/div[4]/span/span[1]/span/ul/li/input"));
                inputAddress.SendKeys(searchTypes.address + OpenQA.Selenium.Keys.Enter);

                var inputSalary = getDataWebEmlement(By.Id("advancedJobSearch_salaryType"));
                if (searchTypes.salary.Equals(Constant.salaryOfferCareerLink[1]))
                {
                    inputSalary.SendKeys(OpenQA.Selenium.Keys.ArrowDown + OpenQA.Selenium.Keys.ArrowDown + OpenQA.Selenium.Keys.ArrowDown + OpenQA.Selenium.Keys.Enter + OpenQA.Selenium.Keys.Enter);
                }
                if (searchTypes.salary.Equals(Constant.salaryOfferCareerLink[2]))
                {
                    inputSalary.SendKeys(OpenQA.Selenium.Keys.ArrowDown + OpenQA.Selenium.Keys.ArrowDown + OpenQA.Selenium.Keys.ArrowDown + OpenQA.Selenium.Keys.ArrowDown + OpenQA.Selenium.Keys.Enter + OpenQA.Selenium.Keys.Enter);
                }

                var btnSearch = getDataWebEmlement(By.XPath("//*[@id=\"searchFormRightCol\"]/div[11]/input"));
                btnSearch.Click();
                getAllLinkJob(e =>
                {
                    list = e;
                });
                chromeDriver.Quit();
            }
            catch(Exception e)
            {
                chromeDriver.Quit();
                System.Diagnostics.Debug.WriteLine(e);
                return list;
            }

            return list;
        }
        int id = 1;

        public void getAllLinkJob(Action<LinkedList<dynamic>> callback)
        {
            LinkedList<dynamic> listAddress = new LinkedList<dynamic>();
            IWebElement aNext1 = null;
            int page = 0;

            while (true)
            {
                Thread.Sleep(1000);
                var jobItems = getDataWebEmlements(By.ClassName("list-group-item"));
                foreach (var job in jobItems)
                {
                    string title = job.FindElement(By.TagName("a")).Text;
                    string href = job.FindElement(By.TagName("a")).GetAttribute("href");
                    listAddress.AddLast(new Job(id++.ToString(), title, href));
                }
                try
                {
                    aNext1 = getDataWebEmlement(By.XPath("//span[contains(text(),'Tiếp tục')]/parent::*"));

                }
                catch (NoSuchElementException e)
                {
                    aNext1 = new EmptyElement();
                }

                if (aNext1.GetAttribute("href")==null || aNext1.GetAttribute("href").Equals("") ||  aNext1 is EmptyElement)
                {
                    callback(listAddress);
                    chromeDriver.Quit();
                    return;
                }
                else
                {
                    System.Diagnostics.Debug.WriteLine("click: " + page);
                    aNext1.Click();
                    page++;
                }

                //Cài đặt bao nhiêu trang là thoát
                if (page == numberPages && numberPages > 0)
                {
                    callback(listAddress);
                    return;
                }

            }

        }

        public void getJobDetail(ICollection<dynamic> list, bool autoSave, string fileDes)
        {
                ExcelAction excel = new ExcelAction();
                LinkedList<JobDetail> listDetail = new LinkedList<JobDetail>();
            try
            {
                foreach (Job job in list)
                {
                    crawler(job, jobDetail =>
                    {
                        listDetail.AddLast(jobDetail);

                    });

                };

                chromeDriver.Quit();
                if (autoSave)
                {
                    excel.saveJobDetailCustom(fileDes, sheet => {
                        insertData(sheet, listDetail);
                    });
                }
                else
                {
                    excel.saveExcelJobDetail(sheet =>
                    {
                        insertData(sheet, listDetail);
                    });
                }
            }
            catch (Exception e)
            {
                chromeDriver.Quit();
                if (autoSave)
                {
                    excel.saveJobDetailCustom(fileDes, sheet => {
                        insertData(sheet, listDetail);
                    });
                }
                else
                {
                    excel.saveExcelJobDetail(sheet =>
                    {
                        insertData(sheet, listDetail);
                    });
                }
                System.Diagnostics.Debug.WriteLine(e);
                return;
            }
        }

        private void insertData(Excel._Worksheet sheet, LinkedList<JobDetail> listDetail)
        {
            
            for (int h = 1; h <= arrayHeader.Length; h++)
            {
                sheet.Cells[1, h] = arrayHeader[h - 1];
            }

            int i = 0;
            System.Diagnostics.Debug.WriteLine(listDetail.Count);
            foreach (var b in listDetail)
            {
                string[] a = {b.id, b.title, b.nameCty,b.ngayCapNhat,
                    b.addressCty,
                    b.quyMoCty,
                    b.nguoiLienHe,
                    b.sdt,
                    b.diachiLienHe,
                    b.luong,
                    b.kinhNghiem,
                    b.tuoi,
                    b.nganhNghe,
                    b.capBac,
                    b.hanNopCV,
                    b.motaCV,
                    b.yeucauCV, b.gioiTinh, b.soluongCT, b.hinhThucLV, b.website };

                for (int j = 1; j <= a.Length; j++)
                {
                    sheet.Cells[i + 2, j].NumberFormat = "@";
                    sheet.Cells[i + 2, j].HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;
                    sheet.Cells[i + 2, j] = a[j - 1];
                }
                i++;
            }
        }

        override
        public void crawler(Job job, Action<JobDetail> callback)
        {
            chromeDriver.Navigate().GoToUrl(job.href);
            try
            {
                    string title = getDataWebEmlement(By.XPath("/html/body/div[1]/div[2]/div[1]/h1/span")).Text;
                    string ngayCapNhat = getDataWebEmlement(By.XPath("/html/body/div[1]/div[2]/div[2]/div[1]/div/dl/dd[1]")).Text;
                    string ngayHetHan = getDataWebEmlement(By.XPath("/html/body/div[1]/div[2]/div[2]/div[1]/div/dl/dd[2]")).Text;

                    string nguoiLienHe = sliptCharacter(getDataWebEmlement(By.XPath("/html/body/div[1]/div[2]/div[2]/div[1]/div/ul[3]/li[3]")).Text,':');
                    string diaChiLienHe = getDataWebEmlement(By.XPath("/html/body/div[1]/div[2]/div[2]/div[1]/div/ul[3]/li[4]/span")).Text;
                    string sdt = getDataWebEmlement(By.XPath("/html/body/div[1]/div[2]/div[2]/div[1]/div/ul[3]/li[5]/span")).Text;

                    string nameCty = getDataWebEmlement(By.XPath("/html/body/div[1]/div[2]/div[2]/div[1]/div/ul[1]/li[1]/a/span/span")).Text;
                    string addressCty = getDataWebEmlement(By.XPath("/html/body/div[1]/div[2]/div[2]/div[1]/div/ul[1]/li[2]/span")).Text;
                    string quyMoCty = getDataWebEmlement(By.XPath("/html/body/div[1]/div[2]/div[2]/div[2]/div/ul[1]/li[1]/span")).Text;
                    string website = getDataWebEmlement(By.XPath("/html/body/div[1]/div[2]/div[2]/div[2]/div/dl/dd")).Text;


                    string luong = sliptCharacter(getDataWebEmlement(By.XPath("/html/body/div[1]/div[2]/div[2]/div[1]/div/ul[1]/li[3]")).Text, ':');
                    string kinhNghiem = sliptCharacter(getDataWebEmlement(By.XPath("//li[contains(text(),\"Mức kinh nghiệm:\")]")).Text, ':');
                    string hinhThucLV = sliptCharacter(getDataWebEmlement(By.XPath("//li[contains(text(),\"Loại công việc:\")]")).Text, ':');
                    string gioiTinh = sliptCharacter(getDataWebEmlement(By.XPath("//li[contains(text(),\"Giới tính:\")]")).Text, ':');
                    string tuoi = sliptCharacter(getDataWebEmlement(By.XPath("//li[contains(text(),\"Tuổi:\")]")).Text, ':');

                    string yeuCauBC = sliptCharacter(getDataWebEmlement(By.XPath("//li[contains(text(),\"Trình độ học vấn:\")]")).Text, ':');
                    string nganhNghe = sliptCharacter(getDataWebEmlement(By.XPath("//li[contains(text(),\"Ngành nghề việc làm:\")]")).Text, ':');
                    //string soluongCT = sliptCharacter(getDataWebEmlement(By.XPath("//*[@id=\"appLayout\"]/main/main/section[1]/div[1]/div[1]/div[1]/div[1]/div[12]/div/span")).Text, ':');

                    string motaCV = getDataWebEmlement(By.XPath("//div[@itemprop=\"description\"]")).Text;
                    string yeucauCV = getDataWebEmlement(By.XPath("//div[@itemprop=\"skills\"]")).Text;
                    //string quyenLoi = getDataWebEmlement(By.ClassName("des-info-job")).FindElements(By.TagName("p"))[2].Text.Trim();

                    System.Diagnostics.Debug.WriteLine(title + "\t" + ngayCapNhat + "\t" + nameCty + "\t" + addressCty + "\t" + nguoiLienHe + "\t" + diaChiLienHe + "\t" + kinhNghiem);
                    //System.Diagnostics.Debug.WriteLine(kinhNghiem + "\t" + nganhNghe + "\t" + yeuCauBC + "\t" + luong);
                    //System.Diagnostics.Debug.WriteLine(motaCV + "\t" + yeucauCV + "\t" + quyenLoi);

                    JobDetail jobDetail = new JobDetail()
                    {
                        id = job.id,
                        title = title,
                        nameCty = nameCty,
                        ngayCapNhat =  ngayCapNhat,
                        hanNopCV =  ngayHetHan,
                        nguoiLienHe = nguoiLienHe,
                        diachiLienHe = diaChiLienHe,
                        addressCty = addressCty,
                        quyMoCty = quyMoCty,
                        luong = luong,
                        kinhNghiem = kinhNghiem,
                        hinhThucLV = hinhThucLV,
                        gioiTinh = gioiTinh,
                        capBac = yeuCauBC,
                        nganhNghe = nganhNghe,
                       // soluongCT = soluongCT,
                        motaCV =  motaCV,
                        yeucauCV =  yeucauCV,
                       // phucLoi = quyenLoi,
                        website = website,
                        tuoi = tuoi,
                        sdt = sdt,

                    };

                    callback(jobDetail);
                
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e);
                return;
            }

        }

        //XML
        override
        public void saveXML(ICollection<JobDetail> list, string desc)
        {
            try
            {
                System.Diagnostics.Debug.WriteLine("Bat dau luu! XML ");
                XElement xmlElements = new XElement("data", list.Select(i => new XElement("Item", nodes(i))));

                xmlElements.Save(desc + ".xml");

                System.Diagnostics.Debug.WriteLine("Xong!");
                Util.showMessageInformation("Lưu thành công: " + list.Count + " Item.");
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e);
                MessageBox.Show("Lưu thất bại!", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private object[] nodes(dynamic b)
        {
            int i = 0;
            string[] a = {b.id, b.title, b.nameCty,b.ngayCapNhat,
                    b.addressCty,
                    b.quyMoCty,
                    b.nguoiLienHe,
                    b.sdt,
                    b.diachiLienHe,
                    b.luong,
                    b.kinhNghiem,
                    b.tuoi,
                    b.nganhNghe,
                    b.capBac,
                    b.hanNopCV,
                    b.motaCV,
                    b.yeucauCV, b.gioiTinh, b.soluongCT, b.hinhThucLV, b.website };

            object[] nodes = arrayHeader.Select(e => new XElement(e.Replace(' ', '-'), a[i++])).ToArray<object>();

            return nodes;
        }

    }
}
