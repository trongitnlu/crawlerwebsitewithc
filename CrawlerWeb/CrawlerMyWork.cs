﻿using CrawlerWeb.model;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Linq;
using Excel = Microsoft.Office.Interop.Excel;

namespace CrawlerWeb
{
    class CrawlerMyWork : Crawler
    {

        private  string[] arrayHeader = {"ID", "Vị trí", "Tên công ty", "Ngày cập nhật", "Địa chỉ",
                                                    "Quy mô công ty", "Người liên hệ",
                        //"Số điện thoại",
                                                    "Địa chỉ liên hệ",
                                                    "Lương", "Kinh nghiệm",
                                                    "Chức vụ",
                        //"Tuổi",
                                                   "Ngành nghề", "Cấp bậc",
                                                   "Hạn nộp CV", "Mô tả",
                                                   "Yêu cầu công việc","Phúc lợi",
                                                   "Yêu cầu hồ sơ",
                                                    "Giới tính", "Số lượng cần tuyển", "Hình thức làm việc",
                                                    "Lượt xem",
                        //"Website"
                                            };
        private string getValueByAddress(string address)
        {
            var a = Constant.addressMyWork;
            for (int i = 0; i < a.Length; i++)
            {
                if (address.Equals(a[i]))
                {
                    return i.ToString();
                }
            }

            return "";
        }

        private string getValueBySalary(string strSalary)
        {
            var a = Constant.salaryOfferMyWork;
            string link = "";
            for (int i = 0; i < a.Length; i++)
            {
                if (strSalary.Equals(a[i]))
                {
                    if (i >= 3)
                    {
                        link = (i + 1) + getLinkBySalary(strSalary);
                    }
                    else
                    {
                        link = (i + 1).ToString();
                    }
                    return link;
                }
            }

            return "";
        }

        private string getLinkBySalary(string salary)
        {
            List<string> a = salary.Split(' ')[0].Split('-').ToList();
            if (a.Count == 1)
            {
                a.Add("100");
            }
            return "&salary_max_vnd=" + a[1] + "000000&salary_min_vnd=" + a[0] + "000000";
        }

        override
        public LinkedList<dynamic> getLinkByCustomListCategory(SearchTypes searchTypes)
        {
            LinkedList<dynamic> list = new LinkedList<dynamic>();

            try
            {
                var location = getValueByAddress(searchTypes.address);
                var salary = getValueBySalary(searchTypes.salary);
                string url = "https://mywork.com.vn/tuyen-dung/trang/1?locations=" + location + "&salary=" + salary;

                chromeDriver.Navigate().GoToUrl(url);

                getAllLinkJob(e =>
                {
                    list = e;
                });
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e);
                return list;
            }

            return list;
        }

        public LinkedList<dynamic> getLinkByCustom(SearchTypes searchTypes)
        {
            LinkedList<dynamic> list = new LinkedList<dynamic>();

            try
            {
                var location = getValueByAddress(searchTypes.address);
                var salary = getValueBySalary(searchTypes.salary);
                string url = "https://mywork.com.vn/tuyen-dung/trang/1?locations=" + location + "&salary=" + salary;

                chromeDriver.Navigate().GoToUrl(url);

                getAllLinkJob(e =>
                {
                    list = e;
                });
                chromeDriver.Quit();
            }
            catch (Exception e)
            {
                chromeDriver.Quit();
                System.Diagnostics.Debug.WriteLine(e);
                return list;
            }

            return list;
        }

        int id = 1;

        public void getAllLinkJob(Action<LinkedList<dynamic>> callback)
        {
            LinkedList<dynamic> listAddress = new LinkedList<dynamic>();
            IWebElement aNext1 = null;
            int page = 0;

            try
            {

                while (true)
                {
                    try
                    {
                        Thread.Sleep(1000);
                        var jobItems = getDataWebEmlement(By.Id("idJobNew")).FindElements(By.ClassName("job-item"));
                        foreach (var job in jobItems)
                        {
                            string title = job.FindElement(By.TagName("a")).Text;
                            string href = job.FindElement(By.TagName("a")).GetAttribute("href");
                            listAddress.AddLast(new Job(id++.ToString(), title, href));
                        }
                    }
                    catch (StaleElementReferenceException e)
                    {
                        System.Diagnostics.Debug.WriteLine(e);
                        try
                        {
                            Thread.Sleep(500);
                            aNext1 = getDataWebEmlement(By.XPath("//span[contains(text(),'Trang sau')]/parent::*"));

                        }
                        catch (NoSuchElementException)
                        {
                            aNext1 = new EmptyElement();
                        }

                        if (aNext1.GetAttribute("href") == null || aNext1.GetAttribute("href").Equals("") || aNext1 is EmptyElement)
                        {
                            callback(listAddress);
                            chromeDriver.Quit();
                            return;
                        }
                        else
                        {
                            System.Diagnostics.Debug.WriteLine("click: " + page);
                            aNext1.Click();
                            page++;
                        }
                        continue;
                    }

                    try
                    {
                        Thread.Sleep(500);
                        aNext1 = getDataWebEmlement(By.XPath("//span[contains(text(),'Trang sau')]/parent::*"));

                    }
                    catch (NoSuchElementException e)
                    {
                        aNext1 = new EmptyElement();
                    }

                    if (aNext1.GetAttribute("href") == null || aNext1.GetAttribute("href").Equals("") || aNext1 is EmptyElement)
                    {
                        callback(listAddress);
                        chromeDriver.Quit();
                        return;
                    }
                    else
                    {
                        System.Diagnostics.Debug.WriteLine("click: " + page);
                        aNext1.Click();
                        page++;
                    }
                    //Cài đặt bao nhiêu trang là thoát
                    if (page == numberPages && numberPages > 0)
                    {
                        callback(listAddress);
                        return;
                    }

                }

            }
            catch (Exception e)
            {
                callback(listAddress);
                chromeDriver.Quit();
                return;
            }

        }

        public void getJobDetail(ICollection<dynamic> list, bool autoSave, string fileDes)
        {
                ExcelAction excel = new ExcelAction();
                LinkedList<JobDetail> listDetail = new LinkedList<JobDetail>();
            try
            {
                foreach (Job job in list)
                {
                    crawler(job, jobDetail =>
                    {
                        listDetail.AddLast(jobDetail);

                    });

                };

                chromeDriver.Quit();
                if (autoSave)
                {
                    excel.saveJobDetailCustom(fileDes, sheet => {
                        insertData(sheet, listDetail);
                    });
                }
                else
                {
                    excel.saveExcelJobDetail(sheet =>
                    {
                        insertData(sheet, listDetail);
                    });
                }
            }
            catch (Exception e)
            {
                chromeDriver.Quit();
                if (autoSave)
                {
                    excel.saveJobDetailCustom(fileDes, sheet => {
                        insertData(sheet, listDetail);
                    });
                }
                else
                {
                    excel.saveExcelJobDetail(sheet =>
                    {
                        insertData(sheet, listDetail);
                    });
                }
                System.Diagnostics.Debug.WriteLine(e);
                return;
            }
        }

        private void insertData(Excel._Worksheet sheet, LinkedList<JobDetail> listDetail)
        {
            for (int h = 1; h <= arrayHeader.Length; h++)
            {
                sheet.Cells[1, h] = arrayHeader[h - 1];
            }

            int i = 0;
            System.Diagnostics.Debug.WriteLine(listDetail.Count);
            foreach (var b in listDetail)
            {
                string[] a = {b.id, b.title, b.nameCty,b.ngayCapNhat,
                    b.addressCty,
                    b.quyMoCty,
                    b.nguoiLienHe,
                    //b.sdt,
                    b.diachiLienHe,
                    b.luong,
                    b.kinhNghiem,
                    b.chucVu,
                    //b.tuoi,
                    b.nganhNghe,
                    b.capBac,
                    b.hanNopCV,
                    b.motaCV,
                    b.yeucauCV,
                    b.phucLoi,
                    b.yeuCauHS,
                    b.gioiTinh, b.soluongCT, b.hinhThucLV,
                    b.luotXem,
                            //b.website
                        };

                for (int j = 1; j <= a.Length; j++)
                {
                    sheet.Cells[i + 2, j].NumberFormat = "@";
                    sheet.Cells[i + 2, j].HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;
                    sheet.Cells[i + 2, j] = a[j - 1];
                }
                i++;
            }
        }

        override
        public void crawler(Job job, Action<JobDetail> callback)
        {
            try
            {
                    chromeDriver.Navigate().GoToUrl(job.href);
                    string title = getDataWebEmlement(By.XPath("//*[@id='detail-el']/div/div[2]/h1")).Text;
                    string ngayCapNhat = sliptCharacter(getDataWebEmlement(By.XPath("//strong[contains(text(),'Ngày duyệt:')]/parent::*")).Text, ':');
                    string ngayHetHan = getDataWebEmlement(By.XPath("//*[@id='detail-el']/div/div[2]/p[3]/span")).Text;

                    string nguoiLienHe = sliptCharacter(getDataWebEmlement(By.XPath("//strong[contains(text(),'Người liên hệ:')]/parent::*/parent::*")).Text,':');
                    string diaChiLienHe = sliptCharacter(getDataWebEmlement(By.XPath("//strong[contains(text(),'Địa chỉ công ty:')]/parent::*/parent::*")).Text, ':');
                    //string sdt = getDataWebEmlement(By.XPath("/html/body/div[1]/div[2]/div[2]/div[1]/div/ul[3]/li[5]/span")).Text;

                    string nameCty = getDataWebEmlement(By.XPath("//*[@id='__layout']/section/div[2]/div/section/div/div[1]/div[5]/div/div[1]/div/div/p[1]")).Text;
                    string addressCty = getDataWebEmlement(By.XPath("//*[@id='__layout']/section/div[2]/div/section/div/div[1]/div[5]/div/div[1]/div/div/p[2]/span")).Text;
                    string quyMoCty = getDataWebEmlement(By.XPath("//*[@id='__layout']/section/div[2]/div/section/div/div[1]/div[5]/div/div[1]/div/div/p[3]")).Text;
                    //string website = getDataWebEmlement(By.XPath("/html/body/div[1]/div[2]/div[2]/div[2]/div/dl/dd")).Text;


                    string luong = sliptCharacter(getDataWebEmlement(By.XPath("//*[@id='detail-el']/div/div[2]/p[2]/span")).Text, ':');
                    string kinhNghiem = sliptCharacter(getDataWebEmlement(By.XPath("//*[@id='__layout']/section/div[2]/div/section/div/div[1]/div[4]/div[1]/div[1]/div[1]/p[1]")).Text, ':');
                    string hinhThucLV = sliptCharacter(getDataWebEmlement(By.XPath("//*[@id='__layout']/section/div[2]/div/section/div/div[1]/div[4]/div[1]/div[1]/div[2]/p[1]")).Text, ':');
                    string gioiTinh = sliptCharacter(getDataWebEmlement(By.XPath("//*[@id='__layout']/section/div[2]/div/section/div/div[1]/div[4]/div[1]/div[1]/div[2]/p[3]")).Text, ':');
                    //string tuoi = sliptCharacter(getDataWebEmlement(By.XPath("//li[contains(text(),\"Tuổi:\")]")).Text, ':');

                    string yeuCauBC = sliptCharacter(getDataWebEmlement(By.XPath("//*[@id='__layout']/section/div[2]/div/section/div/div[1]/div[4]/div[1]/div[1]/div[1]/p[2]")).Text, ':');
                    string nganhNghe = sliptCharacter(getDataWebEmlement(By.XPath("//strong[contains(text(),'Ngành nghề:')]/parent::*/parent::*")).Text, ':');
                    string soluongCT = sliptCharacter(getDataWebEmlement(By.XPath("//strong[contains(text(),'Số lượng cần tuyển:')]/parent::*")).Text, ':');
                    string chucVu = sliptCharacter(getDataWebEmlement(By.XPath("//strong[contains(text(),'Chức vụ:')]/parent::*")).Text, ':');

                    string luotXem = sliptCharacter(getDataWebEmlement(By.XPath("//strong[contains(text(),'Lượt xem:')]/parent::*")).Text, ':');

                    string motaCV = getDataWebEmlement(By.XPath("//*[@id='__layout']/section/div[2]/div/section/div/div[1]/div[4]/div[1]/div[3]")).Text;
                    string yeucauCV = getDataWebEmlement(By.XPath("//*[@id='__layout']/section/div[2]/div/section/div/div[1]/div[4]/div[1]/div[5]")).Text;
                    string quyenLoi = getDataWebEmlement(By.XPath("//*[@id='__layout']/section/div[2]/div/section/div/div[1]/div[4]/div[1]/div[4]")).Text;
                    string yeuCauHS = getDataWebEmlement(By.XPath("//*[@id='__layout']/section/div[2]/div/section/div/div[1]/div[4]/div[1]/div[6]")).Text;




                System.Diagnostics.Debug.WriteLine(title + "\t" + ngayCapNhat + "\t" + nameCty + "\t" + addressCty + "\t" + nguoiLienHe + "\t" + diaChiLienHe + "\t" + kinhNghiem);
                    //System.Diagnostics.Debug.WriteLine(kinhNghiem + "\t" + nganhNghe + "\t" + yeuCauBC + "\t" + luong);
                    //System.Diagnostics.Debug.WriteLine(motaCV + "\t" + yeucauCV + "\t" + quyenLoi);

                    JobDetail jobDetail = new JobDetail()
                    {
                        id    = job.id,
                        title = title,
                        nameCty = nameCty,
                        ngayCapNhat =  ngayCapNhat,
                        hanNopCV =  ngayHetHan,
                        nguoiLienHe = nguoiLienHe,
                        diachiLienHe = diaChiLienHe,
                        addressCty = addressCty,
                        quyMoCty = splitByString(quyMoCty, "Quy mô công ty", 1),
                        luong = luong,
                        kinhNghiem = kinhNghiem,
                        hinhThucLV = hinhThucLV,
                        gioiTinh = gioiTinh,
                        capBac = yeuCauBC,
                        nganhNghe = nganhNghe,
                        soluongCT = soluongCT,
                        motaCV =  motaCV,
                        yeucauCV =  yeucauCV,
                        phucLoi = quyenLoi,
                        chucVu = chucVu,
                        yeuCauHS = yeuCauHS,
                        luotXem = luotXem,
                        //website = website,
                        //tuoi = tuoi,
                        //sdt = sdt,

                    };

                    callback(jobDetail);
                
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e);
                return;
            }

        }

        //XML
        override
        public void saveXML(ICollection<JobDetail> list, string desc)
        {
            try
            {
                System.Diagnostics.Debug.WriteLine("Bat dau luu! XML ");
                XElement xmlElements = new XElement("data", list.Select(i => new XElement("Item", nodes(i))));

                xmlElements.Save(desc + ".xml");

                System.Diagnostics.Debug.WriteLine("Xong!");
                Util.showMessageInformation("Lưu thành công: " + list.Count + " Item.");
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e);
                MessageBox.Show("Lưu thất bại!", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private object[] nodes(dynamic b)
        {
            int i = 0;
            string[] a = {b.id, b.title, b.nameCty,b.ngayCapNhat,
                    b.addressCty,
                    b.quyMoCty,
                    b.nguoiLienHe,
                    //b.sdt,
                    b.diachiLienHe,
                    b.luong,
                    b.kinhNghiem,
                    b.chucVu,
                    //b.tuoi,
                    b.nganhNghe,
                    b.capBac,
                    b.hanNopCV,
                    b.motaCV,
                    b.yeucauCV,
                    b.phucLoi,
                    b.yeuCauHS,
                    b.gioiTinh, b.soluongCT, b.hinhThucLV,
                    b.luotXem,
                            //b.website
                        };
            object[] nodes = arrayHeader.Select(e => new XElement(e.Replace(' ', '-'), a[i++])).ToArray<object>();

            return nodes;
        }

        
    }
}
