﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Linq;

namespace CrawlerWeb
{
    class Util
    {
        public static void downloadImageAsync(ICollection<dynamic> list, string folderDes, Action<string> callback)
        {
            using (WebClient webClient = new WebClient())
            {
                int i = 1;
                foreach (model.Item e in list)
                {
                     if ( e.href !=null && !e.href.Equals("Null"))
                    {
                       webClient.DownloadFile(new Uri(e.href), folderDes + "\\" + getFileName(e.href));

                        while (webClient.IsBusy)
                        {
                        }
                        Console.WriteLine("Tai xong: "+ getFileName(e.href));
                        string progress = i++ + "/" + list.Count;
                        callback(progress);
                            continue;
                    }
                    else
                    {
                        string progress = i++ + "/" + list.Count;
                        callback(progress);
                    }

                }

            }
        }

        private static string getFileName(string urlImage)
        {
            Uri uri = new Uri(urlImage);
            string filename = Path.GetFileName(uri.LocalPath);
            return filename;
        }

        public static void saveXML(ICollection<dynamic> list, string desc)
        {
            try
            {
                System.Diagnostics.Debug.WriteLine("Bat dau luu! XML ");
                XElement xmlElements = new XElement("data", list.Select(i => new XElement("Item", nodes(i))));

                xmlElements.Save(desc + ".xml");

                System.Diagnostics.Debug.WriteLine("Xong!");
                MessageBox.Show("Lưu thành công!", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e);
                MessageBox.Show("Lưu thất bại!", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private static object[] nodes(model.Job b)
        {
            int i = 0;
            string[] arrayHeader = {"id", "title","href" };
            string[] a = {b.id, b.title, b.href };

            object[] nodes = arrayHeader.Select(e => new XElement(e.Replace(' ', '-'), a[i++])).ToArray<object>();

            return nodes;
        }

        public static ICollection<dynamic> OpenXML(string fileName)
        {
                ICollection<dynamic> list = new List<dynamic>();
            try
            {

                XmlDocument doc = new XmlDocument();
                doc.Load(fileName);


                XmlElement root = doc.DocumentElement;
                XmlNodeList nodes = root.SelectNodes("//Item");

                foreach (XmlNode node in nodes)
                {

                    model.Job job = new model.Job(node["id"].InnerText, node["title"].InnerText, node["href"].InnerText);
                    list.Add(job);
                }
            }
            catch(Exception e)
            {
                showMessageError("Sai định dạng Link.");
            }
            

            return list;
        }

        public static SaveFileDialog chooseAddressSaveXML()
        {
            SaveFileDialog dialog = new SaveFileDialog();
            dialog.Filter = "XML files (*.xml)|*.xml|All files (*.*)|*.*";
            dialog.ShowDialog();
            return dialog;
        }

        public static SaveFileDialog chooseAddressSaveExcel()
        {
            SaveFileDialog dialog = new SaveFileDialog();
            dialog.Filter = "Excel files (*.xlsx)|*.xlsx|All files (*.*)|*.*";
            dialog.ShowDialog();
            return dialog;
        }

        public static void showMessageError(string message)
        {
            MessageBox.Show(message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        public static void showMessageInformation(string message)
        {
            MessageBox.Show(message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
        public static OpenFileDialog showOpenFileDialogXML()
        {
            OpenFileDialog fileDialog = new OpenFileDialog();
            fileDialog.Filter = "XML files (*.xml)|*.xml|All files (*.*)|*.*";
            fileDialog.ShowDialog();
            return fileDialog;
        }

        public static ICollection<dynamic> OpenXMLImg(string fileName)
        {
            ICollection<dynamic> list = new List<dynamic>();

            try
            {
                XmlDocument doc = new XmlDocument();
                doc.Load(fileName);


                XmlElement root = doc.DocumentElement;
                XmlNodeList nodes = root.SelectNodes("//Item");

                foreach (XmlNode node in nodes)
                {
                    model.Item job = new model.Item(node["ID"].InnerText, node["title"].InnerText, node["nameSP"].InnerText, node["href"].InnerText);
                    list.Add(job);
                }
            }
            catch(Exception e)
            {
                Console.WriteLine(e);
                showMessageError("Sai định dạng link IMG");
            }

            return list;
        }
    }
}
