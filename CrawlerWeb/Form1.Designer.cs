﻿namespace CrawlerWeb
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCrawler = new System.Windows.Forms.Button();
            this.listView1 = new System.Windows.Forms.ListView();
            this.btnTest = new System.Windows.Forms.Button();
            this.comboBoxSalary = new System.Windows.Forms.ComboBox();
            this.comboBoxAddress = new System.Windows.Forms.ComboBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnGetUrlImage = new System.Windows.Forms.Button();
            this.btnCategory = new System.Windows.Forms.Button();
            this.textBoxPages = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnLoadExcel = new System.Windows.Forms.Button();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.button1 = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.radioButtonXML = new System.Windows.Forms.RadioButton();
            this.radioButtonExcel = new System.Windows.Forms.RadioButton();
            this.btnChooseAddressSaveJobDetail = new System.Windows.Forms.Button();
            this.btnChooseAddressSaveLinkJob = new System.Windows.Forms.Button();
            this.checkBoxAutoSaveJobs = new System.Windows.Forms.CheckBox();
            this.checkBoxAutoSaveLinks = new System.Windows.Forms.CheckBox();
            this.textBoxAddressJobsDetail = new System.Windows.Forms.TextBox();
            this.textBoxAddressLinkJobs = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panelTienTrinh = new System.Windows.Forms.Panel();
            this.labelProgress = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.btnChooseAddressSaveImg = new System.Windows.Forms.Button();
            this.textBoxAddressSaveImg = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.button3 = new System.Windows.Forms.Button();
            this.btnOpenExcel = new System.Windows.Forms.Button();
            this.panel4 = new System.Windows.Forms.Panel();
            this.btnGetLinkByListCategory = new System.Windows.Forms.Button();
            this.btnDeleteCategory = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.btnAddCategory = new System.Windows.Forms.Button();
            this.panel5 = new System.Windows.Forms.Panel();
            this.numericUpDownTo = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownFrom = new System.Windows.Forms.NumericUpDown();
            this.btnDeleteListLinkItem = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.panelDate = new System.Windows.Forms.Panel();
            this.textBoxDate = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.btnFilter = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxPages)).BeginInit();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panelTienTrinh.SuspendLayout();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownTo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownFrom)).BeginInit();
            this.panelDate.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnCrawler
            // 
            this.btnCrawler.Location = new System.Drawing.Point(391, 6);
            this.btnCrawler.Name = "btnCrawler";
            this.btnCrawler.Size = new System.Drawing.Size(75, 43);
            this.btnCrawler.TabIndex = 0;
            this.btnCrawler.Text = "Lấy Data";
            this.btnCrawler.UseVisualStyleBackColor = true;
            this.btnCrawler.Click += new System.EventHandler(this.btnCrawler_Click);
            // 
            // listView1
            // 
            this.listView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listView1.GridLines = true;
            this.listView1.LabelEdit = true;
            this.listView1.Location = new System.Drawing.Point(12, 188);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(1230, 254);
            this.listView1.TabIndex = 3;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = System.Windows.Forms.View.Details;
            // 
            // btnTest
            // 
            this.btnTest.Location = new System.Drawing.Point(259, 30);
            this.btnTest.Name = "btnTest";
            this.btnTest.Size = new System.Drawing.Size(75, 23);
            this.btnTest.TabIndex = 4;
            this.btnTest.Text = "Lấy Link";
            this.btnTest.UseVisualStyleBackColor = true;
            this.btnTest.Click += new System.EventHandler(this.btnTest_Click);
            // 
            // comboBoxSalary
            // 
            this.comboBoxSalary.FormattingEnabled = true;
            this.comboBoxSalary.Items.AddRange(new object[] {
            "Tất cả mức lương",
            "≤ $500",
            "$500 - $1000",
            "$1000 - $1500",
            "$1500 - $2000",
            "$2000 - $3000",
            "≥ $3000"});
            this.comboBoxSalary.Location = new System.Drawing.Point(132, 30);
            this.comboBoxSalary.Name = "comboBoxSalary";
            this.comboBoxSalary.Size = new System.Drawing.Size(121, 21);
            this.comboBoxSalary.TabIndex = 6;
            this.comboBoxSalary.Text = "Tất cả mức lương";
            // 
            // comboBoxAddress
            // 
            this.comboBoxAddress.FormattingEnabled = true;
            this.comboBoxAddress.Items.AddRange(new object[] {
            "Tất cả địa điểm",
            "Hồ Chí Minh",
            "Hà Nội",
            "ĐBSCL",
            "An Giang",
            "Bà Rịa - Vũng Tàu",
            "Bắc Kạn",
            "Bắc Giang",
            "Bạc Liêu",
            "Bắc Ninh",
            "Bến Tre",
            "Biên Hòa",
            "Bình Định",
            "Bình Dương",
            "Bình Phước",
            "Bình Thuận",
            "Cà Mau",
            "Cần Thơ",
            "Cao Bằng",
            "Đà Nẵng",
            "Đắk Lắk",
            "Điện Biên",
            "Đồng Nai",
            "Đồng Tháp",
            "Gia Lai",
            "Hà Giang",
            "Hà Nam",
            "Hà Tây",
            "Hà Tĩnh",
            "Hải Dương",
            "Hải Phòng",
            "Hòa Bình",
            "Huế",
            "Hưng Yên",
            "Khánh Hòa",
            "Kon Tum",
            "Lai Châu",
            "Lâm Đồng",
            "Lạng Sơn",
            "Lào Cai",
            "Long An",
            "Nam Định",
            "Nghệ An",
            "Ninh Bình",
            "Ninh Thuận",
            "Phú Thọ",
            "Phú Yên",
            "Quảng Bình",
            "Quảng Nam",
            "Quảng Ngãi",
            "Quảng Ninh",
            "Quảng Trị",
            "Sóc Trăng",
            "Sơn La",
            "Tây Ninh",
            "Thái Bình",
            "Thái Nguyên",
            "Thanh Hóa",
            "Thừa Thiên-Huế",
            "Tiền Giang",
            "Trà Vinh",
            "Tuyên Quang",
            "Kiên Giang",
            "Vĩnh Long",
            "Vĩnh Phúc",
            "Yên Bái",
            "Khác",
            "Quốc tế",
            "Hậu Giang",
            "Đắk Nông"});
            this.comboBoxAddress.Location = new System.Drawing.Point(5, 30);
            this.comboBoxAddress.Name = "comboBoxAddress";
            this.comboBoxAddress.Size = new System.Drawing.Size(121, 21);
            this.comboBoxAddress.TabIndex = 7;
            this.comboBoxAddress.Text = "Tất cả địa điểm";
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.AutoSize = true;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.btnGetUrlImage);
            this.panel1.Controls.Add(this.btnCategory);
            this.panel1.Controls.Add(this.textBoxPages);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.btnLoadExcel);
            this.panel1.Controls.Add(this.comboBox1);
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.comboBoxSalary);
            this.panel1.Controls.Add(this.comboBoxAddress);
            this.panel1.Controls.Add(this.btnTest);
            this.panel1.Location = new System.Drawing.Point(13, 7);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(445, 88);
            this.panel1.TabIndex = 8;
            // 
            // btnGetUrlImage
            // 
            this.btnGetUrlImage.Location = new System.Drawing.Point(341, 58);
            this.btnGetUrlImage.Name = "btnGetUrlImage";
            this.btnGetUrlImage.Size = new System.Drawing.Size(93, 23);
            this.btnGetUrlImage.TabIndex = 15;
            this.btnGetUrlImage.Text = "Lấy Url Image";
            this.btnGetUrlImage.UseVisualStyleBackColor = true;
            this.btnGetUrlImage.Visible = false;
            this.btnGetUrlImage.Click += new System.EventHandler(this.btnGetUrlImage_Click);
            // 
            // btnCategory
            // 
            this.btnCategory.Location = new System.Drawing.Point(341, 29);
            this.btnCategory.Name = "btnCategory";
            this.btnCategory.Size = new System.Drawing.Size(93, 23);
            this.btnCategory.TabIndex = 14;
            this.btnCategory.Text = "Lấy danh mục";
            this.btnCategory.UseVisualStyleBackColor = true;
            this.btnCategory.Visible = false;
            this.btnCategory.Click += new System.EventHandler(this.btnCategory_Click);
            // 
            // textBoxPages
            // 
            this.textBoxPages.Location = new System.Drawing.Point(132, 57);
            this.textBoxPages.Name = "textBoxPages";
            this.textBoxPages.Size = new System.Drawing.Size(70, 20);
            this.textBoxPages.TabIndex = 13;
            this.textBoxPages.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(4, 58);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(87, 13);
            this.label2.TabIndex = 11;
            this.label2.Text = "Số trang cần lấy:";
            // 
            // label1
            // 
            this.label1.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label1.Location = new System.Drawing.Point(3, 4);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(123, 25);
            this.label1.TabIndex = 10;
            this.label1.Text = "Trang cần lấy dữ liệu:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnLoadExcel
            // 
            this.btnLoadExcel.Location = new System.Drawing.Point(340, 6);
            this.btnLoadExcel.Name = "btnLoadExcel";
            this.btnLoadExcel.Size = new System.Drawing.Size(94, 23);
            this.btnLoadExcel.TabIndex = 10;
            this.btnLoadExcel.Text = "Mở Links";
            this.btnLoadExcel.UseVisualStyleBackColor = true;
            this.btnLoadExcel.Click += new System.EventHandler(this.btnLoadExcel_Click);
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "Việt Nam Work",
            "Tìm Việt Nhanh",
            "CareerLink",
            "IT Viec",
            "My Work",
            "Careerbuilder.vn",
            "DiaChiLamDep.Net",
            "TrangVangVietNam.COM"});
            this.comboBox1.Location = new System.Drawing.Point(132, 6);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(121, 21);
            this.comboBox1.TabIndex = 9;
            this.comboBox1.Text = "Việt Nam Work";
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(259, 6);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 8;
            this.button1.Text = "Lưu Links";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel2.Controls.Add(this.radioButtonXML);
            this.panel2.Controls.Add(this.radioButtonExcel);
            this.panel2.Controls.Add(this.btnChooseAddressSaveJobDetail);
            this.panel2.Controls.Add(this.btnChooseAddressSaveLinkJob);
            this.panel2.Controls.Add(this.checkBoxAutoSaveJobs);
            this.panel2.Controls.Add(this.checkBoxAutoSaveLinks);
            this.panel2.Controls.Add(this.textBoxAddressJobsDetail);
            this.panel2.Controls.Add(this.textBoxAddressLinkJobs);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.btnCrawler);
            this.panel2.Location = new System.Drawing.Point(474, 7);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(475, 86);
            this.panel2.TabIndex = 9;
            // 
            // radioButtonXML
            // 
            this.radioButtonXML.AutoSize = true;
            this.radioButtonXML.Location = new System.Drawing.Point(418, 59);
            this.radioButtonXML.Name = "radioButtonXML";
            this.radioButtonXML.Size = new System.Drawing.Size(47, 17);
            this.radioButtonXML.TabIndex = 11;
            this.radioButtonXML.Text = "XML";
            this.radioButtonXML.UseVisualStyleBackColor = true;
            // 
            // radioButtonExcel
            // 
            this.radioButtonExcel.AutoSize = true;
            this.radioButtonExcel.Checked = true;
            this.radioButtonExcel.Location = new System.Drawing.Point(360, 59);
            this.radioButtonExcel.Name = "radioButtonExcel";
            this.radioButtonExcel.Size = new System.Drawing.Size(51, 17);
            this.radioButtonExcel.TabIndex = 10;
            this.radioButtonExcel.TabStop = true;
            this.radioButtonExcel.Text = "Excel";
            this.radioButtonExcel.UseVisualStyleBackColor = true;
            // 
            // btnChooseAddressSaveJobDetail
            // 
            this.btnChooseAddressSaveJobDetail.Location = new System.Drawing.Point(300, 27);
            this.btnChooseAddressSaveJobDetail.Name = "btnChooseAddressSaveJobDetail";
            this.btnChooseAddressSaveJobDetail.Size = new System.Drawing.Size(49, 23);
            this.btnChooseAddressSaveJobDetail.TabIndex = 9;
            this.btnChooseAddressSaveJobDetail.Text = "Chọn";
            this.btnChooseAddressSaveJobDetail.UseVisualStyleBackColor = true;
            this.btnChooseAddressSaveJobDetail.Click += new System.EventHandler(this.btnChooseAddressSaveJobDetail_Click);
            // 
            // btnChooseAddressSaveLinkJob
            // 
            this.btnChooseAddressSaveLinkJob.Location = new System.Drawing.Point(300, 2);
            this.btnChooseAddressSaveLinkJob.Name = "btnChooseAddressSaveLinkJob";
            this.btnChooseAddressSaveLinkJob.Size = new System.Drawing.Size(49, 23);
            this.btnChooseAddressSaveLinkJob.TabIndex = 8;
            this.btnChooseAddressSaveLinkJob.Text = "Chọn";
            this.btnChooseAddressSaveLinkJob.UseVisualStyleBackColor = true;
            this.btnChooseAddressSaveLinkJob.Click += new System.EventHandler(this.btnChooseAddressSaveLinkJob_Click);
            // 
            // checkBoxAutoSaveJobs
            // 
            this.checkBoxAutoSaveJobs.AutoSize = true;
            this.checkBoxAutoSaveJobs.Location = new System.Drawing.Point(155, 60);
            this.checkBoxAutoSaveJobs.Name = "checkBoxAutoSaveJobs";
            this.checkBoxAutoSaveJobs.Size = new System.Drawing.Size(194, 17);
            this.checkBoxAutoSaveJobs.TabIndex = 7;
            this.checkBoxAutoSaveJobs.Text = "Tự động lưu dữ liệu Job/Img chi tiết";
            this.checkBoxAutoSaveJobs.UseVisualStyleBackColor = true;
            // 
            // checkBoxAutoSaveLinks
            // 
            this.checkBoxAutoSaveLinks.AutoSize = true;
            this.checkBoxAutoSaveLinks.Location = new System.Drawing.Point(6, 60);
            this.checkBoxAutoSaveLinks.Name = "checkBoxAutoSaveLinks";
            this.checkBoxAutoSaveLinks.Size = new System.Drawing.Size(127, 17);
            this.checkBoxAutoSaveLinks.TabIndex = 6;
            this.checkBoxAutoSaveLinks.Text = "Tự động lưu Link Job";
            this.checkBoxAutoSaveLinks.UseVisualStyleBackColor = true;
            // 
            // textBoxAddressJobsDetail
            // 
            this.textBoxAddressJobsDetail.Location = new System.Drawing.Point(155, 30);
            this.textBoxAddressJobsDetail.Name = "textBoxAddressJobsDetail";
            this.textBoxAddressJobsDetail.Size = new System.Drawing.Size(138, 20);
            this.textBoxAddressJobsDetail.TabIndex = 4;
            // 
            // textBoxAddressLinkJobs
            // 
            this.textBoxAddressLinkJobs.Location = new System.Drawing.Point(155, 4);
            this.textBoxAddressLinkJobs.Name = "textBoxAddressLinkJobs";
            this.textBoxAddressLinkJobs.Size = new System.Drawing.Size(138, 20);
            this.textBoxAddressLinkJobs.TabIndex = 3;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 30);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(123, 13);
            this.label4.TabIndex = 2;
            this.label4.Text = "Thư mục lưu Job chi tiết:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 6);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(145, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "Thư mục lưu link Job cần lấy:";
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel3.Controls.Add(this.panelTienTrinh);
            this.panel3.Controls.Add(this.btnChooseAddressSaveImg);
            this.panel3.Controls.Add(this.textBoxAddressSaveImg);
            this.panel3.Controls.Add(this.label5);
            this.panel3.Controls.Add(this.button3);
            this.panel3.Controls.Add(this.btnOpenExcel);
            this.panel3.Location = new System.Drawing.Point(967, 7);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(276, 87);
            this.panel3.TabIndex = 10;
            // 
            // panelTienTrinh
            // 
            this.panelTienTrinh.Controls.Add(this.labelProgress);
            this.panelTienTrinh.Controls.Add(this.label6);
            this.panelTienTrinh.Location = new System.Drawing.Point(183, 6);
            this.panelTienTrinh.Name = "panelTienTrinh";
            this.panelTienTrinh.Size = new System.Drawing.Size(86, 70);
            this.panelTienTrinh.TabIndex = 5;
            // 
            // labelProgress
            // 
            this.labelProgress.AutoSize = true;
            this.labelProgress.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelProgress.Location = new System.Drawing.Point(21, 27);
            this.labelProgress.Name = "labelProgress";
            this.labelProgress.Size = new System.Drawing.Size(31, 20);
            this.labelProgress.TabIndex = 1;
            this.labelProgress.Text = "0/0";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(13, 3);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(51, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "Tiến trình";
            // 
            // btnChooseAddressSaveImg
            // 
            this.btnChooseAddressSaveImg.Location = new System.Drawing.Point(136, 53);
            this.btnChooseAddressSaveImg.Name = "btnChooseAddressSaveImg";
            this.btnChooseAddressSaveImg.Size = new System.Drawing.Size(41, 23);
            this.btnChooseAddressSaveImg.TabIndex = 4;
            this.btnChooseAddressSaveImg.Text = "Chọn";
            this.btnChooseAddressSaveImg.UseVisualStyleBackColor = true;
            this.btnChooseAddressSaveImg.Click += new System.EventHandler(this.btnChooseAddressSaveImg_Click);
            // 
            // textBoxAddressSaveImg
            // 
            this.textBoxAddressSaveImg.Location = new System.Drawing.Point(6, 55);
            this.textBoxAddressSaveImg.Name = "textBoxAddressSaveImg";
            this.textBoxAddressSaveImg.Size = new System.Drawing.Size(123, 20);
            this.textBoxAddressSaveImg.TabIndex = 3;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(3, 36);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(93, 13);
            this.label5.TabIndex = 2;
            this.label5.Text = "Thư mục lưu ảnh: ";
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(102, 5);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 44);
            this.button3.TabIndex = 1;
            this.button3.Text = "Tải Hình";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // btnOpenExcel
            // 
            this.btnOpenExcel.Location = new System.Drawing.Point(6, 6);
            this.btnOpenExcel.Name = "btnOpenExcel";
            this.btnOpenExcel.Size = new System.Drawing.Size(75, 23);
            this.btnOpenExcel.TabIndex = 0;
            this.btnOpenExcel.Text = "Mở Excel";
            this.btnOpenExcel.UseVisualStyleBackColor = true;
            this.btnOpenExcel.Click += new System.EventHandler(this.btnOpenExcel_Click);
            // 
            // panel4
            // 
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel4.Controls.Add(this.btnGetLinkByListCategory);
            this.panel4.Controls.Add(this.btnDeleteCategory);
            this.panel4.Controls.Add(this.dataGridView1);
            this.panel4.Controls.Add(this.btnAddCategory);
            this.panel4.Location = new System.Drawing.Point(12, 97);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(620, 85);
            this.panel4.TabIndex = 11;
            // 
            // btnGetLinkByListCategory
            // 
            this.btnGetLinkByListCategory.Location = new System.Drawing.Point(538, 4);
            this.btnGetLinkByListCategory.Name = "btnGetLinkByListCategory";
            this.btnGetLinkByListCategory.Size = new System.Drawing.Size(75, 34);
            this.btnGetLinkByListCategory.TabIndex = 8;
            this.btnGetLinkByListCategory.Text = "Lấy link";
            this.btnGetLinkByListCategory.UseVisualStyleBackColor = true;
            this.btnGetLinkByListCategory.Click += new System.EventHandler(this.btnGetLinkByListCategory_Click);
            // 
            // btnDeleteCategory
            // 
            this.btnDeleteCategory.Location = new System.Drawing.Point(457, 44);
            this.btnDeleteCategory.Name = "btnDeleteCategory";
            this.btnDeleteCategory.Size = new System.Drawing.Size(74, 34);
            this.btnDeleteCategory.TabIndex = 7;
            this.btnDeleteCategory.Text = "Xóa";
            this.btnDeleteCategory.UseVisualStyleBackColor = true;
            this.btnDeleteCategory.Click += new System.EventHandler(this.btnDeleteCategory_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(4, 4);
            this.dataGridView1.MultiSelect = false;
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.Size = new System.Drawing.Size(431, 74);
            this.dataGridView1.TabIndex = 6;
            // 
            // btnAddCategory
            // 
            this.btnAddCategory.Location = new System.Drawing.Point(456, 4);
            this.btnAddCategory.Name = "btnAddCategory";
            this.btnAddCategory.Size = new System.Drawing.Size(75, 34);
            this.btnAddCategory.TabIndex = 5;
            this.btnAddCategory.Text = "Thêm";
            this.btnAddCategory.UseVisualStyleBackColor = true;
            this.btnAddCategory.Click += new System.EventHandler(this.btnAddCategory_Click);
            // 
            // panel5
            // 
            this.panel5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel5.Controls.Add(this.numericUpDownTo);
            this.panel5.Controls.Add(this.numericUpDownFrom);
            this.panel5.Controls.Add(this.btnDeleteListLinkItem);
            this.panel5.Controls.Add(this.label9);
            this.panel5.Controls.Add(this.label8);
            this.panel5.Controls.Add(this.label7);
            this.panel5.Location = new System.Drawing.Point(639, 97);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(200, 85);
            this.panel5.TabIndex = 12;
            // 
            // numericUpDownTo
            // 
            this.numericUpDownTo.Location = new System.Drawing.Point(58, 58);
            this.numericUpDownTo.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.numericUpDownTo.Name = "numericUpDownTo";
            this.numericUpDownTo.Size = new System.Drawing.Size(70, 20);
            this.numericUpDownTo.TabIndex = 15;
            this.numericUpDownTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numericUpDownTo.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // numericUpDownFrom
            // 
            this.numericUpDownFrom.Location = new System.Drawing.Point(58, 32);
            this.numericUpDownFrom.Name = "numericUpDownFrom";
            this.numericUpDownFrom.Size = new System.Drawing.Size(70, 20);
            this.numericUpDownFrom.TabIndex = 14;
            this.numericUpDownFrom.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numericUpDownFrom.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // btnDeleteListLinkItem
            // 
            this.btnDeleteListLinkItem.Location = new System.Drawing.Point(134, 39);
            this.btnDeleteListLinkItem.Name = "btnDeleteListLinkItem";
            this.btnDeleteListLinkItem.Size = new System.Drawing.Size(59, 34);
            this.btnDeleteListLinkItem.TabIndex = 5;
            this.btnDeleteListLinkItem.Text = "Xóa";
            this.btnDeleteListLinkItem.UseVisualStyleBackColor = true;
            this.btnDeleteListLinkItem.Click += new System.EventHandler(this.btnDeleteListLinkItem_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(15, 4);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(169, 20);
            this.label9.TabIndex = 4;
            this.label9.Text = "Tùy chọn xóa List View";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(4, 60);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(58, 13);
            this.label8.TabIndex = 1;
            this.label8.Text = "Bao nhiêu:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(3, 34);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(47, 13);
            this.label7.TabIndex = 0;
            this.label7.Text = "Từ vị trí:";
            // 
            // panelDate
            // 
            this.panelDate.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panelDate.Controls.Add(this.btnFilter);
            this.panelDate.Controls.Add(this.label10);
            this.panelDate.Controls.Add(this.textBoxDate);
            this.panelDate.Location = new System.Drawing.Point(845, 97);
            this.panelDate.Name = "panelDate";
            this.panelDate.Size = new System.Drawing.Size(114, 83);
            this.panelDate.TabIndex = 13;
            // 
            // textBoxDate
            // 
            this.textBoxDate.Location = new System.Drawing.Point(3, 27);
            this.textBoxDate.Name = "textBoxDate";
            this.textBoxDate.Size = new System.Drawing.Size(100, 20);
            this.textBoxDate.TabIndex = 0;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(3, 9);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(49, 13);
            this.label10.TabIndex = 1;
            this.label10.Text = "Từ ngày:";
            // 
            // btnFilter
            // 
            this.btnFilter.Location = new System.Drawing.Point(6, 53);
            this.btnFilter.Name = "btnFilter";
            this.btnFilter.Size = new System.Drawing.Size(88, 23);
            this.btnFilter.TabIndex = 2;
            this.btnFilter.Text = "Lọc";
            this.btnFilter.UseVisualStyleBackColor = true;
            this.btnFilter.Click += new System.EventHandler(this.btnFilter_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1255, 464);
            this.Controls.Add(this.panelDate);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.listView1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxPages)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panelTienTrinh.ResumeLayout(false);
            this.panelTienTrinh.PerformLayout();
            this.panel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownTo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownFrom)).EndInit();
            this.panelDate.ResumeLayout(false);
            this.panelDate.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        public void setupSalaryCareerLink(string typeSalary)
        {
            switch (typeSalary)
            {
                case "Hơn": setupSalaryCareeLinkHon(); break;
            }

        }

        private void setupSalaryCareeLinkHon()
        {
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox1.Location = new System.Drawing.Point(132, 58);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(121, 20);
            this.textBox1.TabIndex = 11;
            this.panel1.Controls.Add(this.textBox1);
        }

        #endregion

        private System.Windows.Forms.Button btnCrawler;
        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.Button btnTest;
        private System.Windows.Forms.ComboBox comboBoxSalary;
        private System.Windows.Forms.ComboBox comboBoxAddress;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btnLoadExcel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxAddressJobsDetail;
        private System.Windows.Forms.TextBox textBoxAddressLinkJobs;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckBox checkBoxAutoSaveJobs;
        private System.Windows.Forms.CheckBox checkBoxAutoSaveLinks;
        private System.Windows.Forms.Button btnChooseAddressSaveJobDetail;
        private System.Windows.Forms.Button btnChooseAddressSaveLinkJob;
        private System.Windows.Forms.NumericUpDown textBoxPages;
        private System.Windows.Forms.Button btnCategory;
        private System.Windows.Forms.Button btnGetUrlImage;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button btnOpenExcel;
        private System.Windows.Forms.TextBox textBoxAddressSaveImg;
        private System.Windows.Forms.Button btnChooseAddressSaveImg;
        private System.Windows.Forms.Panel panelTienTrinh;
        private System.Windows.Forms.Label labelProgress;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.RadioButton radioButtonXML;
        private System.Windows.Forms.RadioButton radioButtonExcel;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button btnAddCategory;
        private System.Windows.Forms.Button btnDeleteCategory;
        private System.Windows.Forms.Button btnGetLinkByListCategory;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Button btnDeleteListLinkItem;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.NumericUpDown numericUpDownTo;
        private System.Windows.Forms.NumericUpDown numericUpDownFrom;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Panel panelDate;
        private System.Windows.Forms.Button btnFilter;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox textBoxDate;
    }
}

